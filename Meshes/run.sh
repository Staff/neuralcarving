#!/bin/bash
#SBATCH --cpus-per-task=6
#SBATCH --mem=32GB
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu-cluster,I8
#SBATCH --job-name=mesh_opt
#SBATCH --output=run.log

HOME=/clusterstorage/telsner
source /clusterstorage/telsner/.bashrc

conda activate research

python -u script.py