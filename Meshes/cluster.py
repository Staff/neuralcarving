# %%
import torch
import numpy as np
import k3d
from scipy.spatial import KDTree
from urllib.parse import quote as url_quote
import open3d as o3d
from scipy.spatial import KDTree
from helpers import visualise
from nets import Deform3D as Deform3D

# %% [markdown]
# <h1>Arguments to set</h1>

# %%
SCENE_NAME = "floorplan_tri_centred_subset.obj"
SCENE_NAME = "chair_0091_centred.off"
SCENE_NAME = "floorplan_centred.off"
#SCENE_NAME = "floorplan_floor_only_centred.off"

SAMPLES_FOR_INITIALISATION_TRAINING = 10000
AXIS_TO_OFFSET = 0 #axis we apply deformation on
SCALING_FACTOR = 0.5 #> 1: stretch, < 1: compress
CUMSUM_STEPS = 100 #number of samples to approximate cumulative sum 

REDO_ALL = False

# %% [markdown]
# <h1>Arguments to automcompute, do not touch!</h1>

# %%
OBJECT_TO_LOAD = "mesh/"+SCENE_NAME
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#OBJECT_TO_LOAD = "mesh/chair_0007_centred.off"
PATH_DEFORMATION_INIT = f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_deform_init.net"
PATH_INV_DEFORMATION_INIT = f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_inv_deform_init.net"
PATH_ENERGY_SAMPLES = f"nets/{SCENE_NAME}_cmp{SCALING_FACTOR}_energy_samples.dat"
PATH_ENERGY_SAMPLES_ENERGY = f"nets/{SCENE_NAME}_cmp{SCALING_FACTOR}_energy_samples_energy.dat"
PATH_ENERGY_NET = f"nets/{SCENE_NAME}_cmp{SCALING_FACTOR}_energy.net"
PATH_CUM_ENERGY_NET = f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_cum_energy.net"
RESULTING_DEFORMATION = f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_deform.net"

# %% [markdown]
# <h1> Sample Mesh </h1>

# %%
mesh = o3d.io.read_triangle_mesh(OBJECT_TO_LOAD)
mesh.compute_vertex_normals()
#o3d.visualization.draw_geometries([mesh])
samples_surface = torch.tensor(np.asarray(mesh.sample_points_uniformly(number_of_points=150000).points))

# %%
samples_surface_many = torch.tensor(np.asarray(mesh.sample_points_uniformly(number_of_points=100000).points))

# %% [markdown]
# <h1>Compute neighbourhoods</h1>

# %%
import numpy as np

def best_fitting_plane(points):
    """
    Finds the best fitting plane to a set of points.
    Returns the normal vector of the plane and the centroid.
    """
    # Step 1: Compute the centroid of the points
    centroid = np.mean(points, axis=0)

    # Step 2: Move the points so that the centroid is the origin
    centered_points = points - centroid

    # Step 3: Use SVD to compute the normal vector of the plane
    _, _, vh = np.linalg.svd(centered_points)
    normal = vh[-1, :]

    return normal, centroid

def point_plane_distance(point, normal, centroid):
    """
    Computes the distance from a point to a plane.
    """
    return np.abs(np.dot(normal, point - centroid) / np.linalg.norm(normal))

def compute_errors(points, normal, centroid):
    """
    Computes the distances of each point to the plane.
    """
    return [point_plane_distance(point, normal, centroid) for point in points]

# Example usage:
points = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [7, 8, 9],
    [7, 8, 9]
])

normal, centroid = best_fitting_plane(points) #[k x 3]
errors = compute_errors(points, normal, centroid)

print("Normal vector of the plane:", normal)
print("Centroid of the points:", centroid)
print("Errors (distances to the plane):", errors)

# %%
def flatness(points):
    points = points.clone()

    #centre
    centre = points.mean(dim=0)
    points -= centre[None]
    
    #covariance matrix
    cov = torch.cov(points.transpose(0,1), correction=0)
    
    #get eigenvalues
    vals = torch.linalg.eig(cov)[0].real
    
    return vals[0]# / (vals[1] + 0.00001)# + vals[0] / (vals[2] + 0.00001)#torch.linalg.eig(cov)[0].sum().real

def flatness(points):
    points = points.clone()

    #centre
    centre = points.mean(dim=0)
    points -= centre[None]

    return points.abs().mean()

a = torch.rand(20, 3)
b = torch.rand(20, 3)
b[:,2] *= 0.0

flatness(a), flatness(b)

# %%
if False:
    rot = torch.zeros(3,3)
    rot[0,0] = torch.cos(torch.tensor(3.141/2))
    rot[2,2] = torch.cos(torch.tensor(3.141/2))
    rot[2,0] = -torch.sin(torch.tensor(3.141/2))
    rot[0,2] = torch.sin(torch.tensor(3.141/2))
    rot[1,1] = 1.0
    samples_surface = torch.matmul(samples_surface.float(), rot.float())

# %%


# %%
print("Building KD-Tree...")
tree = KDTree(samples_surface) #input [k x 3]
distances = []
for k in range(0, samples_surface.size()[0]):
    if k % 50000 == 0:
        print("DONE WITH ",k/samples_surface.size()[0]*100,"%")
    pt = [samples_surface[k].numpy()]
    _, ii = tree.query(pt, k=20)
    closest = samples_surface[ii]

    if False:
        normal, centroid = best_fitting_plane(closest.numpy()) #[k x 3]
        errors = compute_errors(points, normal, centroid)
        error = torch.sum(torch.tensor(errors).abs())
        distances.append(error)
    else:
        distances.append(flatness(closest))

    #avg_distance = flatness(closest)
    #avg_distance = ((closest - torch.tensor(pt[0])[None]).square().sum(dim=1) + 0.00001).sqrt().mean()
    #as energy: find closest vertex
    #distances.append(avg_distance.min())

#average distances:
averaged_distances = []
distances_t = torch.tensor(distances)
for k in range(0, samples_surface.size()[0]):
    if k % 50000 == 0:
        print("DONE WITH ",k/samples_surface.size()[0]*100,"%")
    pt = [samples_surface[k].numpy()]
    _, ii = tree.query(pt, k=10)
    
    averaged_distances.append(distances_t[ii].min().item())
distances = averaged_distances

d_min = min(distances)
d_max = max(distances)
samples_surface_energy = torch.tensor(distances) - d_min
samples_surface_energy /= d_max#samples_surface_energy.max()
samples_surface_energy = (1.0 - samples_surface_energy) ** 3.0# ** 10.0# ** 4.0# ** 2.0

# %%
# debug #
samples_surface_energy *= 0.0
samples_surface_energy += 1.0

# %%
indices = samples_surface[:,1] < -0.4925
samples_surface_energy[indices] = 0.0

# %%
if True:
    colour = samples_surface_energy[:,None].repeat(1,3)
    colour[:,1:] *= 0.0
    visualise(samples_surface, colour, pt_size=0.015)
    pass

# %% [markdown]
# <h1>0. Definitions</h1>

# %%
samples_surface.size()

# %%
TARGET_BB = torch.zeros(2, 3)
TARGET_BB[0,0] = samples_surface[:,0].min().item()
TARGET_BB[1,0] = samples_surface[:,0].max().item()
TARGET_BB[0,1] = samples_surface[:,1].min().item()
TARGET_BB[1,1] = samples_surface[:,1].max().item()
TARGET_BB[0,2] = samples_surface[:,2].min().item()
TARGET_BB[1,2] = samples_surface[:,2].max().item()

# %%
def get_pt_indices_within_bb(ptc):
    a = torch.nonzero(ptc[:,0] > TARGET_BB[0,0])
    b = torch.nonzero(ptc[:,0] < TARGET_BB[1,0])
    c = torch.nonzero(ptc[:,1] > TARGET_BB[0,1])
    d = torch.nonzero(ptc[:,1] < TARGET_BB[1,1])
    e = torch.nonzero(ptc[:,2] > TARGET_BB[0,2])
    f = torch.nonzero(ptc[:,2] < TARGET_BB[1,2])

    indices = np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(a, b), c), d), e), f)
    return torch.tensor(indices)

def get_pt_indices_outside_bb(ptc):
    global TARGET_BB
    all_indices = torch.range(0, ptc.size()[0]-1).long()
    TARGET_BB *= 0.5
    result = np.setdiff1d(all_indices, get_pt_indices_within_bb(ptc))
    TARGET_BB *= 2.0
    return result

# %% [markdown]
# <h1>1. Initialise deformation & inverse</h1>

# %%
#samples WITHIN the bounding box - BUT: the OUTPUT of the learned net can be outside of this! last sample is at the fringe, i.e. after applying deformation, will be much further away from it
def sample_bb(no_samples, get_offset=True, boundaries_only=False):
    #get_offset: for uniform offset values, linearly increasing offset scale
    samples = torch.rand(no_samples, 3).float()
    if boundaries_only:
        #flick axis to 1 or 0 at random
        zeroing = torch.tensor([1,1,1]).float()
        zeroing[AXIS_TO_OFFSET] *= 0.0
        zeroing = zeroing[None, :]
        indices = torch.randperm(samples.size()[0])
        indices_a = indices[:int(indices.size()[0]/2)]
        indices_b = indices[int(indices.size()[0]/2):]
        #set to zero
        samples[indices_a] *= zeroing
        samples[indices_b] *= zeroing
        #set to one
        samples[indices_b, AXIS_TO_OFFSET] += 1.0

    offset_scale = samples[:,AXIS_TO_OFFSET].clone()

    bb_width = (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])

    #in [0, bb_range]^3
    for ax in range(0, 3):
        samples[:,ax] *= (TARGET_BB[1,ax] - TARGET_BB[0,ax])
    
    if get_offset:
        target_pos = samples.clone()#
        target_pos[:,AXIS_TO_OFFSET] *= SCALING_FACTOR#(1.0 - SCALING_FACTOR)# * -1.0

    #from [0, bb_range] into BB: translate
    for ax in range(0, 3):
        samples[:,ax] += TARGET_BB[0,ax]

    if get_offset:
        return samples.cuda(), target_pos.cuda()
    else:
        return samples.cuda()

# %%
out = sample_bb(10000, get_offset=False).cpu()
combined = torch.cat((samples_surface, out), 0)
c1 = torch.zeros_like(out)
c2 = torch.zeros_like(combined)
c1[:,1] = 1.0
c2[:,0] = 1.0
ccombined = torch.cat((c1, c2), 0)
visualise(combined, ccombined)

# %%
def show_deformation(apply=True):
    with torch.no_grad():
        pts = samples_surface.float().clone()
        if apply:
            offset = deform_net(pts.to(DEVICE)).cpu()
            pts[:,AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += offset
        visualise(pts, pt_size=0.001, centre=True)
        
try:
    if REDO_ALL:
        goto_except
    deform_net = torch.load(PATH_DEFORMATION_INIT).cuda()
    optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.00001)
    deform_net.train(True)
    re_learned_deform = False
except Exception as e:
    re_learned_deform = True
    deform_net = Deform3D().cuda()
    deform_net.train(True)
    optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.00001)

    for epoch in range(0, 50):
        avg_loss = 0.0
        for step in range(0, 100):
            optimiser.zero_grad()
            
            tensors, target_pos = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING)
            
            offset = deform_net(tensors)[:,0]
            
            loss = ((offset + tensors[:,AXIS_TO_OFFSET]) - target_pos[:,AXIS_TO_OFFSET]).square().mean()
            avg_loss += loss.item()
            loss.backward()
            optimiser.step()
        if epoch % 10 == 0:
            print("INPUT: ")
            show_deformation(apply=False)
            print("DEFORMED: ")
            show_deformation(apply=True)
        avg_loss /= 100.0
        print("*** DONE WITH EPOCH ",epoch," - LOSS: ",avg_loss," ***")
    torch.save(deform_net, PATH_DEFORMATION_INIT)

#show_deformation(True)
#show_deformation(False)

# %%
try:
    if re_learned_deform or REDO_ALL:
        goto_except
    inverse_deform_net = torch.load(PATH_INV_DEFORMATION_INIT)
    optimiser_inverse_deform = torch.optim.AdamW(inverse_deform_net.parameters(), lr=0.001, weight_decay=0.00001)
except:
    deform_net.train(False)
    inverse_deform_net = Deform3D().cuda()
    optimiser_inverse_deform = torch.optim.AdamW(inverse_deform_net.parameters(), lr=0.001, weight_decay=0.00001)

    for epoch in range(0, 25):
        avg_loss = 0.0
        avg_distance = 0.0
        for step in range(0, 100):
            optimiser_inverse_deform.zero_grad()

            with torch.no_grad():
                tensors = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, False)
                offset = deform_net(tensors)
                tensors_offset = tensors.clone()
                tensors_offset[:, AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += offset

            #offsets should cancel out
            loss = (inverse_deform_net(tensors_offset) + offset).square().mean()
            avg_loss += loss.item()
            loss.backward()

            tensors_offset[:, AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += inverse_deform_net(tensors_offset)
            avg_distance += (tensors_offset-tensors).square().sum(dim=-1).sqrt().mean()

            optimiser_inverse_deform.step()
        
        torch.save(inverse_deform_net, PATH_INV_DEFORMATION_INIT)
        print("AVG DISTANCE OF POINTS AFTER MAPPING BACK AND FORTH: ",avg_distance/100)
        print("*** DONE WITH EPOCH ",epoch," - INVERSE LOSS: ",avg_loss/100,", DISTANCE: ",avg_distance/100.0," ***")
    deform_net.train(True)

# %% [markdown]
# <h1>2. Learn continuous energy</h1>

# %% [markdown]
# <h3>Generate samples for training</h3>

# %%
try:
    if REDO_ALL:
        goto_except
    nrg_samples_in_bb = torch.load(PATH_ENERGY_SAMPLES)
    nrg_samples_in_bb_energy = torch.load(PATH_ENERGY_SAMPLES_ENERGY)
    re_computed_samples = False
except:
    re_computed_samples = True
    #get example prediction network: make sure points in empty space, i.e. bounding box, are also well-defined in terms of energy: assign them closest energy value
    nrg_samples_in_bb = sample_bb(100000, get_offset=False).cpu()
    nrg_samples_in_bb_energy = torch.zeros(nrg_samples_in_bb.size()[0])
    print("SAMPLES IN BB: ",nrg_samples_in_bb.size())

    print("Building KD-Tree...")
    tree = KDTree(samples_surface) #input [k x 3]
    print("Done building KD-Tree.")

    #compute the distance value to neighbours so we know when we fall off in terms of energy:
    ids = torch.randperm(samples_surface.size()[0])[0:100]
    avg_distance_to_nearest = 0.0
    for k in ids:
        pt = [samples_surface[k].numpy()]
        _, ii = tree.query(pt, k=1)
        closest = torch.tensor(ii)[0]
        distance = ((samples_surface[closest] - samples_surface[k]).square().sum() + 0.000001).sqrt()
        avg_distance_to_nearest += distance
    avg_distance_to_nearest /= ids.size()[0]
    
    #override:
    avg_distance_to_nearest = (TARGET_BB[1] - TARGET_BB[0]).max() / 50

    print("Average distance to neighbour: ",avg_distance_to_nearest)

    for k in range(0, nrg_samples_in_bb.size()[0]):
        if k % 50000 == 0:
            print("DONE WITH ",k/nrg_samples_in_bb.size()[0]*100,"%")
        pt = [nrg_samples_in_bb[k].numpy()]
        _, ii = tree.query(pt, k=1)
        closest = torch.tensor(ii)[0]
        distance = ((samples_surface[closest] - nrg_samples_in_bb[k]).square().sum() + 0.00001).sqrt()
        if distance > 2 * avg_distance_to_nearest:
            nrg_samples_in_bb_energy[k] = 0.0
        else:
            nrg_samples_in_bb_energy[k] = samples_surface_energy[closest]
            #print("CLOSEST")
    torch.save(nrg_samples_in_bb, PATH_ENERGY_SAMPLES)
    torch.save(nrg_samples_in_bb_energy, PATH_ENERGY_SAMPLES_ENERGY)
    print("DONE and saved!")

# %%


# %%
nrg_samples_in_bb_energy.size(), nrg_samples_in_bb.size()

# %%
visualise(nrg_samples_in_bb, nrg_samples_in_bb_energy[:, None].repeat(1,3))

# %%
nrg_samples_in_bb_energy -= nrg_samples_in_bb_energy.min()
nrg_samples_in_bb_energy /= nrg_samples_in_bb_energy.max()

# %%
samples_surface = samples_surface.float()

# %%
try:
    if REDO_ALL or re_computed_samples:
        goto_except
    energy_net = torch.load(PATH_ENERGY_NET)
    energy_net.train(False)
    re_learned_energy = False
except Exception as e:
    re_learned_energy = True
    energy_net = Deform3D().cuda()
    optimiser = torch.optim.AdamW(energy_net.parameters(), lr=0.001)#, weight_decay=0.00001)

    for epoch in range(0, 500):
        avg_loss = 0.0 
        for step in range(0, 50):
            optimiser.zero_grad()
            if step % 2 == 0:
                indices = torch.randperm(nrg_samples_in_bb.size()[0])[0:25000]

                pts = nrg_samples_in_bb[indices].cuda()
                energy = nrg_samples_in_bb_energy[indices].cuda()

                loss = (energy_net(pts)[:,0] - energy).square().mean() + (energy_net(pts)[:,0] - energy).abs().mean()
            else:
                indices = torch.randperm(samples_surface.size()[0])[0:25000]
                
                pts = samples_surface[indices].cuda()
                energy = samples_surface_energy[indices].cuda()

                loss = (energy_net(pts)[:,0] - energy).square().mean() + (energy_net(pts)[:,0] - energy).abs().mean()
            
            avg_loss += loss.item()
            loss.backward()
            optimiser.step()

        print("*** DONE WITH EPOCH ",epoch," - AVG ERROR: ",avg_loss/25," ***")
    energy_net.train(False)
    torch.save(energy_net, PATH_ENERGY_NET)

# %%
#debug vis:
if True:
    with torch.no_grad():
        pts = sample_bb(10000, get_offset=False)
        net_nrg = energy_net(pts)[:,0]
        net_nrg = net_nrg[:, None]
        net_nrg = net_nrg.repeat(1, 3)
        net_nrg[:,1:] *= 0.0
        
        visualise(pts, net_nrg, pt_size=0.0025)
        
    with torch.no_grad():
        pts = samples_surface.clone().to(DEVICE)
        net_nrg = energy_net(pts)[:,0]
        net_nrg = net_nrg[:, None]
        net_nrg = net_nrg.repeat(1, 3)
        net_nrg[:,1:] *= 0.0
        
        visualise(pts, net_nrg, pt_size=0.0025)

# %%
energy_net.train(False)

#iterate:
#   shoot ray through scene
#   accumulate error values across ray
#   train network for it

best_loss = None

try:
    if re_learned_energy or REDO_ALL:
        goto_except
    cumulative_net = torch.load(PATH_CUM_ENERGY_NET)
except:
    cumulative_net = Deform3D().cuda()
    optimiser_cum = torch.optim.AdamW(cumulative_net.parameters(), lr=0.001)

    for epoch in range(0, 25*4+1):
        avg_loss     = 0.0 
        its = 0
        for step in range(0, 100):
            optimiser_cum.zero_grad()

            points_surface_local = samples_surface[torch.randperm(samples_surface.size()[0])[0:100]].cuda()
            points_random  = sample_bb(100, get_offset=False).cuda()

            points_start = torch.cat((points_surface_local, points_random), 0)
            points_end   = points_start.clone()

            #snap to axis
            points_start[:, AXIS_TO_OFFSET]   = TARGET_BB[0, AXIS_TO_OFFSET]
            points_end[:, AXIS_TO_OFFSET]     = TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET]
            points_start   = points_start[None]
            points_end     = points_end[None]

            linear = torch.range(0.0, 1.0, step=(1.0 / CUMSUM_STEPS))[:,None,None].cuda()
            
            grid = points_start * (1.0 - linear) + points_end * linear
            #print("GRID: ",grid.size())
            #visualise(grid.view(-1, 3))
            
            energies_gt = energy_net(grid.view(-1, 3)).detach().view(grid.size()[0], grid.size()[1], -1)

            energies_gt = energies_gt.cumsum(dim=0).view(-1, 1) / CUMSUM_STEPS
            
            #get get values for each
            energies_out = cumulative_net(grid.view(-1, 3))

            if False:
                col = energies_gt.repeat(1,3)# / energies_gt.max()
                col -= col.min()
                col /= col.max()
                col[:,1:] *= 0.0
                pts = grid.view(-1, 3)
                print("COL: ",col.size())
                print("PTS: ",pts.size())
                visualise(pts, col)
            #asdf
            
            loss = (energies_gt - energies_out).square().mean() + (energies_gt - energies_out).abs().mean()

            avg_loss     += loss.item()

            loss.backward()
            
            optimiser_cum.step()
            its += 1
        avg_loss /= its
        print("DONE WITH EPOCH ",epoch,"/",its," TO LEARN CUMULATIVE ERROR!")
        print("\tAVERAGE LOSS: ",avg_loss)

        if best_loss == None or avg_loss < best_loss:
            print("\t--> new best, saving...")
            best_loss = avg_loss
            torch.save(cumulative_net, PATH_CUM_ENERGY_NET)

        if epoch % 25 == 0:
            
            with torch.no_grad():
                test_pts = samples_surface.clone().to(DEVICE)#sample_bb(10000, get_offset=False)
                indices = torch.randperm(test_pts.size()[0])[0:25000]
                test_pts = test_pts[indices]

                energy = energy_net(test_pts)
                energy -= energy.min()
                energy /= energy.max()
                energy = energy.repeat(1,3)
                energy[:,1:] *= 0.0

                cum_energy = cumulative_net(test_pts)
                cum_energy -= cum_energy.min()
                cum_energy /= cum_energy.max()
                cum_energy = cum_energy.repeat(1,3)
                cum_energy[:,1:] *= 0.0
                
                visualise(test_pts, (energy + 0.0001) ** 1.5, 0.05)
                visualise(test_pts, (cum_energy + 0.0001) ** 1.5, 0.05)
                pass
best_loss = None

# %%
def output_mesh(target_path):
    f = open(OBJECT_TO_LOAD, "r")
    pos_x = []
    pos_y = []
    pos_z = []
    it = 0
    for line in f:
        it += 1
        if it == 2:
            compute_until = 2 + int(line.split(" ")[0])
        if it >= 3 and it < compute_until:
            pos_x.append(float(line.split(" ")[0]))
            pos_y.append(float(line.split(" ")[1]))
            pos_z.append(float(line.split(" ")[2]))
        
    f.close()
    #write new file:
    f = open(OBJECT_TO_LOAD, "r")
    ff = open(target_path, "w")
    scale = max((max(pos_x) - min(pos_x)), (max(pos_y) - min(pos_y)), (max(pos_z) - min(pos_z)))
    it = 0
    for line in f:
        it += 1

        if it == 2:
            compute_until = 2 + int(line.split(" ")[0])
        if it >= 3 and it <= compute_until:
            x = float(line.split(" ")[0])
            y = float(line.split(" ")[1])
            z = float(line.split(" ")[2])

            pt = torch.tensor([x, y, z]).view(1, 3).cuda()
            offset = deform_net(pt)
            pt[:,AXIS_TO_OFFSET] += offset[:,0]
            pt = pt.view(-1)
            
            ff.write(str(pt[0].item()) + " "+str(pt[1].item())+" "+str(pt[2].item()) + "\n")
        else:
            ff.write(line)
    ff.close()
    f.close()

# %% [markdown]
# <h1>Retargeting itself</h1>

# %%
deform_net = torch.load(PATH_DEFORMATION_INIT)
energy_net = torch.load(PATH_ENERGY_NET)
cumulative_net = torch.load(PATH_CUM_ENERGY_NET)
inverse_deform_net = torch.load(PATH_INV_DEFORMATION_INIT)

deform_net.train(False)
energy_net.train(False)
cumulative_net.train(False)
inverse_deform_net.train(False)

optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.000001)
optimiser_inverse_deform = torch.optim.AdamW(inverse_deform_net.parameters(), lr=0.001, weight_decay=0.000001)
deform_net.train(True)

offset_range = TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET]
offset_range /= 100

relu = torch.nn.ReLU()

EPSILON = 0.000001
MAX_STRETCH_FACTOR = SCALING_FACTOR

ptc_pts_in = samples_surface.to(DEVICE)
ptc_nrg_in = energy_net(ptc_pts_in.clone())[:,0].detach()

print("DEFORMATION WITHOUT: ")
show_deformation(apply=False)
print("DEFORMATION INIT: ")
show_deformation(apply=True)

output_mesh("out/stretched_"+str(SCALING_FACTOR)+".off")

for epoch in range(0, 100):
    #a) Monotonicity:
    #   along an axis, make sure that we do not jump in different directions (=only ever increase the offset, do not decrease it)
    #   (3D: along the normal of a plane, only ever increase)

    #b) Boundaries:
    #   make sure the boundaries are at their target values
    #   meaning: they define the deformation frame (=front and back side of the cube are fixed)

    #c) Gradients:
    #   make sure gradient(deformation) TIMES energy is minimal

    #d) Shearing:
    #   make sure we do not allow any shearing

    #e) Shrinking:
    #   make sure we do not shrink too much (=deformation between two points should have a maximum)

    avg_loss                    = 0.0 # SUM [a to e]

    avg_loss_mono               = 0.0 #a) 
    avg_loss_boundaries         = 0.0 #b)
    avg_loss_gradients          = 0.0 #c)
    avg_loss_shearing           = 0.0 #d)
    avg_loss_cap_deformation    = 0.0 #e)

    avg_distance_inverting_offset = 0.0

    lambdas = dict()
    lambdas['mono']   =  10000.0# * 0.0
    lambdas['bound']  =    100.0# * 0.0
    lambdas['grad']   =     10.0 * 0.1# * 0.0
    lambdas['cap']    =      0.0
    lambdas['shear']  =      1.0 * 1.0# * 0.0

    iterations_for_optimisation = 100

    pts_on_surface = samples_surface.clone().to(DEVICE)
    pts_on_surface = torch.cat((pts_on_surface, sample_bb(pts_on_surface.size()[0], get_offset=False)),0)
    
    offset_range = TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET]
    offset_range /= 100

    for step in range(0, iterations_for_optimisation):
        optimiser.zero_grad()

        loss_mono = torch.zeros(1, device=DEVICE)
        loss_gradients = torch.zeros(1, device=DEVICE)
        loss_boundaries = torch.zeros(1, device=DEVICE)
        loss_shearing = torch.zeros(1, device=DEVICE)
        loss_cap_deformation = torch.zeros(1, device=DEVICE)

        #a) monotonicity
        if True:
            pts_a = pts_on_surface.clone()
            pts_b = pts_on_surface.clone()
            
            offset = torch.zeros_like(pts_b)
            offset[:,AXIS_TO_OFFSET] = torch.rand_like(offset[:,AXIS_TO_OFFSET]) * offset_range + offset_range * 0.01
            pts_b = pts_b + offset
            #TODO: switch for expansion
            if SCALING_FACTOR < 1.0:
                loss_mono = (relu(deform_net(pts_b) - deform_net(pts_a)) / offset.sum(dim=1)[:,None]).mean()
            else:
                loss_mono = (relu(deform_net(pts_a) - deform_net(pts_b)) / offset.sum(dim=1)[:,None]).mean()
        #b) gradients
        if True:
            pts_a = pts_on_surface.clone()
            pts_b = pts_on_surface.clone()
            
            offset = torch.zeros_like(pts_b)
            offset_magnitude = 0.01#torch.rand_like(offset[:,AXIS_TO_OFFSET]) * offset_range + offset_range
            offset[:,AXIS_TO_OFFSET] = offset_magnitude
            pts_b = pts_b + offset

            deformation_magnitude = (deform_net(pts_a) - deform_net(pts_b)).abs()[:,0] / offset_magnitude

            energy = (cumulative_net(pts_a.clone())[:,0] - cumulative_net(pts_b.clone())[:,0]).abs() / offset_magnitude
            
            loss_gradients = (deformation_magnitude * energy.detach()).square().mean()

        #c) boundaries
        if True:
            samples, target_pos = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, get_offset=True, boundaries_only=True)
            deformed_samples = deform_net(samples)
            loss_boundaries = ((deformed_samples[:,0] + samples[:,AXIS_TO_OFFSET]) - target_pos[:,AXIS_TO_OFFSET]).square().mean()

        #d) shearing
        if True:
            pts_a = pts_on_surface.clone()
            pts_b = pts_on_surface.clone()
            pts_c = pts_on_surface.clone()

            offset_b = torch.zeros_like(pts_b)
            offset_c = torch.zeros_like(pts_b)

            #offset both other axis
            it = 0
            for ax in range(0, 3):
                if ax == AXIS_TO_OFFSET:
                    continue
                else:
                    if it == 0:
                        offset_b[:,ax] += offset_magnitude
                    elif it == 1:
                        offset_c[:,ax] += offset_magnitude

                    it += 1
            
            pts_b += offset_b
            pts_c += offset_c
            
            offset_a = deform_net(pts_a)
            offset_b = deform_net(pts_b)
            offset_c = deform_net(pts_c)

            loss_shearing = ((offset_a - offset_b).abs() / offset_magnitude).mean() + ((offset_a - offset_c).abs() / offset_magnitude).mean()


        #e) cap for deformation (expansion only)
        ### TODO
            
        loss =  loss_mono * lambdas['mono'] +\
                loss_gradients * lambdas['grad'] +\
                loss_boundaries * lambdas['bound'] +\
                loss_shearing * lambdas['shear'] +\
                loss_cap_deformation * lambdas['cap']

        avg_loss_mono += loss_mono.item()
        avg_loss_boundaries += loss_boundaries.item()
        avg_loss_gradients += loss_gradients.item()
        avg_loss_shearing += loss_shearing.item()
        avg_loss_cap_deformation += loss_cap_deformation.item()
        avg_loss += loss.item()

        loss.backward()

        optimiser.step()

    print("*** DONE WITH EPOCH ",epoch," ***")
    print("\t\inverse offset accuracy: ",avg_distance_inverting_offset/iterations_for_optimisation)
    print("\tavg error: ",avg_loss/float(iterations_for_optimisation)," - components: ")
    print("\t\t",lambdas['mono']," * ",avg_loss_mono/float(iterations_for_optimisation)," = ",lambdas['mono']*avg_loss_mono/float(iterations_for_optimisation),"(mono)")
    print("\t\t",lambdas['bound']," * ",avg_loss_boundaries/float(iterations_for_optimisation)," = ",lambdas['bound'] * avg_loss_boundaries/float(iterations_for_optimisation))
    print("\t\t",lambdas['grad']," * ",avg_loss_gradients/float(iterations_for_optimisation)," = ",lambdas['grad']*avg_loss_gradients/float(iterations_for_optimisation),"(grad)")
    print("\t\t",lambdas['cap']," * ",avg_loss_cap_deformation/float(iterations_for_optimisation)," = ",lambdas['cap']*avg_loss_cap_deformation/float(iterations_for_optimisation),"(cap)")
    print("\t\t",lambdas['shear']," * ",avg_loss_shearing/float(iterations_for_optimisation)," = ",lambdas['shear']*avg_loss_shearing/float(iterations_for_optimisation),"(shear)")
    # loss_beginning + loss_ending + loss_others + loss_outside

    if (epoch % 5 == 0 and epoch != 0) or True:
        print("OPTIMISED DEFORMATION AFTER ",epoch,":")
        show_deformation(apply=True)
    
    output_mesh("out/optimised_"+str(epoch)+"_"+str(SCALING_FACTOR)+".off")

# %%


# %% [markdown]
# 


