#debug: centre mesh & set BB accordingly
f = open(OBJECT_TO_LOAD, "r")

pos_x = []
pos_y = []
pos_z = []
for line in f:
    if line[0:2] == "v ":
        pos_x.append(float(line.split(" ")[1]))
        pos_y.append(float(line.split(" ")[2]))
        pos_z.append(float(line.split(" ")[3]))
f.close()

#write new file:
f = open(OBJECT_TO_LOAD, "r")
ff = open("mesh/chair_0007_centred.obj", "w")
scale = max((max(pos_x) - min(pos_x)), (max(pos_y) - min(pos_y)), (max(pos_z) - min(pos_z)))
for line in f:
    if line[0:2] == "v ":
        x = (float(line.split(" ")[1]) - min(pos_x)) / (scale) - 0.5
        y = (float(line.split(" ")[2]) - min(pos_y)) / (scale) - 0.5
        z = (float(line.split(" ")[3]) - min(pos_z)) / (scale) - 0.5
        
        ff.write("v " + str(x) + " "+str(y)+" "+str(z) + "\n")
    else:
        ff.write(line)
ff.close()
f.close()



# for off:
#debug: centre mesh & set BB accordingly
f = open("mesh/chair_0007.off", "r")
pos_x = []
pos_y = []
pos_z = []
it = 0
for line in f:
    it += 1
    if it >= 3 and it < 163:
        pos_x.append(float(line.split(" ")[0]))
        pos_y.append(float(line.split(" ")[1]))
        pos_z.append(float(line.split(" ")[2]))
    
f.close()
#write new file:
f = open("mesh/chair_0007.off", "r")
ff = open("mesh/chair_0007_centred.off", "w")
scale = max((max(pos_x) - min(pos_x)), (max(pos_y) - min(pos_y)), (max(pos_z) - min(pos_z)))
it = 0
for line in f:
    it += 1
    if it >= 3 and it < 163:
        x = (float(line.split(" ")[0]) - min(pos_x)) / (scale) - 0.5
        y = (float(line.split(" ")[1]) - min(pos_y)) / (scale) - 0.5
        z = (float(line.split(" ")[2]) - min(pos_z)) / (scale) - 0.5
        
        ff.write(str(x) + " "+str(y)+" "+str(z) + "\n")
    else:
        ff.write(line)
ff.close()
f.close()












f = open(OBJECT_TO_LOAD, "r")
vs = 0
for line in f:
    if line[0:2] == "v ":
        vs += 1
f.close()

f = open(OBJECT_TO_LOAD, "r")
vertices = torch.zeros(vs, 3)
vs = 0
for line in f:
    if line[0:2] == "v ":
        vertices[vs] = torch.tensor([float(line.split(" ")[1]), float(line.split(" ")[2]), float(line.split(" ")[3])])
        vs += 1
f.close()

def flatness(points):
    points = points.clone()

    #centre
    centre = points.mean(dim=0)
    points -= centre[None]
    #covariance matrix
    cov = torch.cov(points.transpose(0,1), correction=1)
    #get eigenvalues
    return torch.linalg.eig(cov)[0].sum().real

pts = torch.rand(20, 3)
pts[:,0] *= 0.0
flatness(pts)