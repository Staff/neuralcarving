import torch
import torchvision

try:
    _ = __IPYTHON__
    from tqdm.notebook import tqdm
except Exception:
    from tqdm import tqdm


DEFAULT_READ_MODE = torchvision.io.ImageReadMode.RGB
DEFAULT_RESIZE_MODE = torchvision.transforms.InterpolationMode.BILINEAR


def load_image(path, mode=DEFAULT_READ_MODE):
    image = torchvision.io.read_image(path, mode)
    image = image.permute(1, 2, 0)  # CHW -> HWC
    image = image.float() / 255 # [0, 255] -> [0., 1.]
    return image


def scale_image(image, scale_factor, mode=DEFAULT_RESIZE_MODE):

    assert len(image.shape) == 3 # HWC

    new_height = int(image.shape[0] * scale_factor)
    new_width = int(image.shape[1] * scale_factor)

    image = image.permute(2, 0, 1) # HWC -> CHW
    image = torchvision.transforms.functional.resize(image,
                                                     size=(new_height, new_width),
                                                     interpolation=mode,
                                                     antialias=True,)
    image = image.permute(1, 2, 0) # CHW -> HWC

    return image

