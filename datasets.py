import torch
import numpy as np
import os
import json
from glob import glob

try:
    _ = __IPYTHON__
    from tqdm.notebook import tqdm
except Exception:
    from tqdm import tqdm

from .images import load_image, scale_image
from ..math3d import eulers_to_rotation


def load_default_data(dataset_path, split=None, image_scale=1.0, sort_frames=True):
    """ Can be used to load default blender datasets """

    if split is None:
        transform_path = f'{dataset_path}/transforms.json'

        if not os.path.exists(transform_path):
            print('Could not find transforms.json, trying to load train and val splits.')

            train_images, train_c2ws, (focal_x, focal_y) = load_default_data(dataset_path, split='train', image_scale=image_scale)
            val_images, val_c2ws, (focal_x, focal_y) = load_default_data(dataset_path, split='val', image_scale=image_scale)

            print(f'Success, train size is {len(train_images)} and val size is {len(val_images)}.')

            images = torch.cat((train_images, val_images))
            c2ws = torch.cat((train_c2ws, val_c2ws))

            return images, c2ws, (focal_x, focal_y)
    else:
        transform_path = f'{dataset_path}/transforms_{split}.json'

    with open(transform_path, 'r') as f:
        meta = json.load(f)

    images = []
    c2ws = []

    filetypes = ['png', 'jpg', 'jpeg']

    if sort_frames:
        # Sort frames by filename to preserve order
        frames = sorted(meta['frames'], key=lambda d: d['file_path'])

    for frame in tqdm(frames, desc='Loading data'):

        # Read image

        image_path = frame['file_path']
        image_path = f'{dataset_path}/{image_path}'

        for ft in filetypes:
            if image_path.endswith(ft):
                break
            elif os.path.isfile(image_path + '.' + ft):
                image_path += '.' + ft
                break

        image = load_image(image_path)

        if image_scale != 1.0:
            image = scale_image(image, image_scale)

        images.append(image)

        # Extract pose

        transform_key = 'transform_matrix' if 'transform_matrix' in frame else 'transformation_matrix'

        c2w = torch.tensor(frame[transform_key],
                           dtype=torch.float)
        
        c2ws.append(c2w)

    images = torch.stack(images)
    c2ws = torch.stack(c2ws)

    height = images.shape[1]
    width = images.shape[2]

    if 'fl_x' in meta:
        focal_x = float(meta['fl_x'])
        focal_y = float(meta['fl_y']) if 'fl_y' in meta else focal_x

        if image_scale != 1.0:
            focal_x *= image_scale
            focal_y *= image_scale

    else:
        angle_x = float(meta['camera_angle_x'])
        angle_y = float(meta['camera_angle_y']) if 'camera_angle_y' in meta else angle_x

        # NOTE: focal length scaling is also handled by image resizing
        focal_x = 0.5 * width / np.tan(angle_x / 2)
        focal_y = 0.5 * height / np.tan(angle_y / 2)

    return images, c2ws, (focal_x, focal_y)


def load_gta_v_data(path, image_scale=1.0, static=False, image_format='png'):

    images = []
    c2ws = []
    timestamps = []

    meta_files = sorted(glob(path + '/*.meta'))

    views_per_step = 0

    for meta_file in tqdm(meta_files, 'Loading GTA V data'):

        base_path = meta_file[:-len('.meta')]

        views_per_step = max(views_per_step, int(base_path.split('-')[-1]) + 1)

        # Load image
        image_file = base_path + ('-static' if static else '') + '.' + image_format
        image = load_image(image_file)

        if image_scale != 1.0:
            # NOTE: focal length scaling is also handled by image resizing
            image = scale_image(image, image_scale)

        images.append(image)

        # Load and parse meta
        with open(meta_file, 'r') as f:
            meta_raw = f.read()

        meta_raw = meta_raw.replace(',', '.')
        
        xyz_raw, pry_raw, nffrs_raw, rt_raw = meta_raw.strip().split('\n')

        get_floats = lambda string: [float(s) for s in string.split()]

        # Process extrinsics

        x, y, z = get_floats(xyz_raw)
        pitch, roll, yaw = get_floats(pry_raw)

        t = torch.tensor([x, y, z], dtype=torch.float)
        R = eulers_to_rotation(torch.tensor([pitch + 90, roll, yaw], dtype=torch.float))#rot_z(yaw) @ rot_y(roll) @ rot_x(pitch + 90)

        c2w = torch.cat([R, t[:, None]], -1)
        c2ws.append(c2w)

        # Process intrinsics

        near, far, fovy, raspect, saspect = get_floats(nffrs_raw)

        assert raspect == saspect # Render and screen aspect ratios must match

        height, width = image.shape[0], image.shape[1]
        focal_y = 0.5 * height / np.tan(np.deg2rad(fovy) / 2)
        focal_x = focal_y

        # Process extra data

        rendertime, timestamp = rt_raw.split()[:2]
        timestamp = int(timestamp)

        timestamps.append(timestamp)

    # Combine tensors
    images = torch.stack(images)
    c2ws = torch.stack(c2ws)
    timestamps = torch.tensor(timestamps, dtype=torch.float)

    # Reshape to match step+view format
    images = images.unflatten(0, (-1, views_per_step))
    c2ws = c2ws.unflatten(0, (-1, views_per_step))
    timestamps = timestamps.unflatten(0, (-1, views_per_step))
    
    return images, c2ws, (focal_x, focal_y), timestamps