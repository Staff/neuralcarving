
# %%
RECOMPUTE_ALL = False

# %%
try:
    _ = __IPYTHON__
    IN_JUPYTER_NOTEBOOK = True
except Exception:
    IN_JUPYTER_NOTEBOOK = False

print(f'{IN_JUPYTER_NOTEBOOK = }')

# %%
import torch
from torch import nn
import torchvision

import numpy as np
from matplotlib import pyplot as plt

import sys
import os
import random

if IN_JUPYTER_NOTEBOOK:
    from tqdm.notebook import tqdm
else:
    from tqdm import tqdm

# %%
from src.commandline import parse_args, args_to_string, get_view_idx_string, get_free_gpu_memory
from src.dataset.gtav import load_gta_v_data
from src.dataset.blender import load_blender_data
from src.dataset.image import scale_images
from src.dataset.rays import get_ray_dataloader, estimate_scene_normalization
from src.model.encoding import PositionalEncoding, IntegratedPositionalEncoding, SphericalHarmonicsEncoding
from src.model.encoding import RaySegmentToMidpointWrapper, MultiResolutionHashGridEncoding, MultiResolutionHashGridEncodingTCNN
from src.model.encoding import contract_fn
from src.model.model import RadianceField, RadianceFieldAsDensityField, DensityField, LambdaLayer, softplus_mod, sigmoid_mod
from src.model.scene import NeRFScene, run_scene, unflatten_estims, ConstantBackground
from src.visualization import show_estims, show_history

from src.victor.datasets import load_default_data
from helpers import visualise

# %%
#loadable settings

# %%
scenes_available = ['beach', 'farm', 'harbor', 'seminar']

# %%
LOWREST = False
SCENE_TO_DO = scenes_available[2]
SCALING_FACTOR = 0.5 # 0.5 = scale to half
AXIS_TO_OFFSET = 1 #x axis

# %%
if not IN_JUPYTER_NOTEBOOK:
    import sys
    for arg in sys.argv[1:]:
        if arg.startswith('axis='):
            AXIS_TO_OFFSET = int(arg.split('=')[1])
        if arg.startswith('retarget_to='):
            SCALING_FACTOR = float(arg.split('=')[1])
        if arg.startswith('scene='):
            SCENE_TO_DO = scenes_available[int(arg.split('=')[1])]

# %%
print("SCENE TO DO   : ",SCENE_TO_DO)
print("SCALING FACTOR: ",SCALING_FACTOR)
print("AXIS_TO_OFFSET: ",AXIS_TO_OFFSET)

# %%
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# %%
# Fix for RuntimeError: Too many open files. Communication with the workers is no longer possible.
### CHANGED ###
#import torch.multiprocessing
#torch.multiprocessing.set_sharing_strategy('file_system')

# %%
# --- Arguments to use if run from notebook ---

# Predefined (partial) setups

hash_enc_args = [
    '--learningrate 1e-2',
    '--lrdecaysteps 15000',
    '--lrdecayrate 0.5',
    '--weightdecay 1e-6',

    '--posenc hash',
    '--hiddensize 64',
    '--blocks 1',
    '--blocksize 2',

    '--epochs 10',
    '--saveevery 1',

    '--tablesize 19',
]

pe_enc_args = [
    '--learningrate 5e-4',

    '--posenc pe',
    '--posres 14',

    '--epochs 100',
    '--saveevery 10',
]

ipe_enc_args = [
    '--learningrate 5e-4',

    '--posenc ipe',
    '--posres 14',

    '--epochs 100',
    '--saveevery 10',
]

synthetic_lego_args = [
    '--dataset /clusterarchive/NeRF/nerf_synthetic/lego',
    '--scenescale 0.25',
    '--near 0.5',
    '--far 1.5',
    '--valviews ' + get_view_idx_string(100, 130),
    '--posres 12',
]

gtav_beach_20m_args = [
    '--dataset /clusterarchive/NeRF/gta-v/gta-v-beach-20m',
    '--scenecenter -1384.30 -1550.35 13.46',
    '--scenescale 0.01',
    '--near 0.0001',
    #'--far 0.6',
    '--trainviews ' + get_view_idx_string(0, 380),
    '--valviews ' + get_view_idx_string(380, 400),
    '--posres 16',
]

gtav_farm_60m_args = [
    '--dataset /clusterarchive/NeRF/gta-v/gta-v-farm-60m',
    '--scenecenter 1941.02 4636.21 72.70',
    '--scenescale 0.004',
    '--near 0.00001',
    #'--far 0.6',
    '--trainviews ' + get_view_idx_string(0, 380),
    '--valviews ' + get_view_idx_string(380, 400),
    '--posres 18',
    '--distreg 0.001',
]

gtav_harbor_50m_args = [
    '--dataset /clusterarchive/NeRF/gta-v/gta-v-harbor-50m',
    '--scenecenter 844.53 -3091.99 32.76',
    '--scenescale 0.004',
    '--near 0.00001',
    #'--far 0.6',
    '--trainviews ' + get_view_idx_string(0, 380),
    '--valviews ' + get_view_idx_string(380, 400),
    '--posres 18',
    '--distreg 0.001',
]

gtav_filmset_cameraman_args = [
    '--dataset /clusterarchive/NeRF/gta-v/gta-v-filmset-cameraman',
    '--scenecenter -1184.51 -496.44 37.34',
    '--scenescale 0.01',
    '--near 0.0001',
    #'--far 0.6',
    '--trainviews ' + get_view_idx_string(0, 19),
    '--valviews ' + get_view_idx_string(19, 20),
    '--posres 16',
]


if False:
    gtav_harbour_args = [
        '--dataset /clusterarchive/NeRF/gta-v/gta-v-harbor-50m',
        '--scenecenter -1184.51 -496.44 37.34',
        '--scenescale 0.01',
        '--near 0.0001',
        #'--far 0.6',
        '--trainviews ' + get_view_idx_string(0, 19),
        '--valviews ' + get_view_idx_string(19, 20),
        '--posres 16',
    ]

gtav_real_args = [
    '--dataset /clusterarchive/NeRF/gta-v/gta-v-harbor-50m',
    '--scenecenter -1184.51 -496.44 37.34',
    '--scenescale 0.001',
    '--near 0.0001',
    #'--far 0.6',
    '--trainviews ' + get_view_idx_string(0, 19),
    '--valviews ' + get_view_idx_string(19, 20),
    '--posres 16',
]


seminar_args = [
    '--dataset /clusterarchive/NeRF/drone-footage/seminar',
    #'--scenecenter -1184.51 -496.44 37.34',
    '--scenescale 0.01',
    '--near 0.0001',
    '--far 2.0',
    #'--contract',
    ##'--trainviews ' + get_view_idx_string(0, 19),
    ##'--valviews ' + get_view_idx_string(19, 20),
    '--trainviews ' + get_view_idx_string(0, 113),
    '--valviews ' + get_view_idx_string(0, 113, 10),
    '--posres 16',
    '--distreg 0.001',
]

unbound_args = [
    '--contract',
    '--far 2',
]

# -- Build arguments here --

nb_args = []

nb_args += hash_enc_args

imgscale = 1.0
#if LOWREST:
#    imgscale = 0.25

if SCENE_TO_DO == 'beach':
    nb_args += gtav_beach_20m_args
    nb_args += [
        '--batchsize 8192',
        '--imagescale '+str(imgscale),
    ]
elif SCENE_TO_DO == 'farm':
    nb_args += gtav_farm_60m_args
    nb_args += [
        '--batchsize 8192',
        '--imagescale '+str(imgscale),
    ]
elif SCENE_TO_DO == 'harbor':
    nb_args += gtav_harbor_50m_args
    nb_args += [
        '--batchsize 8192',
        '--imagescale 0.3',
    ]
elif SCENE_TO_DO == 'seminar':
    nb_args += seminar_args
    nb_args += [
        '--batchsize 8192',
        '--imagescale 0.5',
    ]
else:
    nb_args += [
        #'--posenc tcnnhash', # NOTE: uncomment if tinycudann is installed for large speedup
        #'--background skybox', # NOTE: optional, --contract gives better density values
        '--batchsize 8192',
        #'--imagescale 0.5',
        #'--imagescale 1.0',#0.1',
        #'--imagescale 1.0',#0.1',
        #'--imagescale 0.1', #<-- seminar
        #'--imagescale 0.5', #<-- seminar
        '--imagescale 0.3',
    ]

nb_args += unbound_args # NOTE: for largescale scenes like the GTA ones, uncomment this! maps [-inf, inf] to [-1, 1]

# -- End build arguments here --

# ---------------------------------------------

nb_args = ' '.join(nb_args).split()

#if IN_JUPYTER_NOTEBOOK:
# Delete jupyter stuff from args
sys.argv = ['Jupyter Notebook'] + nb_args

# Parse and print args
args = parse_args()

header = '--- Arguments ---'
print(header + '\n' + args_to_string(args) + '\n' + '-' * len(header))

# %% [markdown]
# <h1> Settings NeRF Stuff ABOVE </h1>

# %%
# Make project folder

os.makedirs(args.saveckpt, exist_ok=True)
print('Running in folder', args.saveckpt)

# %%
# Load data

if 'seminar' in args.dataset:
    images, c2ws, (focal_x, focal_y), *_ = load_default_data(args.dataset)
    images = images[None]
    c2ws = c2ws[None] 

elif 'gta-v' in args.dataset:
    images, c2ws, (focal_x, focal_y), *_ = load_gta_v_data(args.dataset)

elif 'nerf_synthetic' in args.dataset:
    train_images, train_c2ws, (focal_x, focal_y) = load_blender_data(args.dataset, split='train')
    val_images, val_c2ws, *_ = load_blender_data(args.dataset, split='val')

    num_train = len(train_images)
    num_val = len(val_images)

    images = torch.cat((train_images, val_images))[None]
    c2ws = torch.cat((train_c2ws, val_c2ws))[None] 

    if args.trainviews is None:
        args.trainviews = list(range(num_train))
        print(f'Using original trainviews from {args.trainviews[0]} to {args.trainviews[-1]}')

    if args.valviews is None:
        args.valviews = list(range(num_train, num_train + num_val))
        print(f'Using original valviews from {args.valviews[0]} to {args.valviews[-1]}')

else:
    assert False, f'{args.dataset = } not supported'

print(f'Loaded dataset with initial {images.shape = }')

# %%
estimators = estimate_scene_normalization(c2ws, 
                                          images.shape[-3], 
                                          images.shape[-2], 
                                          focal_x, 
                                          focal_y)
print('---- Estimators for scene normalization:', *estimators.items(), '-----', sep='\n')

# %%
# Scale images

images = scale_images(images, args.imagescale)

focal_x *= args.imagescale
focal_y *= args.imagescale

print(f'Final {images.shape = } ({focal_x = }, {focal_y = })')

num_steps, num_views, height, width, num_channels = images.shape
assert num_channels == 3

# %%
# Center and scale scene

c2ws[..., :3, -1] -= torch.tensor(args.scenecenter)
print(f'Applied scene centering by {args.scenecenter = }.')

c2ws[..., :3, -1] *= args.scenescale
print(f'Applied scene scaling by {args.scenescale = }.')

# %%
# Set train and validation split if not given

if args.trainsize is None:
    args.trainsize = len(images) - args.trainstart
    print(f'Automatic {args.trainsize = }')

if args.trainviews is None:
    args.trainviews = list(range(num_views))
    print(f'Automatic {args.trainviews = }')

if args.valviews is None:
    args.valviews = args.trainviews[::10]
    print(f'Automatic {args.valviews = }')

# %%
def get_encoder(name, 
                resolution, 
                log2_hash_table_size=19, 
                num_levels=16,
                encode_segments=True, 
                contract=False,):

    if name == 'pe':
        encoder = PositionalEncoding(resolution)

        if encode_segments:
            encoder = RaySegmentToMidpointWrapper(encoder, (lambda x: contract_fn(x, 2) / 2) if contract else None)

        enc_size = 2 * 3 * resolution

    elif args.direnc =='sh':
        encoder = SphericalHarmonicsEncoding(args.dirres)
        enc_size = resolution ** 2

    elif name == 'hash':
        encoder = MultiResolutionHashGridEncoding(num_features_in=3,
                                                  num_levels=num_levels, 
                                                  log2_hash_table_size=log2_hash_table_size,
                                                  max_resolution=2 ** resolution)

        if encode_segments:
            encoder = RaySegmentToMidpointWrapper(encoder, (lambda x: contract_fn(x, torch.inf) / 2) if contract else None)

        enc_size = 2 * num_levels

    elif name == 'tcnnhash':
        encoder = MultiResolutionHashGridEncodingTCNN(num_features_in=3,
                                                      num_levels=num_levels, 
                                                      log2_hash_table_size=log2_hash_table_size,
                                                      max_resolution=2 ** resolution)
        if encode_segments:
            encoder = RaySegmentToMidpointWrapper(encoder, (lambda x: contract_fn(x, torch.inf) / 2) if contract else None)

        enc_size = 2 * num_levels

    else:
        assert False, f'Encoding {name} is unknown (NOTE: mip needs rays_d normalization adjustment)'

    return encoder, enc_size

# %%
activations = {
    'activation': nn.ReLU,
    'density_activation': lambda x: torch.exp(x - 1),
    'color_activation': sigmoid_mod,
}

# %%
# Build density field

d_poss_encoder, d_poss_enc_size = get_encoder(args.posenc, 
                                              args.posres - 2, 
                                              log2_hash_table_size=args.tablesize - 2, 
                                              num_levels=args.gridlevels // 2,
                                              contract=args.contract)

density_field = DensityField(d_poss_encoder, 
                             d_poss_enc_size,
                             hidden_size=args.hiddensize,
                             **activations)

# %%
# Build radiance field

r_pos_encoder, r_pos_enc_size = get_encoder(args.posenc, 
                                            args.posres, 
                                            log2_hash_table_size=args.tablesize, 
                                            num_levels=args.gridlevels,
                                            contract=args.contract)

r_dir_encoder, r_dir_enc_size = get_encoder(args.direnc, args.dirres, encode_segments=False)

radiance_field = RadianceField(pos_encoder=r_pos_encoder,
                               pos_enc_size=r_pos_enc_size,
                               dir_encoder=r_dir_encoder,
                               dir_enc_size=r_dir_enc_size,
                               hidden_size=args.hiddensize,
                               num_blocks=args.blocks,
                               block_size=args.blocksize,
                               **activations)

# %%
# Background model

if args.background is None:
    background_model = None

elif args.background == 'white':
    background_model = ConstantBackground(torch.tensor([1, 1, 1]))

elif args.background == 'black':
    background_model = ConstantBackground(torch.tensor([0, 0, 0]))

elif args.background == 'skybox':
    bgencoder, bgenc_size = get_encoder('pe', 6, segment2midpoint=False)
    background_model = nn.Sequential(bgencoder,
                                     nn.Linear(bgenc_size, 32), 
                                     nn.LeakyReLU(), 
                                     nn.Linear(32, 32), 
                                     nn.LeakyReLU(), 
                                     nn.Linear(32, 3), 
                                     LambdaLayer(lambda x: sigmoid_mod(x)))
else:
    assert False, f'{args.background = } unknown'

# %%
# Full scene
scene = NeRFScene(radiance_fields=[radiance_field],
                  num_samples=[args.samples[-1]],
                  density_fields=[density_field],
                  num_density_samples=[args.samples[0]],
                  near=args.near,
                  far=args.far,
                  background_model=background_model,)

num_params = sum(p.numel() for p in scene.parameters() if p.requires_grad)
print(f'Scene has {num_params} trainable parameters.')

# Multi GPU

free_mem = get_free_gpu_memory()
assert len(free_mem) >= args.gpus

gpu_ids = free_mem.argsort(descending=True)[:args.gpus]
print(f'Found free GPU memory {free_mem.tolist()} - choosing devices {gpu_ids.tolist()}.')

scene = scene.cuda()
scene = nn.DataParallel(scene, gpu_ids)

print('\n\n Full scene module:\n\n', scene)

# %%
# Optimizer

# Exclude encoder entries from weight decay (see INGP for reference)
param_groups = [
    {
        'params': [param for name, param in scene.named_parameters() if 'encoder' in name],
        'weight_decay': 0,
    },
    {
        'params': [param for name, param in scene.named_parameters() if 'encoder' not in name],
        'weight_decay': args.weightdecay,
    }
]

print('Params without weight decay:', len(param_groups[0]['params']))

# Betas and eps from https://nvlabs.github.io/instant-ngp/assets/mueller2022instant.pdf
optim = torch.optim.Adam(
    param_groups, 
    lr=args.learningrate,
    eps=1e-15, 
    betas=[0.9, 0.99], 
)

if args.lrdecaysteps is not None:
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optim, 
                                                   args.lrdecaysteps, 
                                                   args.lrdecayrate)
    print(f'Using LR decay with stepsize {args.lrdecaysteps} and rate {args.lrdecayrate}')
else:
    lr_scheduler = None

# These are initialized with model
history = {'train': [], 'val': [], 'final_epochs': []}

# %%
if args.loadckpt is not None:
    
    ckpt = torch.load(args.loadckpt)

    scene.load_state_dict(ckpt['scene'])
    optim.load_state_dict(ckpt['optim'])

    print(f'Loaded checkpoint {args.loadckpt}')

    if ckpt['lr_scheduler'] is not None:
        print(f'Warning, scheduler at step {lr_scheduler.step} is not loaded. Please setup manually!')

# %%
def save_checkpoint(path):

    global scene, optim, history

    ckpt = {
        'scene': scene.state_dict(),
        'optim': optim.state_dict(),
        'history': history,
    }

    if lr_scheduler is not None:
        ckpt['lr_scheduler'] = lr_scheduler.state_dict()

    torch.save(ckpt, path)

# %%
def get_dataloader(step_idx=None, view_idx=None, shuffle=False, patch_size=1, c2ws=None, val_topdown=False):

    if c2ws is None:
        c2ws = globals()['c2ws']
    
    return get_ray_dataloader(images,
                              c2ws,
                              focal_x,
                              focal_y,
                              step_idx,
                              view_idx,
                              shuffle=shuffle,
                              batch_size=args.batchsize,
                              #workers=args.workers,
                              ### CHANGED ###
                              workers=0,
                              patch_size=patch_size, val_topdown=False)

# %% [markdown]
# <h1>Train NeRF</h1>

# %%
SCENE_NAME = nb_args[23].split("/")[-1]
IMG_SCALE = args.imagescale
SCENE_NAME, IMG_SCALE

# %%
NERF_IT_TO_LOAD = 999
SAVEPATH_OUT_IMAGES = None

#if this is beach:
if 'beach' in SCENE_NAME:
    NERF_IT_TO_LOAD = 3
    if IMG_SCALE == 1.0:
        NERF_IT_TO_LOAD = 2
elif 'filmset' in SCENE_NAME:
    NERF_IT_TO_LOAD = 7
elif 'farm' in SCENE_NAME:
    NERF_IT_TO_LOAD = 5
    if IMG_SCALE == 0.25:
        NERF_IT_TO_LOAD = 9
elif 'harbor' in SCENE_NAME:
    NERF_IT_TO_LOAD = 4 #1 for good
elif 'seminar' in SCENE_NAME:
    NERF_IT_TO_LOAD = 9

# %%
if not os.path.exists(f"results/default/checkpoint-0-"+str(NERF_IT_TO_LOAD)+SCENE_NAME+"_sc"+str(IMG_SCALE)+".ckpt"):

    for increment in tqdm(range(args.increments + 1), 'Dataset increments'):
        step_idx = args.trainstart + increment * args.trainsize + torch.arange(args.trainsize)

        train_dataloader = get_dataloader(step_idx, args.trainviews, shuffle=True, patch_size=2)
        val_dataloader = get_dataloader(step_idx, args.valviews)

        for epoch in (epoch_pbar := tqdm(range(args.epochs))):

            # Train run
            train_loss, train_psnr = run_scene(scene,
                                            train_dataloader,
                                            optim=optim,
                                            lr_scheduler=lr_scheduler,
                                            coarse_reg=args.coarsereg,
                                            prop_reg=args.propreg,
                                            dist_reg=args.distreg,)
            history['train'].append(train_psnr)

            # Validation run
            val_loss, val_psnr, val_estims = run_scene(scene, val_dataloader, return_estims=True)
            val_estims = unflatten_estims(val_estims, height, width)
            history['val'].append(val_psnr)

            assert not val_estims['color_map'].abs().sum() == 0, 'Model collapsed, output fully black...'

            # Store checkpoint
            ckpt_identifier = f'{increment}-{str(epoch) if (epoch + 1 + 2) % args.saveevery == 0 else "cur"}'
            save_checkpoint(f'{args.saveckpt}/checkpoint-{ckpt_identifier}{SCENE_NAME}_sc{IMG_SCALE}.ckpt')

            # Plot results
            estims_fig = show_estims(val_estims, figscale=2, show=False)
            _ = estims_fig.savefig(f'{args.saveckpt}/estims-{ckpt_identifier}.png', dpi=200)
            plt.close(estims_fig)
            history_fig = show_history(history, title='PSNR', show=False)
            _ = history_fig.savefig(f'{args.saveckpt}/psnr-{ckpt_identifier}.png', dpi=150)
            plt.close(history_fig)

            if args.targetpsnr is not None and val_psnr >= args.targetpsnr:
                print(f'Reached target PSNR {val_loss:.2f} > {args.targetpsnr:.2f}, exiting loop...')
                break

        history['final_epochs'].append(epoch)

# %%
checkpoint = torch.load(f"results/default/checkpoint-0-"+str(NERF_IT_TO_LOAD)+SCENE_NAME+"_sc"+str(IMG_SCALE)+".ckpt")
scene.load_state_dict(checkpoint['scene'])

# %%
print('Finished training.')

# %%
def show(img, force=False):
    if True:
        plt.imshow(img.clone().transpose(0,1).transpose(1,2).detach().cpu())
        plt.show()

# %%
if args.saveestims:
    step_idx = args.trainstart + torch.arange(args.trainsize)
    view_idx = torch.arange(num_views)

    dataloader = get_dataloader(step_idx, view_idx, shuffle=False)

    loss, psnr, estims = run_scene(scene, dataloader, return_estims=True)
    estims = unflatten_estims(estims, height, width, num_views)

    estims_path = f'{args.saveckpt}/estims.pkl'

    torch.save(estims, estims_path)

    print('Saved estims under', estims_path)

# %% [markdown]
# <h1>Actual Seam Craving Code</h1>

# %% [markdown]
# <h2>Save depth map</h2>

# %%
### TODO FIX ME FOR NEW SCENES ###

# %%
for increment in tqdm(range(args.increments + 1), 'Dataset increments'):
    try:
        if not RECOMPUTE_ALL:
            #ptc_pts = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.ptc")
            #ptc_dir = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.dir")
            #ptc_col = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.col")
            #ptc_grd = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.grd")
            #ptc_gdt = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.gdt")
            ptc_pts = torch.load(f"scene_ptcs/{SCENE_NAME}_sc0.25.ptc")
            ptc_dir = torch.load(f"scene_ptcs/{SCENE_NAME}_sc0.25.dir")
            ptc_col = torch.load(f"scene_ptcs/{SCENE_NAME}_sc0.25.col")
            ptc_grd = torch.load(f"scene_ptcs/{SCENE_NAME}_sc0.25.grd")
            ptc_gdt = torch.load(f"scene_ptcs/{SCENE_NAME}_sc0.25.gdt")
            break
    except:
        pass
    step_idx = args.trainstart + increment * args.trainsize + torch.arange(args.trainsize)

    train_dataloader = get_dataloader(step_idx, args.trainviews, shuffle=True, patch_size=2) # NOTE: patchsize 2
    val_dataloader = get_dataloader(step_idx, args.valviews)
    #train_dataloader = val_dataloader
    
    for epoch in (epoch_pbar := tqdm(range(args.epochs))):

        train_loss, train_psnr, train_estims = run_scene(scene,train_dataloader,return_estims=True)
        
        train_estims = unflatten_estims(train_estims, height, width)
        
        ptc_grd = torch.zeros(train_estims['color_map'].size()[0], train_estims['color_map'].size()[1], train_estims['color_map'].size()[2])
        ptc_gdt = torch.zeros(train_estims['color_map'].size()[0], train_estims['color_map'].size()[1], train_estims['color_map'].size()[2])
        for f in range(0, ptc_grd.size()[0]):
            img = train_estims['color_map'][f]
            dimg = train_estims['depth_map'][f]

            a = (img[0:-1,1:] - img[1:,1:]).abs()
            b = (img[1:,0:-1] - img[1:,1:]).abs()
            img = (a + b).sum(dim=-1)#gradient

            a = (dimg[0:-1,1:] - dimg[1:,1:]).abs()
            b = (dimg[1:,0:-1] - dimg[1:,1:]).abs()
            dimg = (a + b).sum(dim=-1)

            ptc_grd[f, 0:img.size()[0], 0:img.size()[1]] = img
            ptc_grd[f, 1:, 1:] += img
            ptc_gdt[f, 0:img.size()[0], 0:img.size()[1]] = dimg
            ptc_gdt[f, 1:, 1:] += dimg
        #asdf
        if False:
            test = ptc_grd[0].repeat(1,1,3)
            test /= test.max()
            plt.imshow(test)
            plt.show()

            test = ptc_gdt[0].repeat(1,1,3)
            test /= test.max()
            plt.imshow(test)
            plt.show()
            asdf
        
        ptc_pts = (train_estims['rays_o'].view(-1, 3) + train_estims['rays_d'].view(-1, 3) * train_estims['max_contribution'].view(-1, 1)).view(-1, 3).contiguous()
        print("BEFORE : ",ptc_pts.size())
        indices_large_enough = torch.nonzero(train_estims['max_contribution'].view(-1) > 0.8)[:,0]
        print("AFTER 1: ",indices_large_enough.size())
        indices_large_enough = torch.nonzero(train_estims['max_contribution'].view(-1) > 0.7)[:,0]
        print("AFTER 2: ",indices_large_enough.size())
        indices_large_enough = torch.nonzero(train_estims['max_contribution'].view(-1) > 0.5)[:,0]
        print("AFTER 3: ",indices_large_enough.size())
        indices_large_enough = torch.nonzero(train_estims['max_contribution'].view(-1) > 0.1)[:,0]
        indices_large_enough = torch.nonzero(train_estims['max_contribution'].view(-1) >= 0.0)[:,0]
        ptc_pts = (train_estims['rays_o'].view(-1, 3) + train_estims['rays_d'].view(-1, 3) * train_estims['depth_map'].view(-1, 1)).view(-1, 3).contiguous()[indices_large_enough].contiguous()
        ptc_dir = train_estims['rays_d'].view(-1, 3).contiguous()[indices_large_enough].contiguous()
        ptc_col = train_estims['color_map'].view(-1, 3).contiguous()[indices_large_enough].contiguous()
        ptc_grd = ptc_grd.view(-1).contiguous()[indices_large_enough].contiguous()
        ptc_gdt = ptc_gdt.view(-1).contiguous()[indices_large_enough].contiguous()

        #clip to pseudo BB for seminar:
        if True:
            TARGET_BB      = torch.rand(2, 3)
            TARGET_BB[:,0] = torch.tensor([-0.15, +0.15])
            TARGET_BB[:,1] = torch.tensor([-0.15, +0.15])
            TARGET_BB[:,2] = torch.tensor([-0.15, +0.15])
            for ax in range(0, 3):
                indices = ptc_pts[:,ax] > TARGET_BB[0,ax]

                ptc_pts = ptc_pts[indices]
                ptc_dir = ptc_dir[indices]
                ptc_col = ptc_col[indices]
                ptc_grd = ptc_grd[indices]
                ptc_gdt = ptc_gdt[indices]

                indices = ptc_pts[:,ax] < TARGET_BB[1,ax]

                ptc_pts = ptc_pts[indices]
                ptc_dir = ptc_dir[indices]
                ptc_col = ptc_col[indices]
                ptc_grd = ptc_grd[indices]
                ptc_gdt = ptc_gdt[indices]
            
            #indices  = torch.randperm(ptc.size()[0])[0:int(ptc.size()[0]/10)]
            #ptc = ptc[indices]
            #col = col[indices]

        #torch.save(ptc_pts, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.ptc")
        #torch.save(ptc_dir, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.dir")
        #torch.save(ptc_col, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.col")
        #torch.save(ptc_grd, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.grd")
        #torch.save(ptc_gdt, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.gdt")
        torch.save(ptc_pts, f"scene_ptcs/{SCENE_NAME}_sc0.25.ptc")
        torch.save(ptc_dir, f"scene_ptcs/{SCENE_NAME}_sc0.25.dir")
        torch.save(ptc_col, f"scene_ptcs/{SCENE_NAME}_sc0.25.col")
        torch.save(ptc_grd, f"scene_ptcs/{SCENE_NAME}_sc0.25.grd")
        torch.save(ptc_gdt, f"scene_ptcs/{SCENE_NAME}_sc0.25.gdt")
        break
    break

# %%
if False:
    TARGET_BB      = torch.rand(2, 3)
    TARGET_BB[:,0] = torch.tensor([-0.15, +0.15])
    TARGET_BB[:,1] = torch.tensor([-0.15, +0.15])
    TARGET_BB[:,2] = torch.tensor([-0.15, +0.15])

    def cut_off_bb(ptc, col):
        #input: [b x 3]
        ptc = ptc.clone()
        col = col.clone()

        for ax in range(0, 3):
            indices = ptc[:,ax] > TARGET_BB[0,ax]
            ptc = ptc[indices]
            col = col[indices]
            indices = ptc[:,ax] < TARGET_BB[1,ax]
            ptc = ptc[indices]
            col = col[indices]
        
        indices  = torch.randperm(ptc.size()[0])[0:int(ptc.size()[0]/25)]
        ptc = ptc[indices]
        col = col[indices]

        return ptc, col
    p, c = cut_off_bb(ptc_pts, ptc_col)
    visualise(p, c, pt_size=0.001)

# %%
if False:
    TARGET_BB      = torch.rand(2, 3)
    TARGET_BB[:,0] = torch.tensor([-0.15, +0.15])
    TARGET_BB[:,1] = torch.tensor([-0.15, +0.15])
    TARGET_BB[:,2] = torch.tensor([-0.15, +0.15])

    def cut_off_bb(ptc, col):
        #input: [b x 3]
        ptc = ptc.clone()
        col = col.clone()

        for ax in range(0, 3):
            indices = ptc[:,ax] > TARGET_BB[0,ax]
            ptc = ptc[indices]
            col = col[indices]
            indices = ptc[:,ax] < TARGET_BB[1,ax]
            ptc = ptc[indices]
            col = col[indices]
        
        indices  = torch.randperm(ptc.size()[0])[0:int(ptc.size()[0]/10)]
        ptc = ptc[indices]
        col = col[indices]

        return ptc, col

    print("A: ",ptc_pts.size())
    p, c = cut_off_bb(ptc_pts, ptc_col)
    print("B: ",p.size())
    visualise(p, c, pt_size=0.001)
    pass

# %%
ptc_pts.size()

# %%
TRANSP_LIMIT = 0.5

# %% [markdown]
# <h2>Definitions</h2>

# %%
TARGET_BB = torch.zeros(2, 3)

#smaller area on beach
TARGET_BB[:,0] = torch.tensor([-0.055, 0.1])
TARGET_BB[:,1] = torch.tensor([-0.0875, 0.125])
TARGET_BB[:,2] = torch.tensor([-0.2, 0.1])#-0.0990]) #<--- beach optimal
CAMPOS = torch.tensor([0, 0.025, 0.15 + 0.07])
if 'farm' in SCENE_NAME:
    TARGET_BB[:,0] = torch.tensor([-0.055, 0.1])
    TARGET_BB[:,1] = torch.tensor([-0.0875, 0.125])
    TARGET_BB[:,2] = torch.tensor([-0.4, 0.2])
    CAMPOS = torch.tensor([0, 0.025, 0.15])
elif 'beach' in SCENE_NAME:
    TARGET_BB[:,0] = torch.tensor([-0.055, 0.1])
    TARGET_BB[:,1] = torch.tensor([-0.0875, 0.125])
    TARGET_BB[:,2] = torch.tensor([-0.15, 0.0])
elif 'seminar' in SCENE_NAME:
    TARGET_BB      = torch.rand(2, 3)
    TARGET_BB[:,0] = torch.tensor([-0.15, +0.15])
    TARGET_BB[:,1] = torch.tensor([-0.15, +0.15])
    TARGET_BB[:,2] = torch.tensor([-0.15, +0.15])
elif 'harbor' in SCENE_NAME:
    TARGET_BB      = torch.rand(2, 3)
    #TARGET_BB[:,0] = torch.tensor([-0.15, +0.15])
    #TARGET_BB[:,1] = torch.tensor([-0.15, +0.15])
    #TARGET_BB[:,2] = torch.tensor([-0.15, +0.15])
    TARGET_BB[0] = torch.tensor([-0.1500, -0.1000, -0.2])
    TARGET_BB[1] = torch.tensor([0.1500, 0.1300, -0.005])

SAMPLES_FOR_INITIALISATION_TRAINING = 10000

# 1.0 = change nothing; 2.0 = double in size; 0.5 = half in size
SCALING_FACTOR = 1.0/SCALING_FACTOR

#surface points -> through de-offset map to find original position
#get deformation of these points
def get_pt_indices_within_bb(ptc):
    a = torch.nonzero(ptc[:,0] > TARGET_BB[0,0])
    b = torch.nonzero(ptc[:,0] < TARGET_BB[1,0])
    c = torch.nonzero(ptc[:,1] > TARGET_BB[0,1])
    d = torch.nonzero(ptc[:,1] < TARGET_BB[1,1])
    e = torch.nonzero(ptc[:,2] > TARGET_BB[0,2])
    f = torch.nonzero(ptc[:,2] < TARGET_BB[1,2])

    indices = np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(a, b), c), d), e), f)
    return torch.tensor(indices)

def get_pt_indices_outside_bb(ptc):
    global TARGET_BB
    all_indices = torch.range(0, ptc.size()[0]-1).long()
    TARGET_BB *= 0.5 #POSSIBLY: /= SCALING_FACTOR
    result = np.setdiff1d(all_indices, get_pt_indices_within_bb(ptc))
    TARGET_BB *= 2.0 #POSSIBLY: *= SCALING_FACTOR
    return result

# %%
def run_scene_offset(scene, 
              dataloader, 
              optim=None, 
              lr_scheduler=None,
              coarse_reg=None,
              prop_reg=None,
              dist_reg=None,
              return_estims=True,
              forbid=False,
              hide_outside=None):
    global deform_net
    scene.train(False)
    torch.set_grad_enabled(False)

    estims = {}
    losses = []

    for rays_o, rays_d, rays_r, rays_c, rays_i in (pbar := tqdm(dataloader, leave=False)):
        rays_c = rays_c.cuda()

        def offset(x):
            b4 = x.size()
            x = x.view(-1, 3)

            deformed = deform_net(x)[:,0]

            ###TESTING###
            #   find coordinates outside bounding box, set offset accordingly
            if True:
                indices_smaller = x[:,AXIS_TO_OFFSET] < TARGET_BB[0, AXIS_TO_OFFSET]
                indices_bigger = x[:,AXIS_TO_OFFSET] > (TARGET_BB[0, AXIS_TO_OFFSET] + (TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET])/SCALING_FACTOR)
                #--> move far away
                x[indices_smaller] *= 0.0
                x[indices_smaller] -= 1.0
                x[indices_bigger] *= 0.0
                x[indices_bigger] -= 1.0

                ###offset_coord_for_max = 1.0 / SCALING_FACTOR
                ###offset_value_for_max = (1.0 - offset_coord_for_max) * (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])
                ###deformed[indices_bigger] = offset_value_for_max
            else: #### DOES NOT WORK FOR EXPAND FOR SOME REASON
                indices_smaller = x[:,AXIS_TO_OFFSET] < TARGET_BB[0, AXIS_TO_OFFSET]
                indices_bigger = x[:,AXIS_TO_OFFSET] > TARGET_BB[1, AXIS_TO_OFFSET]
                deformed[indices_smaller] = 0.0

                offset_coord_for_max = 1.0 / SCALING_FACTOR
                offset_value_for_max = (1.0 - offset_coord_for_max) * (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])
                deformed[indices_bigger] = offset_value_for_max
            ###TESTING###
            x[:,AXIS_TO_OFFSET] += deformed

            x = x.view(b4)
            return x
        if forbid:
            def offset(x):
                return x

        # Render scene
        render_outs, prop_outs = scene(rays_o, rays_d, rays_r, offset_function=offset, hide_outside=hide_outside)
        final_render = render_outs[-1] # Final level produces final output

        # Extract data
        color_map = final_render['color_map']
        depth_map = final_render['depth_map']
        max_contribution = final_render['max_contribution']
        alpha_map = final_render['alpha_map']
        ts = final_render['ts']
        weights = final_render['weights']
        
        if return_estims:
            # Store current input data            
            for name, data in {# Inputs
                               'rays_o': rays_o, 
                               'rays_d': rays_d, 
                               'rays_r': rays_r, 
                               'rays_c': rays_c,
                               # Outputs
                               'color_map': color_map,
                               'depth_map': depth_map,
                               'max_contribution': max_contribution,
                               'alpha_map': alpha_map}.items():
                if name not in estims:
                    estims[name] = []
                data = data.detach().cpu()
                estims[name].append(data)

    torch.set_grad_enabled(True)
    if return_estims:
        # Combine batches
        estims = {name: torch.cat(data) for name, data in estims.items()}
        return 0, 0, estims
    else:
        return 0, 0

def show_val_frame_offset_small(id=0, hide_outside=None, show_bb=True, forbid=False):
    ### A) show undeformed ###
    val_dataloader = get_dataloader(torch.tensor([0]), [id], shuffle=False, patch_size=1, c2ws=None, val_topdown=True)
    val_loss, val_psnr, val_estims = run_scene_offset(scene, val_dataloader, return_estims=True,  forbid=forbid, hide_outside=hide_outside)
    val_estims = unflatten_estims(val_estims, height, width)
    img_a = val_estims['color_map']
    img_size = img_a.size()

    #train_estims = unflatten_estims(train_estims, height, width)
        
    ### FILTER OUT NOT DENSE RAYS & COLOUR WHITE ###+
    if hide_outside != None:
        dm = val_estims['alpha_map'].view(-1)
        cm = val_estims['color_map'].view(-1, 3)
        indices = dm < TRANSP_LIMIT
        cm[indices,:] = 1.0
    if True:
        ptc_pts = (val_estims['rays_o'].view(-1, 3) + val_estims['rays_d'].view(-1, 3) * val_estims['max_contribution'].view(-1, 1)).view(-1, 3).contiguous()
        print("PTC PTS: ",ptc_pts.size())

    
    pts = (val_estims['rays_o'].view(-1, 3) + val_estims['rays_d'].view(-1, 3) * val_estims['depth_map'].view(-1, 1)).view(-1, 3).contiguous()

    image = img_a.clone()
    image = image.view(-1, 3)
    pts = pts.view(-1, 3)

    indices = get_pt_indices_within_bb(pts)
    if show_bb:
        image[indices,0] = 1.0
    img_c = image.view(img_size)
    if SAVEPATH_OUT_IMAGES != None:
        from torchvision.utils import save_image
        save_image(img_c[0].transpose(2,1).transpose(1,0), SAVEPATH_OUT_IMAGES)
    plt.imshow(img_c[0])
    plt.show()
        

def show_val_frame_offset(id=1, path="tmp.png", val_topdown=False, hide_outside=None):
    ### A) show undeformed ###
    val_dataloader = get_dataloader(torch.tensor([0]), [id], val_topdown)
    val_loss, val_psnr, val_estims = run_scene_offset(scene, val_dataloader, forbid=True, hide_outside=hide_outside)
    val_estims = unflatten_estims(val_estims, height, width)
    ### FILTER OUT NOT DENSE RAYS & COLOUR WHITE ###
    if hide_outside != None:
        dm = val_estims['alpha_map'].view(-1)
        cm = val_estims['color_map'].view(-1, 3)
        indices = dm < TRANSP_LIMIT
        cm[indices,:] = 1.0
    img_a = val_estims['color_map'][0]
    img_size = img_a.size()
    


    pts = (val_estims['rays_o'].view(-1, 3) + val_estims['rays_d'].view(-1, 3) * val_estims['depth_map'].view(-1, 1)).view(-1, 3).contiguous()
    image = img_a.clone()
    image = image.view(-1, 3)
    pts = pts.view(-1, 3)
    
    ### B) show deformed image ###
    val_dataloader = get_dataloader(torch.tensor([0]), [id], val_topdown)
    val_loss, val_psnr, val_estims = run_scene_offset(scene, val_dataloader, forbid=False, hide_outside=hide_outside)
    val_estims = unflatten_estims(val_estims, height, width)
    #print(val_estims['color_map'].size())
    if hide_outside != None:
        dm = val_estims['alpha_map'].view(-1)
        cm = val_estims['color_map'].view(-1, 3)
        indices = dm < TRANSP_LIMIT
        cm[indices,:] = 1.0
    img_b = val_estims['color_map'][0]
    ##plt.imshow(img_b)
    ##plt.show()

    ### C) show undeformed with bounding box ###
    indices = get_pt_indices_within_bb(pts)
    image[indices,0] = 1.0
    img_c = image.view(img_size)

    ### D) show undeformed with deformation on it ###
    deformation_coord = deform_net(pts.cuda()).detach().cpu()
    deformation = pts.clone()
    deformation[:,AXIS_TO_OFFSET] += deformation_coord[:,0]
    deformation = deformation.sum(dim=-1)
    deformation = deformation.unsqueeze(1).repeat(1,3)
    deformation -= deformation.min()
    deformation /= deformation.max()
    img_d = deformation.view(img_size)
    ##plt.imshow(img_d)
    ##plt.show()

    ### E) show inside BB: ###
    val_dataloader = get_dataloader(torch.tensor([0]), [id], val_topdown)
    val_loss, val_psnr, val_estims = run_scene_offset(scene, val_dataloader, forbid=False, hide_outside=hide_outside)
    if hide_outside != None:
        dm = val_estims['alpha_map'].view(-1)
        cm = val_estims['color_map'].view(-1, 3)
        indices = dm < TRANSP_LIMIT
        cm[indices,:] = 1.0
    val_estims = unflatten_estims(val_estims, height, width)
    img_e = val_estims['color_map'][0].view(-1, 3)
    pts = (val_estims['rays_o'].view(-1, 3) + val_estims['rays_d'].view(-1, 3) * val_estims['depth_map'].view(-1, 1)).view(-1, 3).contiguous()
    pts_offset = deform_net(pts.cuda()).detach().cpu()
    pts[:,AXIS_TO_OFFSET] += pts_offset[:,0]
    indices = get_pt_indices_within_bb(pts)
    img_e[indices,1] = 1.0
    img_e = img_e.view(img_size)
    #image[indices,0] = 1.0

    ### F) show inside BB: ###
    img_f = img_c.clone()
    a = (img_f[0:-1,1:] - img_f[1:,1:]).abs()
    b = (img_f[1:,0:-1] - img_f[1:,1:]).abs()
    img_f = (a + b).sum(dim=-1)#gradient
    img_f = img_f - img_f.min()
    img_f = img_f / img_f.max()
    img_f = img_f.unsqueeze(2).repeat(1,1,3)
    img = img_a.clone()
    img[0:img_f.size()[0], 0:img_f.size()[1], :] = img_f
    img_f = img

    plt.figure(figsize = (20,4))
    print("Input / Deformed Input / Bounding Box to deform / Deformation Map / Deformed Input (green is outside BB) / Change of Deformation")
    mega = torch.cat((img_a, img_b, img_c, img_d, img_e, img_f), 1)
    plt.imshow(mega, interpolation='none', aspect='equal')
    plt.show()

    from torchvision.utils import save_image
    save_image(mega.permute(2,0,1), path)

# 100 is the perfect view, aligned with axis
#views to test: 1, 7, 10, 90, 100
#for k in range(100, 400, 10):
#    show_val_frame_offset_small(k)

#
def hide_outside_fnc(data, purpose=None):
    pts_unenc = data[0]
    density   = data[1]
    #return density

    #find all points outside, set their density to zero:
    shape_before = density.size()
    density = density.view(-1)
    pts = pts_unenc.view(-1, 3)
    indices = get_pt_indices_outside_bb(pts.clone().detach().cpu())
    density[indices] = 0.00000

    return density.view(shape_before)

# %%
#free camera:

moved_offset = 0.01
import math

#render 80 frames
angle_range = torch.range(start=-4, end=4, step=0.1)

GLOBAL_CAM_TRANSLATION = None
GLOBAL_CAM_ROTATION    = None

def get_dataloader(step_idx=None, view_idx=None, shuffle=False, patch_size=1, c2ws=None, val_topdown=False):

    c2ws = globals()['c2ws'].clone()
    
    if GLOBAL_CAM_TRANSLATION != None:
        c2ws[:,:, 0, 3] = GLOBAL_CAM_TRANSLATION[0]
        c2ws[:,:, 1, 3] = GLOBAL_CAM_TRANSLATION[1]
        c2ws[:,:, 2, 3] = GLOBAL_CAM_TRANSLATION[2]

    if GLOBAL_CAM_ROTATION != None:
        from scipy.spatial.transform import Rotation as R
        import numpy as np
        r = R.from_euler('xyz', [GLOBAL_CAM_ROTATION[0], GLOBAL_CAM_ROTATION[1], GLOBAL_CAM_ROTATION[2]], degrees=True)
        rotmat = torch.tensor(r.as_matrix()).transpose(0,1)

        print("ROTMAT: ",rotmat.size())
        print("C2WS: ",c2ws.size())
        rotmat = rotmat[None, None].repeat(1,c2ws.size()[1],1,1)
        try:
            c2ws[:, :, :, 0:3] = rotmat
        except:
            c2ws[:, :, 0:3, 0:3] = rotmat

    dl = get_ray_dataloader(images, c2ws, focal_x, focal_y, step_idx, view_idx, shuffle=shuffle, batch_size=args.batchsize, workers=args.workers, patch_size=patch_size, val_topdown=False)
    return dl


#show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False)

#GLOBAL_CAM_TRANSLATION = torch.tensor([0, 0, 0.2])
#GLOBAL_CAM_ROTATION    = torch.tensor([0, 0, 0])
#show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False)

def mkdir(path):
    try:
        os.mkdir(path)
    except:
        pass

FIXEDSTEP = None
def render_gif(apply_deformation=False, single_frame=False, sgl_output=None):
    global SAVEPATH_OUT_IMAGES, GLOBAL_CAM_TRANSLATION, GLOBAL_CAM_ROTATION
    it = 0

    subpath = "sc"+str(1.0/SCALING_FACTOR) + "_ax" + str(AXIS_TO_OFFSET)

    mkdir("final_gif/")
    mkdir("final_gif/"+SCENE_NAME+"/")
    mkdir("final_gif/"+SCENE_NAME+"/" + subpath + "/")
    
    stepsize = 0.01

    if LOWREST:
        stepsize = 0.25

    for alpha in np.arange(0.0, 1.05, stepsize):
        SAVEPATH_OUT_IMAGES = "final_gif/"+SCENE_NAME+"/" + subpath + "/"+str(it)+".png"
        if sgl_output != None:
            SAVEPATH_OUT_IMAGES = sgl_output
        
        if FIXEDSTEP != None:
            alpha = FIXEDSTEP

        GLOBAL_CAM_TRANSLATION = torch.tensor([0, -0.4 * alpha + 0.025, -0.05 + 0.2 * (1.0 - alpha)])
        GLOBAL_CAM_ROTATION    = torch.tensor([-80*alpha, 0, 0])
        if "harbor" in SCENE_NAME:
            if AXIS_TO_OFFSET == 0:
                GLOBAL_CAM_TRANSLATION = torch.tensor([-0.08, -.1 + 0.09 - alpha * 0.2 + 0.03, 0.15 - alpha * 0.15])
                if not apply_deformation:
                    GLOBAL_CAM_TRANSLATION[0] += 0.08
                GLOBAL_CAM_ROTATION    = torch.tensor([-75. * alpha, 0, 0])
            else:
                GLOBAL_CAM_TRANSLATION = torch.tensor([0.0 - alpha * 0.2, 0.04 - 0.02 - 0.05, 0.15 - alpha * 0.20]) #vorher: height 0.2 - alpha * 0.25
                GLOBAL_CAM_ROTATION    = torch.tensor([0.0, 90.0 * alpha , 90.0])
        if "farm" in SCENE_NAME:
            if AXIS_TO_OFFSET == 1:
                #GLOBAL_CAM_TRANSLATION = torch.tensor([0.03+0.25 * alpha, 0.03 - .01, 0.15 - alpha * 0.15])
                #GLOBAL_CAM_ROTATION = torch.tensor([0, -alpha * 60, 270])
                GLOBAL_CAM_TRANSLATION = torch.tensor([0.03+0.2 * alpha, 0.03 - .01 - 0.001 - 0.06 + 0.0575, 0.10 - alpha * 0.13])
                GLOBAL_CAM_ROTATION = torch.tensor([0, -alpha * 60, 270])
            else:
                pass #use the same as beach
        if "beach" in SCENE_NAME:
            if AXIS_TO_OFFSET == 1:
                GLOBAL_CAM_TRANSLATION = torch.tensor([0.03-0.25 * alpha, 0.03, 0.15 - alpha * 0.15])
                GLOBAL_CAM_ROTATION = torch.tensor([0, alpha * 60, 90])
        
        show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False, forbid=(not apply_deformation))

        if FIXEDSTEP != None:
            return
        it += 1
        if single_frame:
            break

# %%
if SCALING_FACTOR == 1.0:
    #render uniform stuff
    render_gif(apply_deformation=False)
    asdf


# %% [markdown]
# <h2>Initialise deformation</h2>

# %% [markdown]
# <h3>PREPARE to initialise deformation to uniform</h3>

# %%
def render_target(apply_deformation=False, translation = None, rotation = None, path=None):
    global SAVEPATH_OUT_IMAGES, GLOBAL_CAM_TRANSLATION, GLOBAL_CAM_ROTATION

    if translation == None:
        translation = torch.tensor([0, 0.025, 0.25])
    if rotation == None:
        rotation = torch.tensor([-80*0.0, 0, 0])

    SAVEPATH_OUT_IMAGES = "test.png"
    if path != None:
        SAVEPATH_OUT_IMAGES = path

    GLOBAL_CAM_TRANSLATION = translation
    GLOBAL_CAM_ROTATION    = rotation
    show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False, forbid=(not apply_deformation))

# %% [markdown]
# <h1>Render tool</h1>

# %%
from nets import Deform3D as Deform3D
deform_net = Deform3D().cuda()
optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.00001)
deform_net.train(True)

#samples WITHIN the bounding box - BUT: the OUTPUT of the learned net can be outside of this! last sample is at the fringe, i.e. after applying deformation, will be much further away from it
def sample_bb(no_samples, get_offset=True, get_boundary_only=False):
    #get_offset: for uniform offset values, linearly increasing offset scale
    samples = torch.rand(no_samples, 3)

    #binarise: only 0 or 1
    if get_boundary_only:
        indices = torch.randperm(samples.size()[0])
        indices_a = indices[0:int(indices.size()[0]/2)]
        indices_b = indices[int(indices.size()[0]/2):]
        samples[indices_a] = 0.0
        samples[indices_b] = 1.0

    bb_width = (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])

    clean_s_vals = samples.clone()

    #in [0, bb_range]^3
    for ax in range(0, 3):
        samples[:,ax] *= (TARGET_BB[1,ax] - TARGET_BB[0,ax])
    
    if get_offset and SCALING_FACTOR < 1.0:
        offset_coord_for_max = 1.0 / SCALING_FACTOR #get original value of scaling again
        offset_value_for_max = (1.0 - offset_coord_for_max) * bb_width
        if SCALING_FACTOR < 1.0: #stretch
            samples[:,AXIS_TO_OFFSET] /= SCALING_FACTOR
            ###target_offset = -(samples[:,AXIS_TO_OFFSET].clone()) * (SCALING_FACTOR) <- works for doubling
            target_offset = (clean_s_vals[:,AXIS_TO_OFFSET].clone() * (1.0 - 1.0 / SCALING_FACTOR)) * bb_width
        else:
            target_offset = samples[:,AXIS_TO_OFFSET].clone() * SCALING_FACTOR
            target_offset = torch.min(offset_value_for_max, target_offset)
    elif get_offset:
        offset_coord_for_max = 1.0 / SCALING_FACTOR
        offset_value_for_max = (1.0 - offset_coord_for_max) * bb_width
        offset_coord_for_max = offset_coord_for_max * bb_width
        target_offset = samples[:,AXIS_TO_OFFSET].clone() * SCALING_FACTOR
        target_offset = torch.min(offset_value_for_max, target_offset)


    #from [0, bb_range] into BB: translate
    for ax in range(0, 3):
        samples[:,ax] += TARGET_BB[0,ax]

    if get_offset:
        return samples.cuda(), target_offset.cuda()
    else:
        return samples.cuda()

def sample_outside_bb(no_samples):
    #get_offset: for uniform offset values, linearly increasing offset scale
    half = int(no_samples/2)
    samples = torch.rand(no_samples, 3)
    samples[:half,AXIS_TO_OFFSET] -= 1.0 #set to zero
    samples[half:,AXIS_TO_OFFSET] += 1.0 #set to maximum offset value

    bb_width = (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])

    #in [0, bb_range]^3
    for ax in range(0, 3):
        samples[:,ax] *= (TARGET_BB[1,ax] - TARGET_BB[0,ax])

    samples[:,AXIS_TO_OFFSET] /= SCALING_FACTOR
    
    #largest possible offset value
    offset_coord_for_max = 1.0 / SCALING_FACTOR
    offset_value_for_max = (1.0 - offset_coord_for_max) * bb_width

    target_offset = torch.zeros_like(samples)[:,0]
    target_offset[:half] = 0.0
    target_offset[half:] = offset_value_for_max

    #from [0, bb_range] into BB: translate
    for ax in range(0, 3):
        samples[:,ax] += TARGET_BB[0,ax]

    return samples.cuda(), target_offset.cuda()

# %% [markdown]
# <h2>Actually training the deformation net</h2>

# %%
try:
    if RECOMPUTE_ALL:
        goto_exception
    #asdf
    #deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc{IMG_SCALE}_deform_init.net").cuda()
    deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_init.net").cuda()
    optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.00001)
    deform_net.train(True)
    re_learned_deform = False
except Exception as e:
    re_learned_deform = True
    deform_net = Deform3D().cuda()
    deform_net.train(True)
    optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.00001)

    for epoch in range(0, 50):
        avg_loss = 0.0

        for step in range(0, 100):
            optimiser.zero_grad()
            bonly = False
            tensors, target_offset = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, get_boundary_only=bonly)
            out_tensors, out_target_offset = sample_outside_bb(SAMPLES_FOR_INITIALISATION_TRAINING)
            offset = deform_net(tensors)[:,0]
            #get points BEFORE boundary box --> make offset zero
            #get points BEHIND boundary box --> make offset whatever biggest offest it
            out_offset = deform_net(out_tensors)[:,0]
            
            loss = (offset - target_offset).square().mean() + (out_offset - out_target_offset).square().mean()
            avg_loss += loss.item()
            loss.backward()
            optimiser.step()
        print("*** DONE WITH EPOCH ",epoch," - AVG LOSS: ",avg_loss/50," ***")
        if epoch % 10 == 0:
            print("INITIAL DEFORMATION AFTER ",epoch,":")
            #render_target(apply_deformation=False)
            if SCALING_FACTOR < 1.0: #stretch
                render_gif(apply_deformation=True, single_frame=True)
                #render_target(apply_deformation=True, translation=CAMPOS)
            else:
                render_gif(apply_deformation=True, single_frame=True)
                #render_target(apply_deformation=True)
    #torch.save(deform_net, f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc{IMG_SCALE}_deform_init.net")
    torch.save(deform_net, f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_init.net")

# %% [markdown]
# <h3>Train inverse deformation</h3>

# %%
try:
    if re_learned_deform or RECOMPUTE_ALL:
        goto_except
    #inverse_deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc{IMG_SCALE}_deform_inverse.net")
    inverse_deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_inverse.net")
    optimiser_inverse_deform = torch.optim.AdamW(inverse_deform_net.parameters(), lr=0.001, weight_decay=0.00001)
except:
    deform_net.train(False)
    inverse_deform_net = Deform3D().cuda()
    optimiser_inverse_deform = torch.optim.AdamW(inverse_deform_net.parameters(), lr=0.001, weight_decay=0.00001)

    for epoch in range(0, 25):
        avg_loss = 0.0
        avg_distance = 0.0
        for step in range(0, 100):
            optimiser_inverse_deform.zero_grad()


            tensors = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, False)
            offset = deform_net(tensors)
            tensors_offset = tensors.clone()
            tensors_offset[:, AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += offset

            #offsets should cancel out
            loss = (inverse_deform_net(tensors_offset) + offset).square().mean()
            avg_loss += loss.item()
            loss.backward()

            tensors_offset[:, AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += inverse_deform_net(tensors_offset)
            avg_distance += (tensors_offset-tensors).square().sum(dim=-1).sqrt().mean()

            optimiser_inverse_deform.step()

        #torch.save(inverse_deform_net, f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc{IMG_SCALE}_deform_inverse.net")
        torch.save(inverse_deform_net, f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_inverse.net")
        print("AVG DISTANCE OF POINTS AFTER MAPPING BACK AND FORTH: ",avg_distance/100)
        print("*** DONE WITH EPOCH ",epoch," - INVERSE LOSS: ",avg_loss/100,", DISTANCE: ",avg_distance/100.0," ***")
    deform_net.train(True)

# %% [markdown]
# <h2>Compute discrete energy field</h2>

# %%
from helpers import visualise

IMG_SCALE_PRE = IMG_SCALE
IMG_SCALE = 0.25

# ---> FIX FOR SCALE TO NOT LOAD BIG <--- #
if os.path.isfile(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.nrg") and not RECOMPUTE_ALL:
    ptc_nrg = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.nrg")
    ptc_pts = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.ptc")
    ptc_dir = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.dir")
    ptc_col = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.col")
    ptc_grd = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.grd")
    ptc_gdt = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.gdt")

    recomputed_all_points = False
else:
    import numpy as np
    from scipy.spatial import KDTree

    recomputed_all_points = True
    
    ptc_pts = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.ptc")
    ptc_dir = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.dir")
    ptc_col = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.col")
    ptc_grd = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.grd")
    ptc_gdt = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}.gdt")


    ### !!! NEW !!! ###
    indices = get_pt_indices_within_bb(ptc_pts)
    ptc_pts = ptc_pts[indices].clone()
    ptc_dir = ptc_dir[indices].clone()
    ptc_col = ptc_col[indices].clone()
    ptc_grd = ptc_grd[indices].clone()
    ptc_gdt = ptc_gdt[indices].clone()





    indices = torch.randperm(ptc_pts.size()[0])[0:5000000]
    ptc_pts = ptc_pts[indices]
    ptc_dir = ptc_dir[indices]
    ptc_gdt = ptc_gdt[indices]
    ptc_grd = ptc_grd[indices]
    ptc_col = ptc_col[indices]

    energy = ptc_grd + ptc_gdt
    energy_filtered = energy.clone()
    energy_filtered_min = energy.clone()
    print("Building KD-Tree...")
    tree = KDTree(ptc_pts) #input [k x 3]
    for k in range(0, energy.size()[0]):
        if k % 50000 == 0:
            print("DONE WITH ",k/energy.size()[0]*100,"%")
        pt = [ptc_pts[k].numpy()]
        _, ii = tree.query(pt, k=20)
        closest = torch.tensor(ii)[0]
        avg = energy[closest].mean()
        energy_filtered[k] = avg

        _, ii = tree.query(pt, k=5)
        energy_filtered_min[k] = energy[closest].min()
    torch.save(energy_filtered_min, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.nrg")
    torch.save(ptc_pts, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.ptc")
    torch.save(ptc_dir, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.dir")
    torch.save(ptc_col, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.col")
    torch.save(ptc_gdt, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.gdt")
    torch.save(ptc_grd, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_sub.grd")

    indices = torch.randperm(ptc_pts.size()[0])[0:100000]
    pts = ptc_pts[indices]
    col = energy_filtered_min[indices]
    col /= col.max()
    col = torch.cat((col[:,None], col[:,None] * 0.0, col[:,None] * 0.0), 1)
    print("Energy filtered:")
    visualise(pts, col, 0.003)

    print("Energy unfiltered:")
    indices = torch.randperm(ptc_pts.size()[0])[0:100000]
    pts = ptc_pts[indices]
    col = energy[indices]
    col /= col.max()
    col = torch.cat((col[:,None], col[:,None] * 0.0, col[:,None] * 0.0), 1)
    visualise(pts, col, 0.003)
    ptc_nrg = energy_filtered_min

# %% [markdown]
# <h2>Learn continuous energy function</h2>

# %% [markdown]
# <h3>Generate samples for training</h3>

# %%
if os.path.isfile(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg_samples_in_bb.smp") and not RECOMPUTE_ALL and not recomputed_all_points:
    nrg_samples_in_bb = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg_samples_in_bb.smp")
    nrg_samples_in_bb_energy = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg_samples_in_bb.nrg")
    recomputed_energy_points = False
else:
    recomputed_energy_points = True

    from scipy.spatial import KDTree
    #get example prediction network: make sure points in empty space, i.e. bounding box, are also well-defined in terms of energy: assign them closest energy value
    nrg_samples_in_bb = sample_bb(1000000, get_offset=False).cpu()
    nrg_samples_in_bb_energy = torch.zeros(nrg_samples_in_bb.size()[0])
    print("SAMPLES IN BB: ",nrg_samples_in_bb.size())

    print("Building KD-Tree...")
    tree = KDTree(ptc_pts) #input [k x 3]
    print("Done building KD-Tree.")

    #compute the distance value to neighbours so we know when we fall off in terms of energy:
    ids = torch.randperm(ptc_pts.size()[0])[0:100]
    avg_distance_to_nearest = 0.0
    for k in ids:
        pt = [ptc_pts[k].numpy()]
        _, ii = tree.query(pt, k=1)
        closest = torch.tensor(ii)[0]
        distance = ((ptc_pts[closest] - ptc_pts[k]).square().sum() + 0.000001).sqrt()
        avg_distance_to_nearest += distance
    avg_distance_to_nearest /= ids.size()[0]
    print("Average distance to neighbour: ",avg_distance_to_nearest)

    for k in range(0, nrg_samples_in_bb.size()[0]):
        if k % 50000 == 0:
            print("DONE WITH ",k/nrg_samples_in_bb.size()[0]*100,"%")
        pt = [nrg_samples_in_bb[k].numpy()]
        _, ii = tree.query(pt, k=1)
        closest = torch.tensor(ii)[0]
        distance = ((ptc_pts[closest] - nrg_samples_in_bb[k]).square().sum() + 0.00001).sqrt()
        if distance > 2 * avg_distance_to_nearest:
            nrg_samples_in_bb_energy[k] = 0.0
        else:
            nrg_samples_in_bb_energy[k] = ptc_nrg[closest]
    torch.save(nrg_samples_in_bb, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg_samples_in_bb.smp")
    torch.save(nrg_samples_in_bb_energy, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg_samples_in_bb.nrg")
    print("DONE and saved!")

# %% [markdown]
# <h3>Train network itself</h3>

# %%
if os.path.isfile(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg.net") and not RECOMPUTE_ALL and not recomputed_energy_points:
    energy_net = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg.net")
    recomputed_energy_net = False
else:
    recomputed_energy_net = True
    
    energy_net = Deform3D().cuda()
    optimiser = torch.optim.AdamW(energy_net.parameters(), lr=0.001, weight_decay=0.00001)

    for epoch in range(0, 25):
        avg_loss_surface = 0.0
        avg_loss_rnd     = 0.0 
        for step in range(0, 50):
            optimiser.zero_grad()
            if step % 2 == 0:
                indices = torch.randperm(nrg_samples_in_bb.size()[0])[0:25000]

                pts = nrg_samples_in_bb[indices].cuda()
                energy = nrg_samples_in_bb_energy[indices].cuda()

                loss = (energy_net(pts)[:,0] - energy).square().mean() + (energy_net(pts)[:,0] - energy).abs().mean()
                avg_loss_rnd += loss.item()
            else:
                indices = torch.randperm(ptc_pts.size()[0])[0:25000]
                
                pts = ptc_pts[indices].cuda()
                energy = ptc_nrg[indices].cuda()

                loss = (energy_net(pts)[:,0] - energy).square().mean() + (energy_net(pts)[:,0] - energy).abs().mean()
                avg_loss_surface += loss.item()
            loss.backward()
            optimiser.step()

        print("*** DONE WITH EPOCH ",epoch," - AVG ERROR SURFACE: ",avg_loss_surface/25," - AVG ERROR RND: ",avg_loss_rnd/25," ***")
    energy_net.train(False)
    torch.save(energy_net, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_nrg.net")

# %% [markdown]
# <h3>Show continuous energy</h3>

# %%
ptc_pts.size()

# %%
with torch.no_grad():
    indices = torch.randperm(ptc_pts.size()[0])[0:25000]
    a = ptc_pts[indices].cuda()
    indices = get_pt_indices_within_bb(a.cpu())
    a = a[indices]
    energy = energy_net(a)
    energy -= energy.min()
    energy /= energy.max()
    energy = energy.repeat(1,3)
    energy[:,1:] *= 0.0
    
    visualise(a, (energy + 0.0001) ** 1.5, 0.003)
    pass

# %% [markdown]
# <h2>Train cumulative energy</h2>

# %%
energy_net.train(False)

#iterate:
#   shoot ray through scene
#   accumulate error values across ray
#   train network for it

CUMSUM_STEPS = 100

best_loss = None

if os.path.isfile(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_ax{AXIS_TO_OFFSET}_cum_nrg.net") and not RECOMPUTE_ALL and not recomputed_energy_net:
    cumulative_net = torch.load(f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_ax{AXIS_TO_OFFSET}_cum_nrg.net")
else:
    cumulative_net = Deform3D().cuda()
    optimiser_cum = torch.optim.AdamW(cumulative_net.parameters(), lr=0.001, weight_decay=0.00001)

    #nrg_samples_in_bb = sample_bb(1000000, get_offset=False).cpu()

    for epoch in range(0, 25+1):
        avg_loss     = 0.0 
        its = 0
        for step in range(0, 100):
            optimiser_cum.zero_grad()

            points_surface = ptc_pts[torch.randperm(ptc_pts.size()[0])[0:100]].cuda()
            points_random  = sample_bb(100, get_offset=False).cuda()

            points_surface = points_random.clone() # debug

            points_start = torch.cat((points_surface, points_random), 0)
            points_end   = points_start.clone()

            #snap to axis
            points_start[:, AXIS_TO_OFFSET]   = TARGET_BB[0, AXIS_TO_OFFSET]
            points_end[:, AXIS_TO_OFFSET]     = TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET]
            points_start   = points_start[None]
            points_end     = points_end[None]

            linear = torch.range(0.0, 1.0, step=(1.0 / CUMSUM_STEPS))[:,None,None].cuda()
            
            grid = points_start * (1.0 - linear) + points_end * linear#points_start + points_range * linear
            energies_gt = energy_net(grid.view(-1, 3)).detach().view(grid.size()[0], grid.size()[1], -1)
            ##print("ENERGIES GT: ",energies_gt.size())
            energies_gt = energies_gt.cumsum(dim=0).view(-1, 1) / CUMSUM_STEPS
            ##print("ENERGIES GT: ",energies_gt.size())
            #get get values for each
            energies_out = cumulative_net(grid.view(-1, 3))
            
            loss = (energies_gt - energies_out).square().mean()

            avg_loss     += loss.item()

            loss.backward()
            
            optimiser_cum.step()
            its += 1
        avg_loss /= its
        print("DONE WITH EPOCH ",epoch,"/25 TO LEARN CUMULATIVE ERROR!")
        print("\tAVERAGE LOSS: ",avg_loss)

        if best_loss == None or avg_loss < best_loss:
            print("\t--> new best, saving...")
            best_loss = avg_loss
            torch.save(cumulative_net, f"scene_ptcs/{SCENE_NAME}_sc{IMG_SCALE}_ax{AXIS_TO_OFFSET}_cum_nrg.net")

        if epoch % 25 == 0:
            
            with torch.no_grad():
                indices = torch.randperm(ptc_pts.size()[0])[0:25000]
                test_pts = ptc_pts[indices].clone().cuda()

                energy = energy_net(test_pts)
                energy -= energy.min()
                energy /= energy.max()
                energy = energy.repeat(1,3)
                energy[:,1:] *= 0.0

                cum_energy = cumulative_net(test_pts)
                cum_energy -= cum_energy.min()
                cum_energy /= cum_energy.max()
                cum_energy = cum_energy.repeat(1,3)
                cum_energy[:,1:] *= 0.0
                
                visualise(test_pts, (energy + 0.0001) ** 1.5, 0.003)
                visualise(test_pts, (cum_energy + 0.0001) ** 1.5, 0.003)
                pass
best_loss = None

# %% [markdown]
# <h1>Debug - we need the old method. Yes, this is spaghetti code from hell. No, I don't care</h1>

# %%
if SCALING_FACTOR > 1.0: #shrink; use old values
    def sample_bb(no_samples, get_offset=True):
        #get_offset: for uniform offset values, linearly increasing offset scale
        samples = torch.rand(no_samples, 3)

        bb_width = (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])

        #in [0, bb_range]^3
        for ax in range(0, 3):
            samples[:,ax] *= (TARGET_BB[1,ax] - TARGET_BB[0,ax])
        
        if get_offset:
            offset_coord_for_max = 1.0 / SCALING_FACTOR
            offset_value_for_max = (1.0 - offset_coord_for_max) * bb_width
            offset_coord_for_max = offset_coord_for_max * bb_width
            target_offset = samples[:,AXIS_TO_OFFSET].clone() * SCALING_FACTOR
            target_offset = torch.min(offset_value_for_max, target_offset)

        #from [0, bb_range] into BB: translate
        for ax in range(0, 3):
            samples[:,ax] += TARGET_BB[0,ax]

        if get_offset:
            return samples.cuda(), target_offset.cuda()
        else:
            return samples.cuda()

    def sample_outside_bb(no_samples):
        half = int(no_samples/2)
        #[0,1]
        samples = torch.rand(no_samples, 3, device=DEVICE)
        #[-0.5, 0]
        samples[:half] *= -0.5
        #[lower-0.5, lower]
        samples[:half,AXIS_TO_OFFSET] += TARGET_BB[0,AXIS_TO_OFFSET]

        #accordingly for points further
        samples[half:] *= 0.5
        samples[half:,AXIS_TO_OFFSET] += TARGET_BB[1,AXIS_TO_OFFSET]
        
        target = torch.zeros_like(samples)[:,0]

        offset_coord_for_max = 1.0 / SCALING_FACTOR
        offset_value_for_max = (1.0 - offset_coord_for_max) * (TARGET_BB[1,AXIS_TO_OFFSET] - TARGET_BB[0,AXIS_TO_OFFSET])
        target[half:] = offset_value_for_max

        return samples, target 

# %% [markdown]
# <h2>Optimise deformation with our approach</h2>

# %%
def show_deform(height=0.5):
    zval = TARGET_BB[0,2] * height + TARGET_BB[1,2] * (1.0 - height)
    stepsize = (TARGET_BB[1, 0] - TARGET_BB[0, 0]) / 100.0
    pts = []

    steps_x = (TARGET_BB[1, 0]-TARGET_BB[0, 0])/stepsize
    steps_y = (TARGET_BB[1, 1]-TARGET_BB[0, 1])/stepsize

    steps_x = int(steps_x)
    steps_y = int(steps_y)

    for x in range(0, steps_x):
        for y in range(0, steps_y):
            pts.append(torch.tensor([stepsize * x + TARGET_BB[0, 0], stepsize * y + TARGET_BB[0, 1], zval]))
    pts = torch.stack(pts)
    print(pts.size())
    #visualise(pts, torch.zeros_like(pts))

    deformed = deform_net(pts.cuda()).detach().cpu()
    deformed = deformed.view(steps_x, -1)[:,:,None].repeat(1,1,3)
    if AXIS_TO_OFFSET == 1:
        deformed = deformed[:,0:int(deformed.size()[1] / SCALING_FACTOR)]
    elif AXIS_TO_OFFSET == 0:
        deformed = deformed[0:int(deformed.size()[0] / SCALING_FACTOR)]
    deformed -= deformed.min()
    deformed /= deformed.max()

    #show(deformed)
    show(deformed.transpose(1,2).transpose(0,1))
    return deformed

# %%
#render_gif(apply_deformation=False, single_frame=True)

# %%
### load here again for better testing ###
deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_init.net")
energy_net.train(False)

inverse_deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_inverse.net")
inverse_deform_net.train(False)
optimiser_inverse_deform = torch.optim.AdamW(inverse_deform_net.parameters(), lr=0.001, weight_decay=0.000001)

IMG_SCALE = IMG_SCALE_PRE
# ---> FIX FOR SCALE TO NOT LOAD BIG <--- #

#for ts in TO_SHOW:
#    show_val_frame_offset(ts)

#render_gif(apply_deformation=False, single_frame=True, sgl_output="training/base.png")
#render_gif(apply_deformation=True, single_frame=True, sgl_output="training/"+str(0)+".png")

if IN_JUPYTER_NOTEBOOK:
    render_gif(apply_deformation=False, single_frame=True)
    render_gif(apply_deformation=True, single_frame=True)

### load here again for better testing ###

offset_range = TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET]
offset_range /= 100

#maximum rate of change: meaning, if we go 1 unit in original space, at most go 2 units in deformed space

optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.000001)

relu = torch.nn.ReLU()

EPSILON = 0.000001
MAX_STRETCH_FACTOR = SCALING_FACTOR

indices = get_pt_indices_within_bb(ptc_pts)
ptc_pts_in = ptc_pts[indices]
ptc_nrg_in = ptc_nrg[indices]

ptc_nrg_in = ptc_nrg_in / ptc_nrg_in.max()
moved_offset = 0.0

for epoch in range(0, 100):
    ### TODO: load whatever was good below down here! ###
    #TODO#
    #break
    if not IN_JUPYTER_NOTEBOOK:
        print("*** RENDERING FINAL OUTS ***")
        epoch_to_load = -1 #if we don't find something to load, CRASH!
        #1. load best
        if 'beach' in SCENE_NAME:
            # 0 = done already
            if AXIS_TO_OFFSET == 1:
                if SCALING_FACTOR == 2.0:
                    epoch_to_load = 30
        if 'farm' in SCENE_NAME:
            if AXIS_TO_OFFSET == 0:
                if SCALING_FACTOR == 2.0:
                    epoch_to_load = 17
            elif AXIS_TO_OFFSET == 1:
                if SCALING_FACTOR == 2.0:
                    epoch_to_load = 25
        if 'harbor' in SCENE_NAME:
            if AXIS_TO_OFFSET == 0:
                if SCALING_FACTOR == 2.0:
                    epoch_to_load = 25 # TODO
            elif AXIS_TO_OFFSET == 1:
                if SCALING_FACTOR == 2.0:
                    epoch_to_load = 12 #REDO ME
        deform_net = torch.load("tmp_deforms/"+SCENE_NAME+"_sc"+str(SCALING_FACTOR)+"_ax"+str(AXIS_TO_OFFSET)+"_e"+str(epoch_to_load)+".net")
        #2. render net
        render_gif(apply_deformation=True, single_frame=False)
        #3. shutdown (finished)
        asdf

    #a) Monotonicity:
    #   along an axis, make sure that we do not jump in different directions (=only ever increase the offset, do not decrease it)
    #   (3D: along the normal of a plane, only ever increase)

    #b) Boundaries:
    #   make sure the boundaries are at their target values
    #   meaning: they define the deformation frame (=front and back side of the cube are fixed)

    #c) Gradients:
    #   make sure gradient(deformation) TIMES energy is minimal

    #d) Shearing:
    #   make sure we do not allow any shearing

    #e) Shrinking:
    #   make sure we do not shrink too much (=deformation between two points should have a maximum)

    outside_pts, _ = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING)
    outside_pts *= 2.0
    samples_deformation_zero = outside_pts[torch.nonzero(outside_pts[:,AXIS_TO_OFFSET] < TARGET_BB[0,AXIS_TO_OFFSET])]
    samples_deformation_one = outside_pts[torch.nonzero(outside_pts[:,AXIS_TO_OFFSET] > TARGET_BB[1,AXIS_TO_OFFSET] / SCALING_FACTOR)]

    avg_loss                    = 0.0 # SUM [a to e]

    avg_loss_mono               = 0.0 #a) 
    avg_loss_boundaries         = 0.0 #b)
    avg_loss_gradients          = 0.0 #c)
    avg_loss_shearing           = 0.0 #d)
    avg_loss_cap_deformation    = 0.0 #e)

    avg_distance_inverting_offset = 0.0

    lambdas = dict()
    #lambdas['mono']   =    1.0
    #lambdas['bound']  =  100.0
    #lambdas['grad']   =   10.0 * 0.01
    #lambdas['cap']    =    0.0
    #lambdas['shear']  =    0.1

    lambdas['mono']   =    1.0
    lambdas['bound']  =  100.0
    lambdas['grad']   =   10.0#* 0.0
    lambdas['cap']    =    0.0
    lambdas['shear']  =    0.1

    if SCALING_FACTOR < 1.0: #stretch
        lambdas['grad']  *=    0.1
        lambdas['shear'] *=    0.1# * 0.0
        lambdas['bound'] *=   100.0
        lambdas['cap']    =    10.0

    iterations_for_optimisation = 100
    
    for step in range(0, iterations_for_optimisation):
        #break
        if step % 2 == 1:
            deform_net.train(False)
            inverse_deform_net.train(True)
            for k in range(0, 1):
                #every second step, update mapping:
                optimiser_inverse_deform.zero_grad()

                tensors = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, False)
                if SCALING_FACTOR < 1.0: #expand; make sure we regularise here
                    #tensorsb, _ = sample_outside_bb(SAMPLES_FOR_INITIALISATION_TRAINING)
                    #tensors = torch.cat((tensors, tensorsb), 0)
                    pts = TARGET_BB[0:1,:] + (TARGET_BB[1:2,:] - TARGET_BB[0:1,:]) * (1.0 / SCALING_FACTOR) * torch.rand(SAMPLES_FOR_INITIALISATION_TRAINING, 3)
                    tensors = torch.cat((tensors, pts.to(DEVICE)), 0)

                offset = deform_net(tensors)
                tensors_offset = tensors.clone()
                tensors_offset[:, AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += offset

                #offsets should cancel out
                loss = (inverse_deform_net(tensors_offset) + offset).square().mean()
                loss.backward()

                tensors_offset[:, AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += inverse_deform_net(tensors_offset)
                avg_distance_inverting_offset += (tensors_offset-tensors).square().sum(dim=-1).sqrt().mean().item()

                optimiser_inverse_deform.step()
        else:
            deform_net.train(True)
            inverse_deform_net.train(False)
            
            optimiser.zero_grad()
            
            #always combine: some random points, some surface points ("importance sampling")
            samples_random_unit = (torch.rand(SAMPLES_FOR_INITIALISATION_TRAINING, 3, device=DEVICE) * 2.0 - 1.0) * 2.0
            samples_random_bb   = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, False)
            samples_random_bb_nrg = torch.zeros_like(samples_random_bb)[:,0].cuda()
            
            indices = torch.randperm(ptc_pts.size()[0])[0:SAMPLES_FOR_INITIALISATION_TRAINING]
            samples_surface_pts = ptc_pts[indices].clone().cuda()
            samples_surface_nrg = ptc_nrg[indices].clone().cuda()

            indices = torch.randperm(ptc_pts_in.size()[0])[0:SAMPLES_FOR_INITIALISATION_TRAINING]
            samples_surface_pts_in = ptc_pts_in[indices].clone().cuda()
            samples_surface_nrg_in = ptc_nrg_in[indices].clone().cuda()
            
            indices_ext = torch.randperm(samples_deformation_zero.size()[0])[0:SAMPLES_FOR_INITIALISATION_TRAINING]
            samples_surface_zro = samples_deformation_zero.clone()[indices_ext][:,0,:].cuda()
            indices_ext = torch.randperm(samples_deformation_one.size()[0])[0:SAMPLES_FOR_INITIALISATION_TRAINING]
            samples_surface_one = samples_deformation_one.clone()[indices_ext][:,0,:].cuda()
            
            loss_mono            = torch.zeros(1, device=DEVICE)
            loss_boundaries      = torch.zeros(1, device=DEVICE) #loss_beginning + loss_ending + loss_outside
            loss_gradients       = torch.zeros(1, device=DEVICE)
            loss_shearing        = torch.zeros(1, device=DEVICE)
            loss_cap_deformation = torch.zeros(1, device=DEVICE)

            if SCALING_FACTOR > 1.0: #for shrink, all as usual
                #compose mixed set of samples (all inside the bounding box):
                #   1) points at where the deformation lands
                #   2) points at where the deformation starts (undeformed)
                #   3) random points
                surface_points_mixed = samples_surface_pts_in.clone()
                #if stretch, include from whole bb:
                if SCALING_FACTOR < 1.0:
                    pts = TARGET_BB[0:1,:] + (TARGET_BB[1:2,:] - TARGET_BB[0:1,:]) * (1.0 / SCALING_FACTOR) * torch.rand(SAMPLES_FOR_INITIALISATION_TRAINING, 3)
                    surface_points_mixed = torch.cat((surface_points_mixed, pts.to(DEVICE)), 0)
            
                offset = inverse_deform_net(surface_points_mixed).detach() #find offset for our points such that we know the coordinates that get mapped to these points
                surface_points_mixed[:,AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += offset
                surface_points_surface_only = surface_points_mixed.clone().detach()
                surface_points_mixed = torch.cat((surface_points_mixed, samples_surface_pts_in, sample_bb(samples_surface_pts_in.size()[0], get_offset=False)), 0).detach()
            else:
                pts = TARGET_BB[0:1,:] + (TARGET_BB[1:2,:] - TARGET_BB[0:1,:]) * (1.0 / SCALING_FACTOR) * torch.rand(SAMPLES_FOR_INITIALISATION_TRAINING, 3)
                surface_points_mixed = pts.to(DEVICE)#torch.cat((surface_points_mixed, pts.to(DEVICE)), 0)
            
            #always used a mixed bag for those!
            ptcs_for_grad   = surface_points_mixed.clone()
            ptcs_for_deform = surface_points_mixed.clone()
            ptcs_for_shear  = surface_points_mixed.clone()

            #a) Monotonicity:
            #   along an axis, make sure that we do not jump in different directions (=only ever increase the offset, do not decrease it)
            #   (3D: along the normal of a plane, only ever increase)
            if SCALING_FACTOR > 1.0: #shrink
                samples_surface_pts_offset = surface_points_mixed.clone()
                offset = torch.zeros_like(surface_points_mixed)
                offset[:,AXIS_TO_OFFSET] = torch.rand_like(offset[:,AXIS_TO_OFFSET]) * offset_range + offset_range
                samples_surface_pts_offset = samples_surface_pts_offset + offset
                # TODO FOR STRETCH
                #shrink
                if SCALING_FACTOR > 1.0:
                    loss_mono = (relu(deform_net(surface_points_mixed) - deform_net(samples_surface_pts_offset)) / offset.sum(dim=1)[:,None]).mean()
                else:
                #expand
                    loss_mono = (relu(deform_net(samples_surface_pts_offset) - deform_net(surface_points_mixed)) / offset.sum(dim=1)[:,None]).mean()
            else: #regularise everywhere
                ### --> make sure we can't skip gaps
                samples_surface_pts_offset = surface_points_mixed.clone()
                offset = torch.zeros_like(surface_points_mixed)
                offset[:,AXIS_TO_OFFSET] = torch.rand_like(offset[:,AXIS_TO_OFFSET]) * offset_range + offset_range
                samples_surface_pts_offset = samples_surface_pts_offset + offset
                # TODO FOR STRETCH
                #shrink
                if SCALING_FACTOR > 1.0:
                    loss_mono = (relu(deform_net(surface_points_mixed) - deform_net(samples_surface_pts_offset)) / offset.sum(dim=1)[:,None]).mean()
                else:
                #expand
                    loss_mono = (relu(deform_net(samples_surface_pts_offset) - deform_net(surface_points_mixed)) / offset.sum(dim=1)[:,None]).mean()

            #b) Boundaries:
            #   make sure the boundaries are at their target values
            #   meaning: they define the deformation frame (=front and back side of the cube are fixed)
            if SCALING_FACTOR > 1.0: #shrink
                #take samples in unit cube; map to beginning/ending of side of the cube
                bb_width = (TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET])

                boundary_samples_should_be_zero = surface_points_mixed.clone()#samples_random_bb.clone()
                boundary_samples_should_be_zero[:, AXIS_TO_OFFSET] = TARGET_BB[0, AXIS_TO_OFFSET]
                offset_beginning = deform_net(boundary_samples_should_be_zero)
                loss_beginning   = offset_beginning.square().mean() + offset_beginning.abs().mean()

                offset_at_ending = bb_width / SCALING_FACTOR
                boundary_samples_should_be_one = surface_points_mixed.clone()
                boundary_samples_should_be_one[:, AXIS_TO_OFFSET] = TARGET_BB[0, AXIS_TO_OFFSET] + (TARGET_BB[1, AXIS_TO_OFFSET] - TARGET_BB[0, AXIS_TO_OFFSET]) / SCALING_FACTOR
                offset_ending    = deform_net(boundary_samples_should_be_one)
                loss_ending      = (offset_ending - offset_at_ending).square().mean() + (offset_ending - offset_at_ending).abs().mean()

                #fix stuff outside the axis:
                loss_outside  = deform_net(samples_surface_zro).square().mean()
                loss_outside += (deform_net(samples_surface_one) - offset_at_ending).square().mean()

                loss_boundaries = loss_beginning + loss_ending# + loss_outside * 0.0
            else:
                tensors, target_offset = sample_bb(SAMPLES_FOR_INITIALISATION_TRAINING, get_offset=True, get_boundary_only=True)
                out_tensors, out_target_offset = sample_outside_bb(SAMPLES_FOR_INITIALISATION_TRAINING)
                
                offset = deform_net(tensors)[:,0]
                out_offset = deform_net(out_tensors)[:,0]

                loss_boundaries = (offset - target_offset).square().mean() + (out_offset - out_target_offset).square().mean()
                
            #c) Gradients NEW FORMULATION:
            if True:
                #1. get rate of change in the offset, i.e. how large the jump for one point is
                #surface_points_post_deform are now those points that are mapped ONTO our features
                offset_axis = torch.zeros_like(ptcs_for_grad)
                offset_axis[:,AXIS_TO_OFFSET] = offset_range
                
                #compute offset for a point and the offset version of the point
                D_ptc_offset = deform_net(ptcs_for_grad.clone())
                D_ptc_eps_offset = deform_net(ptcs_for_grad.clone() + offset_axis)

                #offset the point
                D_ptc = ptcs_for_grad.clone()
                D_ptc_eps = (ptcs_for_grad.clone() + offset_axis).clone()
                D_ptc[:,AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += D_ptc_offset
                D_ptc_eps[:,AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += D_ptc_eps_offset

                #compute the change magnitude of the offset
                change_in_offset = (D_ptc_eps_offset - D_ptc_offset).abs().sum(dim=-1) / offset_range

                #compute th energy value
                if SCALING_FACTOR > 1.0: #shrink
                    energy_undeformed = cumulative_net(D_ptc.clone())[:,0].detach()
                    energy_deformed   = cumulative_net(D_ptc_eps.clone())[:,0].detach()

                    energy = (energy_deformed - energy_undeformed).abs()
                else: #stretch: only take energy, not cumulative energy!
                    energy = energy_net(D_ptc).abs().detach()
                
                loss_gradients = (energy * change_in_offset).mean()
            
            #d) Cap on deformation (expansion only)
            if SCALING_FACTOR < 1.0: #stretch
                pts_a = surface_points_mixed.clone()
                pts_b = pts_a.clone()
                offsets = torch.rand(pts_a.size()[0], device=DEVICE) * offset_range + offset_range * 0.0001
                pts_b[:,AXIS_TO_OFFSET] += offsets

                offset_a = deform_net(pts_a)[:,0]
                offset_b = deform_net(pts_b)[:,0]

                #punish if we go over the maximum, i.e. more than 1
                loss_cap_deformation = torch.nn.functional.relu((offset_a - offset_b) / offsets - 3 * (1.0 / SCALING_FACTOR - 1.0)).mean()

            #e) Shearing:
            #   make sure we do not allow any shearing for surface points WITH HIGH ENERGY
            if True:
                offset_range_rnd = offset_range * 0.01 + offset_range * torch.rand_like(offset_range)
                #make sure deformation for neighbouring is the same
                offset_axis_a = torch.zeros_like(ptcs_for_shear)
                offset_axis_b = torch.zeros_like(ptcs_for_shear) #will be z-axis if we don't explicitly use z axis
                it = 0
                #increase all axis BUT the main axis of deformation - shearing is NEVER on that axis, but on all others!
                #WE DO TWO DIRECTIONS OF SHEARING!!!!
                for ax in range(0, 3):
                    if ax == AXIS_TO_OFFSET:
                        continue
                    else:
                        if it == 0:
                            offset_axis_a[:,ax] = offset_range_rnd
                            it += 1
                        else:
                            offset_axis_b[:,ax] = offset_range_rnd
                
                #compare deformation at our base point versus the two points on the other axis
                deform_base = deform_net(ptcs_for_shear.clone())
                deform_a = deform_net(ptcs_for_shear.clone() + offset_axis_a.detach())
                deform_b = deform_net(ptcs_for_shear.clone() + offset_axis_b.detach())

                loss_shearing = ((deform_base - deform_a).abs() + (deform_base - deform_b).abs()).mean(dim=1)
                loss_shearing = loss_shearing / offset_range_rnd
                energy_points = ptcs_for_shear.clone()
                energy_points[:,AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += deform_base
                nrg = torch.nn.functional.relu(energy_net(energy_points).detach()[:,0])
                #nrg += nrg.max() * 0.01
                loss_shearing = (loss_shearing * nrg).mean()
                
            loss =  loss_mono * lambdas['mono'] +\
                    loss_gradients * lambdas['grad'] +\
                    loss_boundaries * lambdas['bound'] +\
                    loss_shearing * lambdas['shear'] +\
                    loss_cap_deformation * lambdas['cap']

            avg_loss_mono += loss_mono.item()
            avg_loss_boundaries += loss_boundaries.item()
            avg_loss_gradients += loss_gradients.item()
            avg_loss_shearing += loss_shearing.item()
            avg_loss_cap_deformation += loss_cap_deformation.item()
            avg_loss += loss.item()

            loss.backward()

            optimiser.step()
    
    print("*** DONE WITH EPOCH ",epoch," ***")
    print("\t\inverse offset accuracy: ",avg_distance_inverting_offset/iterations_for_optimisation)
    print("\tavg error: ",avg_loss/float(iterations_for_optimisation)," - components: ")
    print("\t\t",lambdas['mono']," * ",avg_loss_mono/float(iterations_for_optimisation)," = ",lambdas['mono']*avg_loss_mono/float(iterations_for_optimisation),"(mono)")
    print("\t\t",lambdas['bound']," * ",avg_loss_boundaries/float(iterations_for_optimisation)," = ",lambdas['bound']*avg_loss_boundaries/100.0,"(bound)")
    print("\t\t",lambdas['grad']," * ",avg_loss_gradients/float(iterations_for_optimisation)," = ",lambdas['grad']*avg_loss_gradients/float(iterations_for_optimisation),"(grad)")
    print("\t\t",lambdas['cap']," * ",avg_loss_cap_deformation/float(iterations_for_optimisation)," = ",lambdas['cap']*avg_loss_cap_deformation/float(iterations_for_optimisation),"(cap)")
    print("\t\t",lambdas['shear']," * ",avg_loss_shearing/float(iterations_for_optimisation)," = ",lambdas['shear']*avg_loss_shearing/float(iterations_for_optimisation),"(shear)")
    # loss_beginning + loss_ending + loss_others + loss_outside

    torch.save(deform_net, "tmp_deforms/"+SCENE_NAME+"_sc"+str(SCALING_FACTOR)+"_ax"+str(AXIS_TO_OFFSET)+"_e"+str(epoch)+".net")

    if (epoch % 5 == 0 and epoch != 0) or True:
        print("OPTIMISED DEFORMATION AFTER ",epoch,":")
        #render_gif(apply_deformation=True, single_frame=True, sgl_output="training/"+str(epoch+1)+".png")
        if True:
            #render_target(apply_deformation=True, translation=CAMPOS)
            render_gif(apply_deformation=True, single_frame=True)
        else:
            render_target(apply_deformation=True, translation=torch.tensor([0, 0.0, 0.0]))
        show_deform()
        #for ts in TO_SHOW:
        #    show_val_frame_offset(ts)
    #safg
#visualise(ptc_pts[indices].transpose(0,1).contiguous(), ptc_col[indices].transpose(0,1).contiguous(), 0.0005)
for k in range(0, 5):
    break
    render_gif(apply_deformation=True, single_frame=True, sgl_output="training/"+str(epoch+1+k)+".png")

# %%
render_gif(apply_deformation=False, single_frame=True, sgl_output="training/"+str(epoch+1+k)+".png")

# %%
deform_net = torch.load("tmp_deforms/"+SCENE_NAME+"_sc"+str(SCALING_FACTOR)+"_ax"+str(AXIS_TO_OFFSET)+"_e"+str(25)+".net")

# %%
for i in np.arange(0.0, 1.0, 0.05):
    show_deform(i)

# %%
AXIS_TO_OFFSET

# %%
alpha = 0.5
GLOBAL_CAM_TRANSLATION = torch.tensor([-0.08, -.1 + 0.09 - alpha * 0.2 + 0.03, 0.15 - alpha * 0.15])
GLOBAL_CAM_ROTATION    = torch.tensor([-75. * alpha, 0, 0])
show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False, forbid=False)
#show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False, forbid=True)

# %%
alpha = 1
GLOBAL_CAM_TRANSLATION = torch.tensor([0.03+0.2 * alpha, 0.03 - .01 - 0.001 - 0.06, 0.12 - alpha * 0.15])
GLOBAL_CAM_ROTATION = torch.tensor([0, -alpha * 60, 270])
show_val_frame_offset_small(1, hide_outside=hide_outside_fnc, show_bb=False, forbid=False)

# %%
FIXEDSTEP = 0.0
render_gif(apply_deformation=True, single_frame=True)
asdf

# %%

a = show_deform(0.0)
a = show_deform(0.1)
a = show_deform(0.2)
a = show_deform(0.3)
a = show_deform(0.4)
a = show_deform(0.5)
a = show_deform(0.6)
a = show_deform(0.7)
a = show_deform(0.8)
a = show_deform(0.9)
a = show_deform(10.0)

# %% [markdown]
# <h1>Show results</h1>

# %%


# %%
render_gif(apply_deformation=False, single_frame=False)
asdf

# %%
ptcs_for_shear.size()

# %% [markdown]
# <h1>Visualise deformation field</h1>

# %%
with torch.no_grad():
    #deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc{IMG_SCALE}_deform_init.net")
    indices = torch.randperm(ptc_pts_in.size()[0])[0:1000000]
    subset = ptc_pts_in[indices].clone()
    deformations = deform_net(subset.cuda()).cpu().view(-1)
    subset[:,AXIS_TO_OFFSET] += deformations
    deformations = deformations[:,None].repeat(1,3)
    deformations -= deformations.min()
    deformations /= deformations.max()
    deformations = deformations * 2.0
    deformations = deformations.clamp(0.0, 1.0)
    subset[:,2] *= 0.0
    #print(subset.size())
    #print(deformations.size())
    visualise(subset, deformations, pt_size=0.005)

# %%
offset_range_rnd = offset_range * 0.01 + offset_range * torch.rand_like(offset_range)
#make sure deformation for neighbouring is the same
offset_axis_a = torch.zeros_like(ptcs_for_shear)
offset_axis_b = torch.zeros_like(ptcs_for_shear) #will be z-axis if we don't explicitly use z axis
it = 0
#increase all axis BUT the main axis of deformation - shearing is NEVER on that axis, but on all others!
#WE DO TWO DIRECTIONS OF SHEARING!!!!
for ax in range(0, 3):
    if ax == AXIS_TO_OFFSET:
        continue
    else:
        if it == 0:
            offset_axis_a[:,ax] = offset_range_rnd
            it += 1
        else:
            offset_axis_b[:,AXIS_TO_OFFSET] = offset_range_rnd

#compare deformation at our base point versus the two points on the other axis
deform_base = deform_net(ptcs_for_shear.clone())
deform_a = deform_net(ptcs_for_shear.clone() + offset_axis_a.detach())
deform_b = deform_net(ptcs_for_shear.clone() + offset_axis_b.detach())

deformation_magnitude = ((deform_base - deform_a).abs() + (deform_base - deform_b).abs()).mean(dim=1)
#print("SHEAR: ",loss_shearing.size())
loss_shearing = deformation_magnitude / offset_range_rnd
#print("DIV: ",loss_shearing.size())
energy_points = ptcs_for_shear.clone()
energy_points[:,AXIS_TO_OFFSET:(AXIS_TO_OFFSET+1)] += deform_base
nrg = torch.nn.functional.relu(energy_net(energy_points).detach()[:,0])
loss_shearing_total = (loss_shearing * nrg)

# %%
nrg.size()

# %%
ptcs_for_shear_offset[:,2].min()

# %%


# %%
with torch.no_grad():
    ptcs_for_shear_offset = ptcs_for_shear.clone()
    ptcs_for_shear_offset[:,AXIS_TO_OFFSET] += deform_net(ptcs_for_shear_offset.clone())[:,0]
    
    v_nrg = nrg.clone()[:,None].repeat(1,3)
    v_nrg -= v_nrg.min()
    v_nrg /= v_nrg.max()
    v_nrg[:,1:] *= 0.0 
    visualise(ptcs_for_shear_offset, v_nrg, pt_size=0.001)
    
    v_nrg = deformation_magnitude.clone()[:,None].repeat(1,3)
    v_nrg -= v_nrg.min()
    v_nrg /= v_nrg.max()
    v_nrg[:,1:] *= 0.0 
    visualise(ptcs_for_shear_offset, v_nrg, pt_size=0.001)

    v_nrg = loss_shearing_total.clone()[:,None].repeat(1,3)
    v_nrg -= v_nrg.min()
    v_nrg /= v_nrg.max()
    v_nrg[:,1:] *= 0.0 
    visualise(ptcs_for_shear_offset, v_nrg, pt_size=0.001)
    pass

# %%


# %%


# %%
deform_net = torch.load("/clusterstorage/telsner/NeuralCarving/3D/tmp_deforms/26.net")
render_gif(apply_deformation=True, single_frame=False)
asdf

# %%
deformations.size()

# %%


# %%


# %%
range_x = TARGET_BB[1,0] - TARGET_BB[0,0]
range_y = TARGET_BB[1,1] - TARGET_BB[0,1]

size_factor = range_x / range_y
print(size_factor)

# %%
subset.size()[0]

# %%
deformations.size()

# %%
grid_pts = []
for x in range(0, 200):
    grid_pts.append([])
    for y in range(0, int(size_factor * 200)):
        grid_pts[x].append([])
for k in range(0, subset.size()[0]):
    #get position & value
    x = (ptc_pts_in[k,1] - TARGET_BB[0,1]) / range_y * 200
    y = (ptc_pts_in[k,0] - TARGET_BB[0,0]) / range_x * 200 * size_factor
    x = int(x.item())
    y = int(y.item())
    if x > 199 or y > int(size_factor * 200 - 1):
        continue
    grid_pts[x][y].append(deformations[k,0].item())
    if k % 10000 == 0:
        print("DONE WITH ",k,"/",subset.size()[0])

# %%
import statistics

img = torch.ones(1, len(grid_pts), len(grid_pts[0])) * -1
lrimg = torch.ones(1, int(len(grid_pts)/10+1), int(len(grid_pts[0])/10)+1) * -1
for x in range(0, len(grid_pts)):
    for y in range(0, len(grid_pts[0])):
        if len(grid_pts[x][y]) == 0:
            continue
        img[0,x,y] = statistics.median(grid_pts[x][y])
        lrimg[0,int(x/10),int(y/10)] = statistics.median(grid_pts[x][y])

# %%
show(img.repeat(3,1,1))
show(lrimg.repeat(3,1,1))

# %%
deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_init.net")
render_gif(apply_deformation=True, single_frame=False)

# %%
#deformation field: 

# %%


# %%


# %%
deform_net = torch.load(f"nets/{SCENE_NAME}_ax{AXIS_TO_OFFSET}_cmp{SCALING_FACTOR}_sc0.25_deform_init.net")
render_gif(apply_deformation=True, single_frame=False)

# %%
asdf

# %%
moved_offset = 0.01
import math

#render 80 frames
angle_range = torch.range(start=-4, end=4, step=0.1)

def get_dataloader(step_idx=None, view_idx=None, shuffle=False, patch_size=1, c2ws=None, val_topdown=False):

    if c2ws is None:
        c2ws = globals()['c2ws'].clone()
    
    #c2ws[:,:,0,-1] += moved_offset * 0.2
    #c2ws[:,:,2,-1] += moved_offset * 0.2 #zoom in closer
    c2ws[:,:,2,-1] -= 0.1 #zoom in closer

    #before
    angle = 0.025 * (MOVE_ITS - 4)

    rotation = torch.zeros(3, 3)
    #rotation around x-axis
    rotation[0,0] = 1.0
    rotation[1,1] = math.cos(angle)
    rotation[2,2] = math.cos(angle)

    rotation[1,2] = -math.sin(angle) #switched out 
    rotation[2,1] = math.sin(angle)

    print(c2ws.size())
    for k in range(0, c2ws.size()[1]):
        c2ws[0, k, 0:3, 0:3] = torch.matmul(rotation, c2ws[0, k, 0:3, 0:3])

    #asdf
    
    dl = get_ray_dataloader(images,
                              c2ws,
                              focal_x,
                              focal_y,
                              step_idx,
                              view_idx,
                              shuffle=shuffle,
                              batch_size=args.batchsize,
                              workers=args.workers,
                              patch_size=patch_size, val_topdown=False)

    return dl

MOVE_ITS = 0
iteration = 0
for k in range(0, len(angle_range)):
    MOVE_ITS = k
    show_val_frame_offset(id=1, path="moved/moved_"+str(iteration)+".png", val_topdown=0.0, hide_outside=hide_outside_fnc)
    iteration += 1

# %%



