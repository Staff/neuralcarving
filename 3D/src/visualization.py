import torch
import torchvision
import numpy as np
from matplotlib import pyplot as plt


def to_hex(rgb):
    rgb_255 = (255 * rgb).long()
    rgb_hex = torch.stack([rgb_255[..., i] << 8 * (2 - i) for i in range(3)], -1)
    rgb_hex = rgb_hex.sum(-1)
    return rgb_hex
    

def make_grid(images, nrow='auto', **kwargs):

    if nrow == 'auto':
        # Kinda square
        nrow = int(np.sqrt(len(images)))

    if len(images.shape) > 4:
        images = images.flatten(0, -4)

    grid = images.permute(0, 3, 1, 2)
    grid = torchvision.utils.make_grid(grid, 
                                       nrow=nrow, 
                                       **kwargs)
    grid = grid.permute(1, 2, 0)
    grid = grid[..., :images.shape[-1]] # Do not switch from single to three channels

    return grid


def show_estims(estims, num_plot=4, figscale=6, show=True):

    keys = ['rays_c', 'color_map', 'depth_map', 'alpha_map']

    fig, axs = plt.subplots(len(keys), figsize=(figscale * num_plot, figscale * len(keys)))

    for name, ax in zip(keys, axs):

        data = estims[name][:num_plot]

        _ = ax.set_title(f'{name}  {list(data.shape)}')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        if data.shape[-1] == 3:
            data = data.clip(0, 1) # Suppress warning text

        grid = make_grid(data, num_plot, padding=0)

        img = ax.imshow(grid, cmap='Greys')

        # Add colorbar to greyscale images
        if data.shape[-1] == 1:
            cbax = ax.inset_axes([0, -0.1, 1, 0.1])
            _ = plt.colorbar(img, orientation='horizontal', cax=cbax)
            pass
    
    _ = fig.tight_layout()

    if show:
        _ = plt.show()

    return fig


def show_history(history, title=None, show=True):

    fig = plt.figure()

    _ = plt.plot(history['train'], label=f'train {history["train"][-1]:.4f}')
    _ = plt.plot(history['val'], label=f'validation {history["val"][-1]:.4f}')

    _ = plt.legend()

    if title is not None:
        _ = plt.title(title)

    if show:
        _ = plt.show()

    return fig