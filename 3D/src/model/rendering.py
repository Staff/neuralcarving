# Similar to https://github.com/KAIR-BAIR/nerfacc/blob/master/nerfacc/volrend.py
import torch
from torch import nn


def cumsum_exclusive(x):
    """ Computes `y_i = sum_{j=0..i-1} x_j` along last dimension """
    y = x.cumsum(-1)
    y = y.roll(1, -1)
    y[..., 0] = 0
    return y


def render_rays(rgb, density, ts, background_rgb=None, weights_only=False, hide_outside=None):

    assert density.shape[-1] == 1
    density = density.flatten(-2)

    # Distance between consecutive t values
    density_dt = density * (ts[..., 1:] - ts[..., :-1])

    # Accumulated transmittance
    trans = torch.exp(-cumsum_exclusive(density_dt))

    # Opacity
    alpha = 1 - torch.exp(-density_dt)

    weights = alpha * trans
    if hide_outside != None and False:
        #remove anything that's kinda transparent:
        print("ALPHA: ",alpha.size())
        print("weights: ",weights.size())
        alphasums = alpha.sum(dim=1)
        indices = alphasums < 0.1 #kill
        print("INDICES: ",weights[indices].size())
        weights[indices] = 0.0


    if weights_only:
        return weights

    color_map = (weights[..., None] * rgb).sum(-2)

    alpha_map = weights.sum(-1, keepdim=True)
    
    t_mids = (ts[..., 1:] + ts[..., :-1]) / 2
    depth_map = (weights * t_mids).sum(-1, keepdim=True) / alpha_map
    depth_map = torch.clamp(depth_map.nan_to_num(), ts[..., :1], ts[..., -1:])

    if background_rgb is not None:
        color_map = color_map + (1 - alpha_map) * background_rgb

    return {'color_map': color_map,
            'depth_map': depth_map,
            'alpha_map': alpha_map,
            # Might be needed for resampling
            'trans': trans,
            'max_contribution': weights.max(dim=1)[0] / alpha_map[:,0],
            'weights': weights} #max_contribution