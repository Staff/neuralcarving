import torch
from torch import nn
from torch.nn import functional as F
import numpy as np
import math

try:
    _ = __IPYTHON__
    from tqdm.notebook import tqdm
except Exception:
    from tqdm import tqdm

from .sampling import sample_stratified, sample_cdf, sample_weights
from .rendering import render_rays, cumsum_exclusive
from ..geometry import get_ray_aabb_intersections


def searchsorted(sorted_sequence, values):
    # https://github.com/google-research/multinerf/blob/main/internal/stepfun.py#L30
    # Returns (idx_l, idx_r), where sorted_sequence[idx_l] <= value < sorted_sequence[idx_r]
    # Aka torch.searchsorted with side='right', but it returns low and high indices

    idx = torch.arange(sorted_sequence.shape[-1], device=sorted_sequence.device)

    v_ge_s = values[..., None, :] >= sorted_sequence[..., :, None]

    idx_l = torch.amax(torch.where(v_ge_s, idx[..., :, None], idx[..., :1, None]), -2)
    idx_r = torch.amin(torch.where(~v_ge_s, idx[..., :, None], idx[..., -1:, None]), -2)

    return idx_l, idx_r


def proposal_loss(ts_input, weights_input, ts_target, weights_target):
    # https://github.com/google-research/multinerf/blob/main/internal/stepfun.py#L64

    # Compute bound
    idx_l, idx_r = searchsorted(ts_input, ts_target)
    
    weights_cumsum = torch.cat([torch.zeros_like(weights_input[..., :1]), weights_input.cumsum(-1)], -1)

    weights_l = torch.take_along_dim(weights_cumsum, idx_l, -1)
    weights_r = torch.take_along_dim(weights_cumsum, idx_r, -1)

    weights_bound = weights_r[..., 1:] - weights_l[..., :-1]

    # Compute loss (Eq. 13)
    loss = (weights_target - weights_bound).clip(min=0).square() / (weights_target + 1e-7)

    return loss


def distortion_loss(depth, weights, ts):

    # Normalize each ray to range from 0 to 1
    ts = (ts - ts[..., :1]) / (ts[..., -1:] - ts[..., :1])

    # Compute depth as weighted mean of midpoints
    t_mids = (ts[..., :-1] + ts[..., 1:]) / 2
    depths = (weights * t_mids).sum(-1, keepdim=True)

    # Compute weighted distance of each sample from mean
    peak_dists = (depths - t_mids).abs()
    mean_weighted_dist = (weights * peak_dists).sum(-1)

    return mean_weighted_dist.mean()
    ### CHANGED ###

    t_mids = (ts[..., :-1] + ts[..., 1:]) / 2
    distance_from_peak = (t_mids - depth).abs()
    ray_distortion = (weights * distance_from_peak).mean(-1)
    return ray_distortion

def trans_to_cdf(trans):
    cdf = torch.cat((1 - trans, torch.ones_like(trans[..., :1])), -1)
    return cdf


class NeRFScene(nn.Module):

    def __init__(self, 
                 radiance_fields,
                 num_samples,
                 density_fields=None,
                 num_density_samples=None,
                 near=None,
                 far=None,
                 background_model=None,
                 alpha_thold=None,):
        super(NeRFScene, self).__init__()

        if density_fields is None:
            density_fields = []

        if num_density_samples is None:
            num_density_samples = []

        assert len(radiance_fields) == len(num_samples)
        assert len(density_fields) == len(num_density_samples)

        self.radiance_fields = nn.ModuleList(radiance_fields)
        self.num_samples = num_samples
        self.density_fields = nn.ModuleList(density_fields)
        self.num_density_samples = num_density_samples
        self.near = near
        self.far = far
        self.background_model = background_model
        self.alpha_thold = alpha_thold
    
    def generate_samples(self, rays_o, rays_d, rays_r, offset_function=None, hide_outside=None):

        # Find ray bounds
        if self.near is None or self.far is None:
            t_min, t_max = get_ray_aabb_intersections(rays_o, rays_d, -1, 1)

        near = t_min if self.near is None else self.near
        far = t_max if self.far is None else self.far

        full_num_samples = self.num_density_samples + [self.num_samples[0]]

        # Get initial stratified samples
        ts = sample_stratified(num_rays=len(rays_o),
                               num_samples=full_num_samples[0],
                               near=near,
                               far=far,
                               randomized=self.training,
                               device=rays_o.device)
        
        lvl_outs = []

        # Iteratively sample density fields
        for density_field, num_samples in zip(self.density_fields, full_num_samples[1:]):

            # Evaluate density field
            density = density_field(rays_o, rays_d, rays_r, ts, offset_function=offset_function, hide_outside=hide_outside) #DONE#  ###TODO OFFSET ACCORDING TO DEFORMATION###
            ### TODO OFFSET ACCORDING TO HIDING ###

            weights = render_rays(None, density, ts, weights_only=True, hide_outside=hide_outside)
            
            lvl_outs.append((ts, weights))
            
            # Resample according to weights
            ts, _ = sample_weights(ts, weights, num_samples, randomized=self.training) ###TODO OFFSET ACCORDING TO DEFORMATION###
            ts = ts.detach()

        return ts, lvl_outs
    
    def render_rays(self, rays_o, rays_d, rays_r, ts, background_color=None, offset_function=None, hide_outside=None):

        lvl_outs = []

        full_num_samples = self.num_samples[1:] + [None]

        for radiance_field, num_samples in zip(self.radiance_fields, full_num_samples):
            
            # Evaluate radiance fields
            rgb, density = radiance_field(rays_o, rays_d, rays_r, ts, density_only=False, offset_function=offset_function, hide_outside=hide_outside)

            # Render rays
            # NOT FROM THIS CLASS #
            lvl_out = render_rays(rgb, density, ts, background_color, weights_only=False, hide_outside=hide_outside)
            
            if num_samples is not None:
                # Resample according to weights
                weights = lvl_out['weights']
                ts, _ = sample_weights(ts, weights, num_samples, randomized=self.training)
                ts = ts.detach()
            else:
                lvl_out['ts'] = ts # Add ts for proposal loss

            lvl_outs.append(lvl_out)

        return lvl_outs


    def forward(self, rays_o, rays_d, rays_r, offset_function=None, hide_outside=None):

        # Normalize rays_d (this is a shared assumption of all functions below!)
        rays_d = rays_d / rays_d.norm(p=2, dim=-1, keepdim=True)

        # Evaluate background
        background_color = self.background_model(rays_d) if self.background_model is not None else None

        # Generate samples
        ts, prop_outs = self.generate_samples(rays_o, rays_d, rays_r, offset_function) #DONE# ###TODO OFFSET ACCORDING TO DEFORMATION###

        # Render rays over radiance field
        render_outs = self.render_rays(rays_o, rays_d, rays_r, ts, background_color, offset_function=offset_function, hide_outside=hide_outside) #DONE# ###TODO OFFSET ACCORDING TO DEFORMATION###

        return render_outs, prop_outs
    

class ConstantBackground(nn.Module):

    def __init__(self, rgb):
        super(ConstantBackground, self).__init__()
        self.register_buffer('rgb', rgb)

    def forward(self, rays_d):
        return self.rgb.expand_as(rays_d)
    

def run_scene(scene, 
              dataloader, 
              optim=None, 
              lr_scheduler=None,
              coarse_reg=None,
              prop_reg=None,
              dist_reg=None,
              return_estims=False,):

    # Setup train or validation mode
    train = optim is not None
    torch.set_grad_enabled(train)
    scene.train(train)

    estims = {}
    losses = []

    for rays_o, rays_d, rays_r, rays_c, rays_i in (pbar := tqdm(dataloader, leave=False)):

        # Load data to GPU
        #rays_o = rays_o.cuda() # Optimized for nn.DataParallel loading correct data to GPU
        #rays_d = rays_d.cuda()
        #rays_r = rays_r.cuda()
        rays_c = rays_c.cuda()
        #rays_i = rays_i.cuda() # Currently not used
        
        # Render scene
        render_outs, prop_outs = scene(rays_o, rays_d, rays_r)
        final_render = render_outs[-1] # Final level produces final output

        # Extract data
        color_map = final_render['color_map']
        depth_map = final_render['depth_map']
        max_contribution = final_render['max_contribution']
        alpha_map = final_render['alpha_map']
        ts = final_render['ts']
        weights = final_render['weights']

        # Compute loss
        loss = F.mse_loss(color_map, rays_c)

        # Use extra loss for sum all other losses, because loss is used for PSNR
        extra_loss = torch.zeros(1, dtype=torch.float, device='cuda')

        if coarse_reg is not None:
            for lvl_out in render_outs[:-1]:
                # Multiple sampling lvls
                lvl_loss = coarse_reg * F.mse_loss(lvl_out['color_map'], rays_c)
                extra_loss += lvl_loss

        if prop_reg is not None:
            # Let proposal weights follow output weights
        
            for prop_ts, prop_weights in prop_outs:
                # With stop grad!
                prop_loss = prop_reg * proposal_loss(prop_ts, 
                                                     prop_weights, 
                                                     ts.detach(), 
                                                     weights.detach()).mean()
                extra_loss += prop_loss

        if dist_reg is not None:
            # Concentrate weights on final depth value
            dist_loss = distortion_loss(depth_map.detach(), weights, ts.detach())
            extra_loss += dist_reg * dist_loss.mean()
        
        extra_loss += (alpha_map - 1.0).square().mean() * 0.0001

        # Backward pass
        if optim is not None:

            optim.zero_grad()
            (loss + extra_loss).backward()
            optim.step()

            if lr_scheduler is not None:
                lr_scheduler.step() # One step per batch

        assert not torch.isnan(extra_loss)
        assert not torch.isnan(loss)

        loss = loss.item()

        losses.append(loss)
        pbar.set_description(f'{train = } {loss:.6f}')

        if return_estims:
            # Store current input data            
            for name, data in {# Inputs
                               'rays_o': rays_o, 
                               'rays_d': rays_d, 
                               'rays_r': rays_r, 
                               'rays_c': rays_c,
                               # Outputs
                               'color_map': color_map,
                               'depth_map': depth_map,
                               'max_contribution': max_contribution,
                               'alpha_map': alpha_map}.items():
                if name not in estims:
                    estims[name] = []
                data = data.detach().cpu()
                estims[name].append(data)

    mean_loss = sum(losses) / len(losses)
    mean_psnr = -10 * np.log10(mean_loss)

    torch.set_grad_enabled(True)

    if return_estims:
        # Combine batches
        estims = {name: torch.cat(data) for name, data in estims.items()}
        return mean_loss, mean_psnr, estims
    else:
        return mean_loss, mean_psnr


def unflatten_estims(estims, height, width, views_per_step=None):
    shape = (-1, height, width) if views_per_step is None else (-1, views_per_step, height, width)
    unflat = {name: data.unflatten(0, shape) for name, data in estims.items()}
    return unflat
