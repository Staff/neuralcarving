import torch
from torch.utils.data import TensorDataset, DataLoader
import numpy as np

def get_ray_origins_and_directions(c2ws, height, width, focal_x, focal_y, offset=0.5):

    x = torch.arange(width, dtype=torch.float)
    y = torch.arange(height, dtype=torch.float)

    xx, yy = torch.meshgrid(x, y, indexing='xy')

    rays_d = torch.stack([
         (xx - 0.5 * width + offset) / focal_x,
        -(yy - 0.5 * height + offset) / focal_y,
        -torch.ones_like(xx),
    ], dim=-1,)

    # Decompose c2w matrices
    Rs = c2ws.flatten(0, -3)[:, :3, :3]
    ts = c2ws.flatten(0, -3)[:, :3, -1]

    # NOTE: rays_d are not normalized
    rays_d = (rays_d[None, :, :, None] * Rs[:, None, None]).sum(-1)
    rays_o = ts[:, None, None, :].expand_as(rays_d)

    # Undo flatten
    rays_o = rays_o.unflatten(0, c2ws.shape[:-2])
    rays_d = rays_d.unflatten(0, c2ws.shape[:-2])

    return rays_o, rays_d

def get_ray_radii(rays_d):
    """ Computes rays_r of each pixel determined by unnormalized direction vectors rays_d """

    assert len(rays_d.shape) == 3 and rays_d.shape[-1] == 3 # Needs (H, W, 3) shape

    # Compute distance to neighboring pixel in x and y direction
    dx = (rays_d[:, :-1] - rays_d[:, 1:]).norm(p=2, dim=-1)
    dy = (rays_d[:-1, :] - rays_d[1:, :]).norm(p=2, dim=-1)

    # Add value for last pixel
    dx = torch.cat((dx, dx[:, -1, None]), 1)
    dy = torch.cat((dy, dy[-1, None, :]), 0)

    # Compute rays_r
    rays_r = (dx + dy) / 2
    # Convention of https://arxiv.org/pdf/2103.13415.pdf, section 3.1
    rays_r = rays_r[..., None]  * 2 / np.sqrt(12)

    return rays_r


def get_patches(x, kernel_size, stride=None):

    if stride is None:
        # Set to non-overlapping patches
        stride = kernel_size

    x_flat = x.flatten(0, -4).movedim(-1, -3).float()

    patches_flat = torch.nn.functional.unfold(x_flat, kernel_size=kernel_size, stride=stride)
    patches_flat = patches_flat.unflatten(1, (-1, kernel_size, kernel_size))

    patches = patches_flat.unflatten(0, x.shape[:-3])

    patches = patches.movedim(-1, -4)
    patches = patches.movedim(-3, -1)

    patches = patches.type(x.dtype)

    return patches


def get_ray_dataloader(images,
                       c2ws,
                       focal_x,
                       focal_y, 
                       step_idx=None,
                       view_idx=None,
                       shuffle=False,
                       batch_size=1,
                       workers=0,
                       patch_size=1, val_topdown=False):

    # Build rays

    height, width = images.shape[-3], images.shape[-2]

    if val_topdown != False:
        c2ws[:,:,0,2] += val_topdown
        #c2ws[:,:,2,2] -= val_topdown #increase z axis (height) of cam
    rays_o, rays_d = get_ray_origins_and_directions(c2ws, height, width, focal_x, focal_y)
    
    rays_r = get_ray_radii(rays_d.flatten(0, -4)[0])
    rays_r = rays_r[None].expand(*rays_d.shape[:-1], -1)

    rays_c = images

    rays_i = torch.arange(len(images))[:, None, None, None, None].expand_as(rays_o[..., :1])

    # Check for correct patch sizes

    assert height % patch_size == 0, patch_size
    assert width % patch_size == 0, patch_size
    assert batch_size % (patch_size ** 2) == 0, patch_size

    # Get data subsets

    all_data = rays_o, rays_d, rays_r, rays_c, rays_i

    idx_data = [data[step_idx][:, view_idx] for data in all_data]

    patch_data = [get_patches(data, patch_size) for data in idx_data]
    # Flatten to [num_patches, patchsize ** 2, channels]
    patch_data = [data.flatten(0, 2).flatten(1, 2) for data in patch_data]

    if shuffle:
        # Shuffle patches
        num_patches = len(patch_data[0])
        rand_perm = torch.randperm(num_patches)
        patch_data = [data[rand_perm] for data in patch_data]
        
    flat_data = [data.flatten(0, -2) for data in patch_data]

    # Create dataset and dataloader

    dataset = TensorDataset(*flat_data)

    dataloader = DataLoader(dataset, 
                            batch_size=batch_size, 
                            shuffle=False, # NOTE: Shuffle is handled manually a few lines before
                            num_workers=workers)

    return dataloader


def estimate_scene_normalization(c2ws, height, width, focal_x, focal_y, min_view_dist=0):

    c2ws = c2ws.flatten(0, -3)

    rays_o, rays_d = get_ray_origins_and_directions(c2ws,
                                                    height,
                                                    width,
                                                    focal_x,
                                                    focal_y)
    
    # Only use edgepoints to save ressources
    rays_o = rays_o[:, [0, -1]][:, :, [0, -1]]
    rays_d = rays_d[:, [0, -1]][:, :, [0, -1]]

    # Normalize rays to unit length
    rays_l = rays_d.norm(p=2, dim=-1, keepdim=True)
    rays_n = rays_d / rays_l

    points = (rays_o + rays_n * min_view_dist).flatten(0, -2)

    aabb_min = points.min(0).values
    aabb_max = points.max(0).values

    scene_center = (aabb_min + aabb_max) / 2 # Mean of AABB
    points_shift = points - scene_center

    # "Shortest" ray must span over whole scene
    scene_size = (aabb_max - aabb_min).norm(p=2) # Diagonal across scene
    far = scene_size / rays_l.min()

    max_point_dist = points_shift.norm(p=2, dim=-1).max()

    scene_scale = 1 / (max_point_dist + far * rays_l.max())

    scaled_far = far * scene_scale

    estimators = {
        # AABB
        'aabb_min': aabb_min.tolist(),
        'aabb_max': aabb_max.tolist(),
        'scene_center': scene_center.tolist(),
        # Scaling
        'scene_scale': scene_scale.item(),
        'far': far.item(),
        'scaled_far': scaled_far.item()
    }

    return estimators