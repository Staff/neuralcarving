import torch
import torchvision
import numpy as np
import os
import json
from glob import glob

try:
    _ = __IPYTHON__
    from tqdm.notebook import tqdm
except Exception:
    from tqdm import tqdm

from .image import load_image


def load_blender_data(dataset_path, split=None, min_idx=None, max_idx=None):

    if split is None:
        transform_path = f'{dataset_path}/transforms.json'
    else:
        transform_path = f'{dataset_path}/transforms_{split}.json'

    with open(transform_path, 'r') as f:
        meta = json.load(f)

    images = []
    c2ws = []

    filetypes = ['png', 'jpg', 'jpeg']

    if min_idx is None:
        min_idx = 0

    if max_idx is None:
        max_idx = len(meta['frames'])

    for frame in tqdm(meta['frames'][min_idx:max_idx], desc='Loading blender data'):

        # Read image

        image_path = frame['file_path']
        image_path = f'{dataset_path}/{image_path}'

        for ft in filetypes:
            if image_path.endswith(ft):
                break
            elif os.path.isfile(image_path + '.' + ft):
                image_path += '.' + ft
                break

        image = torchvision.io.read_image(image_path,
                                          torchvision.io.ImageReadMode.RGB)
        image = image.float().permute(1, 2, 0) / 255

        images.append(image)

        # Extract pose

        transform_key = 'transform_matrix' if 'transform_matrix' in frame else 'transformation_matrix'

        c2w = torch.tensor(frame[transform_key],
                           dtype=torch.float)
        
        c2ws.append(c2w)

    images = torch.stack(images)
    c2ws = torch.stack(c2ws)

    height = images.shape[1]
    width = images.shape[2]

    if 'fl_x' in meta:
        focal_x = float(meta['fl_x'])
        focal_y = float(meta['fl_y']) if 'fl_y' in meta else focal_x

    else:
        angle_x = float(meta['camera_angle_x'])
        angle_y = float(meta['camera_angle_y']) if 'camera_angle_y' in meta else angle_x

        focal_x = 0.5 * width / np.tan(angle_x / 2)
        focal_y = 0.5 * height / np.tan(angle_y / 2)

    return images, c2ws, (focal_x, focal_y)
