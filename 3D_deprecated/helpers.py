# ========== IMPORTS ==========
import numpy as np
import torch
import os
import json
import imageio
from PIL import Image
import os

if torch.cuda.is_available():
    import torch.backends.cudnn as cudnn
try:
    import k3d #don't import k3d if we're on the cluster!
    import open3d as o3d
    def visualise(data, colour = torch.zeros(1), pt_size=0.05):
        if data.size()[1] == 3 and data.size()[0] != 3:
            data = data.transpose(0,1)
            colour = colour.transpose(0,1)
        data = data.contiguous()
        colour = colour.contiguous()
        x = np.random.randn(data.size()[1],3).astype(np.float32)
        
        for i in range(data.size()[1]):
            x[i][0] = data[0][i]
            x[i][1] = data[1][i]
            x[i][2] = data[2][i]
        point_size = pt_size#0.005#0.002

        plot = k3d.plot(name='points')
        
        plot.camera_auto_fit = False
        plot.grid_auto_fit = False


        plt_points = k3d.points(positions=x, point_size=point_size)
        plot += plt_points
        plt_points.shader='3d'
        plot.display()

        def RGBTo32bitInt(r, g, b):
            return int('%02x%02x%02x' % (r, g, b), 16)

        f = (np.sum(x**3-.1*x**2,axis=1))

        colormap = k3d.colormaps.basic_color_maps.WarmCool
        colors = k3d.helpers.map_colors(f,colormap,[-2,.1])
        
        for i in range(0, len(colors)):
            if colour.view(-1).size()[0] > 1:
                colors[i] = RGBTo32bitInt(int(colour[0][i] * 255.), int(colour[1][i] * 255.), int(colour[2][i] * 255.))
            else:
                colors[i] = RGBTo32bitInt(0, 0, 0)
        
        plt_points.colors = colors.astype(np.uint32)
        colors = colors.astype(np.uint32)
        plt_points.colors = colors
        plot.render()
        return plot
except ModuleNotFoundError as err:
    def visualise(data, colour = torch.zeros(1)):
        pass

#folder creation
def create_folder(path):
    try:
        os.mkdir(path)
    except:
        pass

# ========== DATA HELPER FUNCTIONS ==========
# Matrix: Translation in z-direction.
def trans_t(t):
    return np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, t],
        [0, 0, 0, 1],
    ])

# Matrix: CCW rotation around x-axis.
def rot_x(phi):
    return np.array([
        [1, 0, 0, 0],
        [0, np.cos(phi), -np.sin(phi), 0],
        [0, np.sin(phi), np.cos(phi), 0],
        [0, 0, 0, 1],
    ])

# Matrix: CCW rotation around y-axis.
def rot_y(th):
    return np.array([
        [np.cos(th), 0, -np.sin(th), 0],
        [0, 1, 0, 0],
        [np.sin(th), 0, np.cos(th), 0],
        [0, 0, 0, 1],
    ])

# Returns a camera transformation matrix, where the camera lies on a circle around the object with a given radius and points towards the object.
# Rotation around x-axis (phi) and radius should be fixed, rotation around y-axis depends on the camera position on circle.
def pose_spherical(theta, phi, radius):
    c2w = trans_t(radius)
    c2w = rot_x(phi/180.*np.pi) @ c2w
    c2w = rot_y(theta/180.*np.pi) @ c2w
    # Flip y and z axis and negate x axis to convert to suitable coordinate system.
    c2w = np.array([[-1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]]) @ c2w
    return c2w

# Function to load data given as images and .json for the camera transforms.
# Used for large_lego_bulldozer dataset.
def load_blender_data_train(basedir, res_mult=1.):  
    # Extract information from .json file
    with open(os.path.join(basedir, 'transforms_{}.json'.format('train')), 'r') as fp:
        meta = json.load(fp)
    
    # Extract image and corresponding transformation matrix
    imgs = []
    poses = []
    for frame in meta['frames']:
        fname = os.path.join(basedir, frame['file_path'] + '.png')
        img = imageio.imread(fname)
        
        new_height, new_width = int(img.shape[0]*res_mult), int(img.shape[1]*res_mult)
        img = Image.fromarray(img).resize((new_height, new_width))
        img = np.asarray(img)
        
        imgs.append(img)
        poses.append(np.array(frame['transform_matrix']))
    imgs = (np.array(imgs) / 255.).astype(np.float32)
    poses = np.array(poses).astype(np.float32)
    
    H, W = imgs[0].shape[:2]
    camera_angle_x = float(meta['camera_angle_x'])
    focal = .5 * W / np.tan(.5 * camera_angle_x)
    focal *= res_mult
        
    return {'images': imgs, 'poses': poses, 'focal': focal}

# Function to load data given as images and .json for the camera transforms.
# Used for dom dataset.
def load_data(basedir):
    # Extract information from .json file
    with open(os.path.join(basedir, 'transforms.json'), 'r') as fp:
        meta = json.load(fp)

    # Extract imahe and corresponding transformation matrix
    imgs = []
    poses = []
    for frame in meta['frames']:
        # If dataset was created on windows, replace backslashes.
        file_path = frame['file_path']
        fname = os.path.join(basedir, file_path)
        img = imageio.imread(fname)
        imgs.append(img)
        poses.append(np.array(frame['transformation_matrix']))
    imgs = (np.array(imgs) / 255.).astype(np.float32)
    poses = np.array(poses).astype(np.float32)

    h, w = meta['h'], meta['w']
    focal_x = float(meta['fl_x'])
    focal_y = float(meta['fl_y'])
    near = meta['near']
    far = meta['far']

    return {'images': imgs, 
            'poses': poses,
            'h': h,
            'w': w,
            'near': near,
            'far': far,
            'focal_x': focal_x, 
            'focal_y': focal_y}

# NOTE: MIP CODE
# Implementation and helpers of integrated positional encoding
def get_pixel_radii(rays_d):
    """ Computes radius of each pixel determined by unnormalized direction vectors rays_d """

    assert len(rays_d.shape) == 3 and rays_d.shape[-1] == 3 # Needs (H, W, 3) shape

    # Compute distance to neighboring pixel in x and y direction
    dx = (rays_d[:, :-1] - rays_d[:, 1:]).norm(p=2, dim=-1)
    dy = (rays_d[:-1, :] - rays_d[1:, :]).norm(p=2, dim=-1)

    # Add value for last pixel
    dx = torch.cat((dx, dx[:, -1, None]), 1)
    dy = torch.cat((dy, dy[-1, None, :]), 0)

    # Compute radius
    rd = torch.min(dx, dy) # NOTE: ideally dx and dy should be equal so pixels are square
    # Convention of https://arxiv.org/pdf/2103.13415.pdf, section 3.1
    rd = rd[..., None]  * 2 / np.sqrt(12)

    return rd

APPLY_DEFORMATION = False
def build_gaussian_frustums(rays_o, rays_d, pixel_radii, ts):
    """ For explanation of arguments see `integrated_positional_encoding` function """

    assert len(rays_o.shape) == 2
    assert rays_o.shape == rays_d.shape
    assert rays_o.shape[:-1] == pixel_radii.shape[:-1]
    assert pixel_radii.shape[-1] == 1
    assert rays_o.shape[:-1] == ts.shape[:-1]

    t0 = ts[..., :-1]
    t1 = ts[..., 1:]

    t_mu = (t0 + t1) / 2
    t_hw = (t1 - t0) / 2 # t_delta in paper (aka half-width)

    # Eq. 7 -- don't ask, it's `stable`

    means = t_mu + (2 * t_mu * t_hw ** 2) / (3 * t_mu ** 2 + t_hw ** 2)

    var_t = (
        t_hw ** 2 / 3 
        - 
        4 * t_hw ** 4 * (12 * t_mu ** 2 - t_hw ** 2) 
        / 
        (15 * (3 * t_mu ** 2 + t_hw ** 2 ) ** 2) 
    )

    var_r = (
        pixel_radii ** 2 
        * 
        (
            t_mu ** 2 / 4 
            + 
            5 * t_hw ** 2 / 12 
            - 
            4 * t_hw ** 4 / (15 * (3 * t_mu ** 2 + t_hw ** 2))
        )
    )

    # Eq. 8

    means = rays_o[..., None, :] + means[..., :, None] * rays_d[..., None, :]

    # Eq. 16

    rdrd = rays_d.square()

    diags = (
        var_t[..., None] * rdrd[..., None, :] 
        + 
        var_r[..., None] 
        * 
        (1 - (rdrd / (rdrd.sum(-1, keepdim=True) + 1e-10))[..., None, :])
    )

    return means, diags

def integrated_positional_encoding(rays_o, rays_d, pixel_radii, ts, L_embed):
    """ 
    As presented by mip-NeRF https://arxiv.org/pdf/2103.13415.pdf 
    
    Arguments:

        rays_o:         torch.tensor of shape `(B, 3)` - ray origins
        rays_d:         torch.tensor of shape `(B, 3)` - unnormalized ray directions from origin to image plane
        pixel_radii:    torch.tensor of shape `(B, 1)` - size of each pixel on the image plane
        ts:             torch.tensor of shape `(B, T)` - fenceposts of `T-1` frustums for each pixel
        L_embed:        int - number of frequencies to consider in embedding

    Returns:

        means:          torch.tensor of shape `(B, T-1, 3)` - means of the gaussian frustums
        diags:          torch.tensor of shape `(B, T-1, 3)` - diagonal entries of the \
                            covariance matrix of the gaussian frustums

    """

    means, diags = build_gaussian_frustums(rays_o, rays_d, pixel_radii, ts)
    
    scales = 2 ** torch.arange(L_embed, dtype=torch.float, device=means.device)

    # Eq. 10
    means_scaled = means[..., None] * scales[[None] * (means.ndim - 1)]

    # Eq. 15
    diags_scaled = diags[..., None] * scales[[None] * (diags.ndim - 1)] ** 2

    # Eq. 14

    diags_scaled = torch.exp(-0.5 * diags_scaled)

    encoding = torch.cat(
        (
            means_scaled.sin() * diags_scaled, 
            means_scaled.cos() * diags_scaled, # Version A
            # Using sine instead of cosine replicates https://github.com/hjxwhy/mipnerf_pl/blob/dev/models/mip.py,
            # but using cosine directly is closer to the math of the original paper.
            # NOTE: cos(x) = sin(x + 0.5 * pi). 
            # (means_scaled + 0.5 * torch.pi).sin() * diags_scaled, # Version B
        ), 
        dim=-1
    )

    encoding = encoding.flatten(-2)

    return encoding.view(-1, 3*2*L_embed), means.view(-1, 3)
# NOTE: MIP CODE END









# Positional encoding function (as in NeRF paper page 7, but without pi).
# Function is applied to each dimension of x separately.
# Results in a output shape of [... x 3*2*L_embed+3].
def positional_encoding(x, L_embed):
    rets = []
    for i in range(L_embed):
        for fn in [torch.sin, torch.cos]:
            rets.append(fn((2. ** i) * x))
    return torch.cat(rets, -1)



# Helper to debug tensors: tells us what device they are (0 or something for GPU, 'cpu' for CPU)
def get_device(tensor):
    try:
        return tensor.get_device()
    except:
        return 'cpu'


# Algorithm to optimise sample positions
#   tl;dr take an input sampling and a pointcloud with weights
#   move samples such that weights per voronoi cell is roughly the same
#   do so by pulling together regions with high density (=get less area hence density per centre) and pull apart regions with low density (=expand to have more error per centre)

def optimise_samples(samples, pts, weights):
    assert(samples.size()[1] == 3)
    assert(pts.size()[1] == 3)
    assert(weights.size()[0] == weights.numel())
    assert(len(weights.size()) == 1)

    samples = samples.clone()

    MS_LR = 0.1
    NO_NEIGHBOURS_PER_CELL = 8 #8 seems good in our test cases

    #samples, pts \in [MANY x 3]
    best_samples = None
    best_score = None
    for it in range(0, 500):
        if it % 500 == 0:
            MS_LR = MS_LR / 2.0
        #get neighbours of sample centres
        neighbours = (samples.unsqueeze(0) - samples.unsqueeze(1)).square().sum(2)
        neighbour_centres = neighbours.topk(k=min(samples.size()[0], NO_NEIGHBOURS_PER_CELL+1), largest=False, sorted=True)[1][:,1:]
        
        centre_assignments = (pts.unsqueeze(0) - samples.unsqueeze(1)).square().sum(2).argmin(dim=0)
        
        #get error per centre
        error_per_centre = torch.zeros(samples.size()[0])
        for s in range(0, samples.size()[0]):
            sample_indices = (centre_assignments == s).nonzero()[:, 0]
            if sample_indices.numel() == 0: #skip empty
                continue
            #centre_assignments
            error_per_centre[s] = weights[sample_indices].sum()
        error_per_centre /= error_per_centre.max()

        #get update directions for cell centres
        #   for NO_NEIGHBOURS_PER_CELL neighbours:
        #       get direction towards neighbours
        #       see if neighbours are above or below our own value (weight the direction vector)
        
        for s in range(0, samples.size()[0]):
            avg_dir = torch.zeros(3)
            for n in range(0, min(samples.size()[0]-1, NO_NEIGHBOURS_PER_CELL)):
                n_index = neighbour_centres[s, n]

                #from centre to neighbour
                dir = samples[n_index] - samples[s]
                ###dir = dir / dir.square().sum().sqrt()

                weight = error_per_centre[n_index] - error_per_centre[s]

                avg_dir = avg_dir + (weight * dir) / NO_NEIGHBOURS_PER_CELL
            
            #update sample by applying direction
            samples[s] = samples[s] + avg_dir * MS_LR
        
        score = error_per_centre.std()
        if best_score == None or score < best_score:
            best_score = score
            best_samples = samples.clone()
        
        #visualise
        if it % 500 == 0 and False:
            print("ITERATION ",it,": ")
            pts = pts.transpose(0,1)
            samples = samples.transpose(0,1)

            visualise(torch.cat((samples, corners), 1), colours_samples)

            pts = pts.transpose(0,1)
            samples = samples.transpose(0,1)

    return best_samples, best_score