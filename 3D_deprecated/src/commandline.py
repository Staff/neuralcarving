import torch
import subprocess
from argparse import ArgumentParser


# Dummy datatypes
class floatlist: pass
class intlist: pass


DEFAULT_OPTIONS = {
    # Dataset
    'dataset':          (str,       None),
    'scenecenter':      (floatlist, [0., 0., 0.]),
    'scenescale':       (float,     1.0),
    'imagescale':       (float,     1.0),

    # Subset
    'trainstart':       (int,       0),
    'trainsize':        (int,       None),
    'trainviews':       (intlist,   None),
    'valviews':         (intlist,   None),

    # Training
    'epochs':           (int,       100),
    'increments':       (int,       0),
    'targetpsnr':       (float,     None),
    'saveevery':        (int,       25),
    'batchsize':        (int,       2048),
    'gpus':             (int,       1),
    'workers':          (int,       6),

    # Model
    'hiddensize':       (int,       128),
    'blocks':           (int,       2),
    'blocksize':        (int,       4),
    'posenc':           (str,       'ipe'),
    'posres':           (int,       18),
    'tablesize':        (int,       19),
    'gridlevels':       (int,       16),
    'direnc':           (str,       'pe'),
    'dirres':           (int,       6),
    'contract':         (bool,      'store_true'),

    # Scene
    'samples':          (intlist,   [100, 100]),
    'near':             (float,     None),
    'far':              (float,     None),
    'background':       (str,       None),

    # Optimizer
    'learningrate':     (float,     5e-4),
    'weightdecay':      (float,     1e-5),
    'lrdecaysteps':     (int,       None),
    'lrdecayrate':      (float,     None),

    # Regularization
    'coarsereg':        (float,     0.1),
    'propreg':          (float,     1.0),
    'distreg':          (float,     None),

    # Checkpoint
    'loadckpt':         (str,       None),
    'saveckpt':         (str,       'results/default'),
    'saveestims':       (bool,      'store_true'),
}


def parse_args(args=None, arg_options=DEFAULT_OPTIONS):

    arg_parser = ArgumentParser()

    for arg, (dtype, defval) in arg_options.items():
        if dtype == bool:
            assert defval == 'store_true' # other not supported
            _ = arg_parser.add_argument('--' + arg, action=defval)
        elif dtype == floatlist:
            _ = arg_parser.add_argument('--' + arg, type=float, default=defval, nargs='*')
        elif dtype == intlist:
            _ = arg_parser.add_argument('--' + arg, type=int, default=defval, nargs='*')
        else:
            _ = arg_parser.add_argument('--' + arg, type=dtype, default=defval)

    args = arg_parser.parse_args(args)

    return args


def args_to_string(args, as_command_line_string=False):

    arg_string = ''

    max_len = max([len(name) for name in args.__dict__.keys()])

    for name, value in args.__dict__.items():

        if as_command_line_string:

            if value is None:
                continue

            if type(value) == bool:
                if value == True: # Only action='store_true' supported
                    arg_string += ' --' + name
            elif type(value) == list:
                arg_string += ' --' + name + ' ' + ' '.join([str(v) for v in value])
            else:
                arg_string += ' --' + name + ' ' + str(value)

        else:
            padding = " " * (max_len - len(name) + 2)
            arg_string += f'{name}:{padding}{value}\n'

    arg_string = arg_string.strip()

    return arg_string


def get_view_idx_string(start, stop, step=1):
    view_idx = range(start, stop, step)
    view_idx_str = [str(i) for i in view_idx]
    view_idx_str = ' '.join(view_idx_str)
    return view_idx_str


def get_free_gpu_memory():
    # Calls nvidia-smi interface via shell
    cmd = 'nvidia-smi -q -d Memory'
    out = subprocess.check_output(cmd, shell=True).decode('UTF-8')
    # Look at cmd output to see whats going on here ;)
    gpus = out.split('\nGPU ')[1:]
    free = [int(gpu.split('Free')[1].split()[1]) for gpu in gpus]
    return torch.tensor(free)
