import torch
import numpy as np
from glob import glob

try:
    _ = __IPYTHON__
    from tqdm.notebook import tqdm
except Exception:
    from tqdm import tqdm

from .image import load_image
from ..geometry import rot_x, rot_y, rot_z


def load_gta_v_data(path, static=False, image_format='png'):

    images = []
    c2ws = []
    timestamps = []

    meta_files = sorted(glob(path + '/*.meta'))

    views_per_step = 0

    for meta_file in tqdm(meta_files, 'Loading GTA V data'):

        base_path = meta_file[:-len('.meta')]

        views_per_step = max(views_per_step, int(base_path.split('-')[-1]) + 1)

        # Load image
        image_file = base_path + ('-static' if static else '') + '.' + image_format
        image = load_image(image_file)
        images.append(image)

        # Load and parse meta
        with open(meta_file, 'r') as f:
            meta_raw = f.read()

        meta_raw = meta_raw.replace(',', '.')
        
        xyz_raw, pry_raw, nffrs_raw, rt_raw = meta_raw.strip().split('\n')

        get_floats = lambda string: [float(s) for s in string.split()]

        # Process extrinsics

        x, y, z = get_floats(xyz_raw)
        pitch, roll, yaw = get_floats(pry_raw)

        t = torch.tensor([x, y, z], dtype=torch.float)
        R = rot_z(yaw) @ rot_y(roll) @ rot_x(pitch + 90)

        c2w = torch.cat([R, t[:, None]], -1)
        c2ws.append(c2w)

        # Process intrinsics

        near, far, fovy, raspect, saspect = get_floats(nffrs_raw)

        assert raspect == saspect # Render and screen aspect ratios must match

        height, width = image.shape[0], image.shape[1]
        focal_y = 0.5 * height / np.tan(np.deg2rad(fovy) / 2)
        focal_x = focal_y

        # Process extra data

        rendertime, timestamp = rt_raw.split()[:2]
        timestamp = int(timestamp)

        timestamps.append(timestamp)

    # Combine tensors
    images = torch.stack(images)
    c2ws = torch.stack(c2ws)
    timestamps = torch.tensor(timestamps, dtype=torch.float)

    # Reshape to match step+view format
    images = images.unflatten(0, (-1, views_per_step))
    c2ws = c2ws.unflatten(0, (-1, views_per_step))
    timestamps = timestamps.unflatten(0, (-1, views_per_step))
    
    return images, c2ws, (focal_x, focal_y), timestamps