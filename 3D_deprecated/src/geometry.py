import torch


def rot_non_tensor_input(func):
    def wrap(x):
        if type(x) != torch.Tensor:
            x = torch.tensor(x, dtype=torch.float)
        return func(x)
    return wrap


def rot_non_batch_input(func):
    def wrap(x):
        if x.ndim < 1:
            return func(x[None])[0]
        else:
            return func(x)
    return wrap


@rot_non_tensor_input
@rot_non_batch_input
def rot_x(deg):
    c = torch.deg2rad(deg).cos()
    s = torch.deg2rad(deg).sin()
    R = torch.zeros(len(deg), 3, 3, device=deg.device)
    R[:, 0, 0] = 1
    R[:, 0, 1] = 0
    R[:, 0, 2] = 0
    R[:, 1, 0] = 0
    R[:, 1, 1] = c
    R[:, 1, 2] = -s
    R[:, 2, 0] = 0
    R[:, 2, 1] = s
    R[:, 2, 2] = c
    return R


@rot_non_tensor_input
@rot_non_batch_input
def rot_y(deg):
    c = torch.deg2rad(deg).cos()
    s = torch.deg2rad(deg).sin()
    R = torch.zeros(len(deg), 3, 3, device=deg.device)
    R[:, 0, 0] = c
    R[:, 0, 1] = 0
    R[:, 0, 2] = s
    R[:, 1, 0] = 0
    R[:, 1, 1] = 1
    R[:, 1, 2] = 0
    R[:, 2, 0] = -s
    R[:, 2, 1] = 0
    R[:, 2, 2] = c
    return R


@rot_non_tensor_input
@rot_non_batch_input
def rot_z(deg):
    c = torch.deg2rad(deg).cos()
    s = torch.deg2rad(deg).sin()
    R = torch.zeros(len(deg), 3, 3, device=deg.device)
    R[:, 0, 0] = c
    R[:, 0, 1] = -s
    R[:, 0, 2] = 0
    R[:, 1, 0] = s
    R[:, 1, 1] = c
    R[:, 1, 2] = 0
    R[:, 2, 0] = 0
    R[:, 2, 1] = 0
    R[:, 2, 2] = 1
    return R


def rot_x_y_z(degx, degy, degz):
    return torch.bmm(rot_z(degz), torch.bmm(rot_y(degy), rot_x(degx)))


def rot_xyz(degxyz):
    return rot_x_y_z(*degxyz.T)


def build_vertex_grid(resolutions, dtype=torch.long, device=None):
    axes = [torch.arange(r, dtype=dtype, device=device) for r in resolutions]
    vertices = torch.meshgrid(*axes, indexing='xy')
    grid = torch.stack(vertices, -1)
    return grid


#@torch.compile()
def get_ray_aabb_intersections(rays_o, rays_d, aabb_mins, aabb_maxs):
    ''' Requires `rays_d` to be normalized! '''
    # https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection.html

    t_aabb_min = (aabb_mins - rays_o) / rays_d
    t_aabb_max = (aabb_maxs - rays_o) / rays_d

    t_min = torch.stack((t_aabb_min, t_aabb_max)).amin(0)
    t_max = torch.stack((t_aabb_min, t_aabb_max)).amax(0)

    t_min = t_min.amax(-1).clamp(min=0)
    t_max = t_max.amin(-1).clamp(min=0)

    invalid_mask = t_max <= t_min
    t_min = torch.where(invalid_mask, torch.nan, t_min)
    t_max = torch.where(invalid_mask, torch.nan, t_max)

    return t_min, t_max
