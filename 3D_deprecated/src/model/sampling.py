import torch
from torch import nn


def sample_stratified(num_rays, 
                      num_samples, 
                      near=0, 
                      far=1, 
                      lindisp=False, 
                      randomized=False, 
                      device=None,):

    if randomized:
        samples = torch.linspace(0, 1, num_samples + 1, device=device)[:-1]
        offsets = torch.rand(num_samples, device=device) / (num_samples + 1)
        samples = samples + offsets
    else:
        samples = torch.linspace(0, 1, num_samples, device=device)

    if type(near) == torch.Tensor:
        near = near[:, None]

    if type(far) == torch.Tensor:
        far = far[:, None]

    if lindisp: 
        # Linear in disparity https://arxiv.org/abs/2103.13415 Eq. 11
        samples =  1 / (samples * 1 / far + (1 - samples) * 1 / near)
    else:
        samples = near + (far - near) * samples

    if len(samples.shape) == 1:
        samples = samples[None].expand(num_rays, -1)

    return samples


def sample_cdf(bins, cdf, num_samples, randomized=False):

    # Sample [0, 1)
    u = sample_stratified(num_rays=len(bins), 
                          num_samples=num_samples,
                          near=0,
                          far=1 - torch.finfo(torch.float).eps,
                          randomized=randomized,
                          device=bins.device)

    mask = u[..., None, :] >= cdf[..., :, None]

    def find_interval(x):
        # Grab the value where `mask` switches from True to False, and vice versa.
        # This approach takes advantage of the fact that `x` is sorted.
        x0, _ = torch.max(torch.where(mask, x[..., None], x[..., :1, None]), -2)
        x1, _ = torch.min(torch.where(~mask, x[..., None], x[..., -1:, None]), -2)
        return x0, x1
    
    bins0, bins1 = find_interval(bins)
    cdf0, cdf1 = find_interval(cdf)

    t = torch.nan_to_num((u - cdf0) / (cdf1 - cdf0)).clip(0, 1)
    samples = bins0 + t * (bins1 - bins0)

    return samples


def pdf_to_cdf(pdf):
    ''' Assumes pdf sums up to 1 '''
    cdf = pdf[..., :-1].cumsum(-1)
    zeros = torch.zeros_like(cdf[..., :1])
    ones = torch.ones_like(cdf[..., :1])
    cdf = torch.cat((zeros, cdf, ones), -1)
    return cdf


def pad_weights(weights, eps=1e-5):
    ''' Assures weights sum up to atleast eps '''
    weights_sum = weights.sum(-1, keepdim=True)
    weights_pad = weights + eps / weights.shape[-1]
    weights_pad = torch.where(weights_sum < eps, weights_pad, weights)
    return weights_pad


def sample_weights(bins, weights, num_samples, randomized=False):
    ''' Inspired by https://github.com/bebeal/mipnerf-pytorch/blob/main/ray_utils.py#L14 '''

    weights = pad_weights(weights)

    # Probability density function (normalized to sum 1)
    pdf = weights / weights.sum(-1, keepdim=True)

    # Cumulative distribution function
    cdf = pdf_to_cdf(pdf)

    samples = sample_cdf(bins, cdf, num_samples, randomized)

    return samples, cdf


class PointCloudSamplePrior(nn.Module):

    def __init__(self, points):
        super(PointCloudSamplePrior, self).__init__()
        self.register_buffer('points', points)

    def __call__(self, queries):
        dists = torch.cdist(queries, self.points)
        min_dists = dists.min(-1).values
        weights = torch.exp(-min_dists.square())
        return weights
    