import torch
from torch import nn
from torch.nn import functional as F


def initialize_activation(activation):
    return activation() if type(activation) == type else activation


def build_mlp(layer_dims, activation=nn.LeakyReLU):

    layers = []

    for i_dim, o_dim in zip(layer_dims, layer_dims[1:]):
        layers.append(nn.Linear(i_dim, o_dim))
        layers.append(initialize_activation(activation))

    layers = layers[:-1] # Discard output activation

    return nn.Sequential(*layers)


def softplus_mod(x):
    ''' 
    Shifted Softplus function to be used as density activation function
    Discussed in Appendix E.2. in https://arxiv.org/pdf/2103.13415.pdf
    '''
    return F.softplus(x - 1)


def sigmoid_mod(x, eps=0.001):
    ''' 
    Widened sigmoid function to be used as rgb activation function
    Discussed in Appendix E.2. in https://arxiv.org/pdf/2103.13415.pdf
    '''
    return (1 + 2 * eps) * torch.sigmoid(x) - eps


class LambdaLayer(nn.Module):

    def __init__(self, fn):
        super(LambdaLayer, self).__init__()
        self.fn = fn

    def forward(self, x):
        return self.fn(x)


class RadianceField(nn.Module):

    def __init__(self,
                 pos_encoder,
                 pos_enc_size,
                 dir_encoder,
                 dir_enc_size,
                 hidden_size=256,
                 num_blocks=2,
                 block_size=4,
                 activation=nn.ReLU,
                 density_activation=nn.ReLU,
                 color_activation=nn.Sigmoid):
        super(RadianceField, self).__init__()

        self.pos_encoder = pos_encoder
        self.dir_encoder = dir_encoder

        self.activation = initialize_activation(activation)
        self.density_activation = initialize_activation(density_activation)
        self.color_activation = initialize_activation(color_activation)

        self.mlp_blocks = nn.ModuleList()

        for b in range(num_blocks):
            layer_dims = [pos_enc_size + (b != 0) * hidden_size] \
                       + [hidden_size] * block_size
            mlp_block = build_mlp(layer_dims, activation=activation)
            self.mlp_blocks.append(mlp_block)

        color_dims = [dir_enc_size + hidden_size, hidden_size, hidden_size // 2, 3]
        self.color_mlp = build_mlp(color_dims, activation=activation)

    def forward(self, rays_o, rays_d, rays_r, ts, density_only=False, offset_function=None):

        pos_enc = self.pos_encoder(rays_o, rays_d, rays_r, ts, offset_function)
        hidden = torch.empty_like(pos_enc[..., :0])

        for mlp_block in self.mlp_blocks:
            hidden = self.activation(hidden)
            hidden = torch.cat((pos_enc, hidden), -1)
            hidden = mlp_block(hidden)

        density = self.density_activation(hidden[..., :1])

        if density_only:
            return density # Early exit

        hidden = self.activation(hidden)

        dir_enc = self.dir_encoder(rays_d)
        dir_enc = dir_enc[:, None].expand(-1, hidden.shape[1], -1)

        hidden = torch.cat((dir_enc, hidden), -1)

        hidden = self.color_mlp(hidden)
        rgb = self.color_activation(hidden)

        return rgb, density


class RadianceFieldAsDensityField(nn.Module):

    def __init__(self, radiance_field):
        super(RadianceFieldAsDensityField, self).__init__()
        self.radiance_field = radiance_field

    def forward(self, rays_o, rays_d, rays_r, ts):
        return self.radiance_field(rays_o, rays_d, rays_r, ts, density_only=True)
    

class DensityField(nn.Module):

    def __init__(self,
                 pos_encoder,
                 pos_enc_size,
                 hidden_size=64,
                 num_blocks=1,
                 block_size=1,
                 activation=nn.ReLU,
                 density_activation=nn.ReLU,
                 color_activation=nn.Sigmoid):
        super(DensityField, self).__init__()

        self.pos_encoder = pos_encoder

        self.activation = initialize_activation(activation)
        self.density_activation = initialize_activation(density_activation)
        self.color_activation = initialize_activation(color_activation)

        self.mlp_blocks = nn.ModuleList()

        for b in range(num_blocks):
            layer_dims = [pos_enc_size + (b != 0) * hidden_size] \
                       + [hidden_size] * block_size
            mlp_block = build_mlp(layer_dims, activation=activation)
            self.mlp_blocks.append(mlp_block)

        self.density_mlp = nn.Linear(hidden_size, 1)

    def forward(self, rays_o, rays_d, rays_r, ts, offset_function=None):

        pos_enc = self.pos_encoder(rays_o, rays_d, rays_r, ts, offset_function)
        hidden = torch.empty_like(pos_enc[..., :0])

        for mlp_block in self.mlp_blocks:
            hidden = self.activation(hidden)
            hidden = torch.cat((pos_enc, hidden), -1)
            hidden = mlp_block(hidden)

        density = self.density_activation(self.density_mlp(hidden))

        return density
