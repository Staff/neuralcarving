import torch
from torch import nn
import math
import functools
import itertools

try:
    import tinycudann as tcnn
except Exception as e:
    print(e, 'Warning, could not load tinycudann.')


def positional_encoding(x, freqs=None):

    scales = 2 ** torch.arange(freqs, 
                               dtype=torch.float, 
                               device=x.device)

    x_enc = scales[[None] * x.ndim] * x[..., None]
    x_enc = torch.stack((x_enc.sin(), x_enc.cos()), -1)

    x_enc = x_enc.flatten(-3)
    
    return x_enc


class PositionalEncoding(nn.Module):

    def __init__(self, freqs):
        super(PositionalEncoding, self).__init__()

        self.freqs = freqs

    def forward(self, x):
        return positional_encoding(x, self.freqs)


def build_gaussian_frustums(rays_o, rays_d, rays_r, ts):
    ''' 
    As presented by mip-NeRF https://arxiv.org/pdf/2103.13415.pdf 
    '''

    t_0 = ts[..., :-1]
    t_1 = ts[..., 1:]

    t_mu = (t_0 + t_1) / 2 # midpoints
    t_dt = (t_1 - t_0) / 2 # half-widths

    # Eq. 7 (1)
    mu_t = t_mu + (2 * t_mu * t_dt**2) / (3 * t_mu**2 + t_dt**2)

    # Eq. 7 (2)
    var_t = (
        t_dt**2 / 3 
        - 
        (
            4 * t_dt**4 
            * 
            (12 * t_mu**2 - t_dt**2) 
            / 
            (15 * (3 * t_mu**2 + t_dt**2 )**2)
        )
    )

    # Eq. 7 (3)
    var_r = (
        rays_r**2 
        * 
        (
            t_mu**2 / 4 
            + 
            5 * t_dt**2 / 12 
            - 
            4 * t_dt**4 / (15 * (3 * t_mu**2 + t_dt**2))
        )
    )

    # Eq. 8 (1)
    means = rays_o[..., None, :] + mu_t[..., :, None] * rays_d[..., None, :]

    # Eq. 16

    rays_d_sq = rays_d.square()
    rays_d_sq_sum = rays_d_sq.sum(-1, keepdim=True).clip(min=1e-10)
    
    diags = (
        var_t[..., None] * rays_d_sq[..., None, :] 
        + 
        var_r[..., None] * (1 - rays_d_sq / rays_d_sq_sum)[..., None, :]
    )

    return means, diags
    

def integrated_positional_encoding(means, diags, num_freqs):
    ''' 
    As presented by mip-NeRF https://arxiv.org/pdf/2103.13415.pdf 
    '''

    assert False, 'Check normalized ray directions and ray radii'

    scales = 2 ** torch.arange(num_freqs, dtype=torch.float, device=means.device)
    
    # Eq. 10
    means_scaled = means[..., None] * scales[[None] * (means.ndim - 1)]

    # Eq. 15
    diags_scaled = diags[..., None] * scales[[None] * (diags.ndim - 1)]**2

    # Eq. 14

    diags_scaled = torch.exp(-0.5 * diags_scaled)

    encoding = torch.cat(
        (
            torch.sin(means_scaled) * diags_scaled, 
            torch.cos(means_scaled) * diags_scaled, # Version A
            # Using sine instead of cosine replicates https://github.com/hjxwhy/mipnerf_pl/blob/dev/models/mip.py,
            # but using cosine directly is closer to the math of the original paper.
            # NOTE: cos(x) = sin(x + 0.5 * pi). 
            # (means_scaled + 0.5 * torch.pi).sin() * diags_scaled, # Version B
        ), 
        dim=-1
    )

    encoding = encoding.flatten(-2)
    
    return encoding


class IntegratedPositionalEncoding(nn.Module):

    def __init__(self, num_freqs):
        super(IntegratedPositionalEncoding, self).__init__()
        self.num_freqs = num_freqs

    def forward(self, rays_o, rays_d, rays_r, ts):
        means, diags = build_gaussian_frustums(rays_o, rays_d, rays_r, ts)
        x_enc = integrated_positional_encoding(means, diags, self.num_freqs)
        return x_enc
    

def contract_fn(x, p=2):
    """ As presented in mip-NeRF 360 https://arxiv.org/pdf/2111.12077.pdf """
    x_norm = x.norm(p=p, dim=-1, keepdim=True)
    x_cont = (2 - 1 / x_norm) * (x / x_norm)
    x_cont = torch.where(x_norm <= 1, x, x_cont)
    return x_cont


class Contract(nn.Module):

    def __init__(self):
        super(Contract, self).__init__()
        # TODO: Add scene to inf rate setting

    def forward(self, x):
        x_cont = contract_fn(x)
        x_cont = x_cont / 2
        return x_cont


class RaySegmentToMidpointWrapper(nn.Module):

    def __init__(self, point_encoder, point_transform=None):
        super(RaySegmentToMidpointWrapper, self).__init__()
        self.point_encoder = point_encoder
        self.point_transform = point_transform

    def forward(self, rays_o, rays_d, rays_r, ts, offset_function=None):
        """ NOTE: You most likely want to normalize rays_d before feeding it to this function! """

        # Convert fenceposts to midpoints
        t_mids = (ts[..., :-1] + ts[..., 1:]) / 2

        # Convert midpoints to sample positions
        points = rays_o[..., None, :] + rays_d[..., None, :] * t_mids[..., :, None]

        #apply offset
        if offset_function != None:
            points = offset_function(points)

        if self.point_transform is not None:
            points = self.point_transform(points)
        
        # Encode points
        encoding = self.point_encoder(points.flatten(0, -2)).unflatten(0, points.shape[:-1])

        return encoding


class MultiResolutionHashGridEncodingTCNN(nn.Module):

    def __init__(self,
                 num_features_in=3,
                 num_levels=16,
                 num_features_per_level=2,
                 log2_hash_table_size=21,
                 min_resolution=16,
                 max_resolution=8192,):
        super(MultiResolutionHashGridEncodingTCNN, self).__init__()

        # Sec. 3 Eq. 3
        growth_factor = math.exp((math.log(max_resolution) - math.log(min_resolution)) / (num_levels - 1))

        hash_config = {
            'otype': 'Grid',
            'type': 'Hash',
            'n_levels': num_levels,
            'n_features_per_level': num_features_per_level,
            'log2_hashmap_size': log2_hash_table_size,
            'base_resolution': min_resolution,
            'per_level_scale': growth_factor,
        }

        self.hash_config = hash_config

        self.encoder = tcnn.Encoding(n_input_dims=num_features_in,
                                     encoding_config=hash_config,
                                     dtype=torch.float,)
        
    def forward(self, points):
        # [-1, 1]^d to [0, 1]^d
        points = (points + 1) / 2
        return self.encoder(points.flatten(0, -2)).unflatten(0, points.shape[:-1])


def build_voxel_offsets(ndim, dtype=torch.long):
    offsets = itertools.product([0, 1], repeat=ndim)
    offsets = torch.tensor(list(offsets), dtype=dtype)
    return offsets


class BaseGridEncoding(nn.Module):

    def __init__(self, num_features_in, num_features_out, resolution):
        super(BaseGridEncoding, self).__init__()

        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        self.resolution = resolution

        nbor_offsets = build_voxel_offsets(num_features_in, dtype=torch.float)
        self.register_buffer('nbor_offsets', nbor_offsets)

        if torch.__version__.startswith('2.'):
            # Compile forward function for speedup
            self.forward = torch.compile(self.forward)
            # The grid encoding needs a large cache for compilation
            # Default is 64, 256 seems suitable for most table and batch sizes
            from torch._dynamo import config
            config.cache_size_limit = max(config.cache_size_limit, 256)
        else:
            print('BaseGridEncoding: Use PyTorch >= 2.0 for speedup.')

    def reset_parameters(self):
        # Sec. 4
        nn.init.uniform_(self.table_data, -1e-4, 1e-4)

    def get_grid_indices(self, points):

        # Map from [-1, 1] to [0, 1)
        points = (points + 1) / 2 * (1 - torch.finfo(points.dtype).eps)

        # Scale points to grid resolution and compute neighbors
        points_scaled = points * (self.resolution - 1)
        neighbors = points_scaled.floor()[..., None, :] + self.nbor_offsets[[None] * (points.ndim - 1)]

        indices = neighbors.long()

        # Compute distance to neighbors and infer normalized weights
        distances = (neighbors - points[..., None, :]).norm(p=2, dim=-1)
        # Maximum distance in a cell is sqrt(num_features_in)
        #weights = torch.softmax(-distances /  math.sqrt(self.num_features_in), -1)
        weights = 1 - distances / math.sqrt(self.num_features_in)
        weights = weights / weights.sum(-1, keepdim=True)

        return indices, weights

    def get_table_indices(self, grid_indices): 
        assert False, 'Not implemented! Use a subclass of this module...'

    def forward(self, points):

        # Find neighboring vertex indices
        grid_indices, weights = self.get_grid_indices(points)

        # Collect data from table
        table_indices = self.get_table_indices(grid_indices)
        table_entries = self.table_data[table_indices]

        # Accumulate over neighbors
        point_features = (weights[..., None] * table_entries).sum(-2)

        return point_features


class FullGridEncoding(BaseGridEncoding):

    def __init__(self, num_features_in, num_features_out, resolution):
        super(FullGridEncoding, self).__init__(num_features_in, num_features_out, resolution)

        dim_multis = resolution ** torch.arange(num_features_in).flip(0)
        self.register_buffer('dim_multis', dim_multis)

        self.table_size = resolution ** num_features_in
        self.table_data = nn.Parameter(torch.empty(self.table_size, 
                                                   self.num_features_out, 
                                                   dtype=torch.float))

        self.reset_parameters()

    def get_table_indices(self, grid_indices):
        table_indices = (grid_indices * self.dim_multis).sum(-1)
        return table_indices


class HashGridEncoding(BaseGridEncoding):

    def __init__(self, num_features_in, num_features_out, resolution, log2_hash_table_size):
        super(HashGridEncoding, self).__init__(num_features_in, num_features_out, resolution)

        # Sec. 3
        HASH_PRIMES = [1, 2654435761, 805459861, 3674653429]
        assert num_features_in <= len(HASH_PRIMES), 'Add more primes in source code if needed'
        self.register_buffer('hash_primes', torch.tensor(HASH_PRIMES[:num_features_in], dtype=torch.long))

        self.table_size = 2 ** log2_hash_table_size
        self.table_data = nn.Parameter(torch.empty(self.table_size, self.num_features_out, dtype=torch.float))

        self.reset_parameters()

    def get_table_indices(self, grid_indices):

        # Multiply with primes
        primed = grid_indices * self.hash_primes

        # Reduce primed indices via bitwise XOR
        xored = primed[..., 0]

        for i in range(1, self.num_features_in):
            xored ^= primed[..., i]

        # Map to table size via modulo
        hash_indices = xored % self.table_size

        return hash_indices


class MultiResolutionHashGridEncoding(nn.Module):

    def __init__(self,
                 num_features_in,
                 num_levels=16,
                 num_features_per_level=2,
                 log2_hash_table_size=19,
                 min_resolution=16,
                 max_resolution=8192,):
        super(MultiResolutionHashGridEncoding, self).__init__()

        self.num_features_in = num_features_in
        self.num_features_in = num_features_in
        self.num_levels = num_levels
        self.num_features_per_level = num_features_per_level
        self.log2_hash_table_size = log2_hash_table_size
        self.min_resolution = min_resolution
        self.max_resolution = max_resolution

        self.num_features_out = num_levels * num_features_per_level

        # Sec. 3 Eq. 3
        growth_factor = math.exp((math.log(max_resolution) - math.log(min_resolution)) / (num_levels - 1))

        # Sec. 3 Eq. 2
        resolutions = (min_resolution * growth_factor ** torch.arange(num_levels) + 1e-7).long()
        self.register_buffer('resolutions', resolutions)

        # Build multi resolution grids
        self.multi_resolution_grids = nn.ModuleList()

        for resolution in resolutions:

            use_hashing = resolution ** num_features_in > 2 ** log2_hash_table_size

            if use_hashing:
                grid = HashGridEncoding(num_features_in, 
                                        num_features_per_level,
                                        resolution,
                                        log2_hash_table_size)
            else:
                grid = FullGridEncoding(num_features_in,
                                        num_features_per_level,
                                        resolution)

            self.multi_resolution_grids.append(grid)

    def forward(self, points):

        all_features = []

        for grid in self.multi_resolution_grids:
            cur_features = grid(points)
            all_features.append(cur_features)

        all_features = torch.cat(all_features, -1)

        return all_features


def components_from_spherical_harmonics(directions, levels=4):
    # From https://github.com/nerfstudio-project/nerfstudio/blob/main/nerfstudio/utils/math.py#L27
    """
    Returns value for each component of spherical harmonics.

    Args:
        levels: Number of spherical harmonic levels to compute.
        directions: Spherical harmonic coefficients
    """
    num_components = levels**2
    components = torch.zeros((*directions.shape[:-1], num_components), device=directions.device)

    assert 1 <= levels <= 5, f"SH levels must be in [1,4], got {levels}"
    assert directions.shape[-1] == 3, f"Direction input should have three dimensions. Got {directions.shape[-1]}"

    x = directions[..., 0]
    y = directions[..., 1]
    z = directions[..., 2]

    xx = x**2
    yy = y**2
    zz = z**2

    # l0
    components[..., 0] = 0.28209479177387814

    # l1
    if levels > 1:
        components[..., 1] = 0.4886025119029199 * y
        components[..., 2] = 0.4886025119029199 * z
        components[..., 3] = 0.4886025119029199 * x

    # l2
    if levels > 2:
        components[..., 4] = 1.0925484305920792 * x * y
        components[..., 5] = 1.0925484305920792 * y * z
        components[..., 6] = 0.9461746957575601 * zz - 0.31539156525251999
        components[..., 7] = 1.0925484305920792 * x * z
        components[..., 8] = 0.5462742152960396 * (xx - yy)

    # l3
    if levels > 3:
        components[..., 9] = 0.5900435899266435 * y * (3 * xx - yy)
        components[..., 10] = 2.890611442640554 * x * y * z
        components[..., 11] = 0.4570457994644658 * y * (5 * zz - 1)
        components[..., 12] = 0.3731763325901154 * z * (5 * zz - 3)
        components[..., 13] = 0.4570457994644658 * x * (5 * zz - 1)
        components[..., 14] = 1.445305721320277 * z * (xx - yy)
        components[..., 15] = 0.5900435899266435 * x * (xx - 3 * yy)

    # l4
    if levels > 4:
        components[..., 16] = 2.5033429417967046 * x * y * (xx - yy)
        components[..., 17] = 1.7701307697799304 * y * z * (3 * xx - yy)
        components[..., 18] = 0.9461746957575601 * x * y * (7 * zz - 1)
        components[..., 19] = 0.6690465435572892 * y * z * (7 * zz - 3)
        components[..., 20] = 0.10578554691520431 * (35 * zz * zz - 30 * zz + 3)
        components[..., 21] = 0.6690465435572892 * x * z * (7 * zz - 3)
        components[..., 22] = 0.47308734787878004 * (xx - yy) * (7 * zz - 1)
        components[..., 23] = 1.7701307697799304 * x * z * (xx - 3 * yy)
        components[..., 24] = 0.6258357354491761 * (xx * (xx - 3 * yy) - yy * (3 * xx - yy))

    return components

class SphericalHarmonicsEncoding(nn.Module):

    def __init__(self, num_levels):
        super(SphericalHarmonicsEncoding, self).__init__()
        self.num_levels = num_levels

    def forward(self, d):
        # NOTE: d must be normalized!
        d_enc = components_from_spherical_harmonics(d, self.num_levels)
        return d_enc
