import torch
from torch import nn
import torchvision

import numpy as np

import sys
import os
import random

class Deform3D(torch.nn.Module):
    def __init__(self):
        super(Deform3D, self).__init__()

        self.L_EMBED_X = 10

        internal_dim = 64

        self.lin_res = torch.nn.Linear(in_features=3*2*self.L_EMBED_X, out_features=internal_dim)

        self.lin_1 = torch.nn.Linear(in_features=3*2*self.L_EMBED_X, out_features=internal_dim)
        self.lin_2 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_3 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_4 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_5 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_6 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_7 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_8 = torch.nn.Linear(in_features=internal_dim, out_features=1)

        self.sigmoid = torch.nn.Sigmoid()

        self.relu = torch.nn.LeakyReLU()

    def positional_encoding(self, x):
        rets = []
        for i in range(self.L_EMBED_X):
            for fn in [torch.sin, torch.cos]:
                rets.append(fn((2. ** i) * x))
        return torch.cat(rets, -1)
    
    def forward(self, x_in):
        x = self.positional_encoding(x_in)
        x_res = self.lin_res(x)

        x = self.relu(self.lin_1(x))
        x = self.relu(self.lin_2(x))
        x = self.relu(self.lin_3(x))
        x = self.relu(self.lin_4(x))
        x = self.relu(self.lin_5(x+x_res))
        x = self.relu(self.lin_6(x))
        x = self.relu(self.lin_7(x))
        x = self.lin_8(x)
        return x