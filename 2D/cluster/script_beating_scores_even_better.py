import numpy as np
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import torch.nn.functional as F
if torch.cuda.is_available():
    import torch.backends.cudnn as cudnn
from torch import autograd
from torch.utils.data import Dataset
###from torchvision import datasets
from torch.utils.data import DataLoader
###from torchvision.utils import save_image
import matplotlib.pyplot as plt
import time
import imageio
from PIL import Image
import numpy as np
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import torch.nn.functional as F
if torch.cuda.is_available():
    import torch.backends.cudnn as cudnn
from torch import autograd
from torch.utils.data import Dataset
###from torchvision import datasets
from torch.utils.data import DataLoader
###from torchvision.utils import save_image
import matplotlib.pyplot as plt
import sys
import imageio
from PIL import Image

CLUSTERMODE = True

for arg in sys.argv:
    if arg[0:3] == "it=":
        ITERATION = int(arg[3:])

print("RUNNING ITERATION ",ITERATION)
print("\tdoing stuff from ",(ITERATION*4), " to ",((ITERATION+1)*4))

ALLOW_SAVE = False
def save_image(img, path):
    print("---> SAVING ",path[:-3] + "dat, allowed: ",ALLOW_SAVE)
    if ALLOW_SAVE:
        torch.save(img, path[:-3] + "dat")

SCENE = [["tower", 6], "beach", "noise", "noise_diagonal", "noise_easy", "noise_nonstraight", ["parkinglot", 10], "balloons", "checkerboard", "tower_shearing", "balloons_"][0]
STRETCH_FACTOR = 0.5
DOWNSCALE_FACTOR = 1
if len(SCENE) == 2:
    DOWNSCALE_FACTOR = SCENE[1]
    SCENE = SCENE[0]

DONT_SHOW_IMAGES = False
SILENT_MODE = False

print_ = print
def print(*args, **kwargs):
    if not SILENT_MODE:
        print_(*args, **kwargs)

DEVICE = torch.device('cuda')

DIM_L_EMBED = 10
def positional_encoding(x):
    rets = []
    for i in range(DIM_L_EMBED):
        for fn in [torch.sin, torch.cos]:
            rets.append(fn((2. ** i) * x))
    return torch.cat(rets, -1)

class NeRF2D(nn.Module):
    def __init__(self):
        global DIM_L_EMBED
        
        super(NeRF2D, self).__init__()

        internal_dim = 128 + 64

        self.lin_res = torch.nn.Linear(in_features=2*2*DIM_L_EMBED, out_features=internal_dim)

        self.lin_1 = torch.nn.Linear(in_features=2*2*DIM_L_EMBED, out_features=internal_dim)
        self.lin_2 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_3 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_4 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_5 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_6 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_7 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_8 = torch.nn.Linear(in_features=internal_dim, out_features=3)

        self.sigmoid = torch.nn.Sigmoid()

        self.relu = nn.LeakyReLU()
    
    def forward(self, x):
        if x.size()[-1] == 2:
            x = positional_encoding(x)

        x_res = self.lin_res(x)

        x = self.relu(self.lin_1(x))
        x = self.relu(self.lin_2(x))
        x = self.relu(self.lin_3(x))
        x = self.relu(self.lin_4(x))
        x = self.relu(self.lin_5(x+x_res))
        x = self.relu(self.lin_6(x))
        x = self.relu(self.lin_7(x))
        x = self.sigmoid(self.lin_8(x))
        return x

class Deform2d(nn.Module):
    def __init__(self):
        global DIM_L_EMBED
        
        super(Deform2d, self).__init__()

        internal_dim = 64

        self.lin_res = torch.nn.Linear(in_features=2*2*DIM_L_EMBED, out_features=internal_dim)

        self.lin_1 = torch.nn.Linear(in_features=2*2*DIM_L_EMBED, out_features=internal_dim)
        self.lin_2 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_3 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_4 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_5 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_6 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_7 = torch.nn.Linear(in_features=internal_dim, out_features=internal_dim)
        self.lin_8 = torch.nn.Linear(in_features=internal_dim, out_features=1)

        self.sigmoid = torch.nn.Sigmoid()

        self.relu = nn.LeakyReLU()
    
    def forward(self, x):
        if x.size()[-1] == 2:
            x = positional_encoding(x)
            
        x_res = self.lin_res(x)

        x = self.relu(self.lin_1(x))
        x = self.relu(self.lin_2(x))
        x = self.relu(self.lin_3(x))
        x = self.relu(self.lin_4(x))
        x = self.relu(self.lin_5(x+x_res))
        x = self.relu(self.lin_6(x))
        x = self.relu(self.lin_7(x))
        x = self.lin_8(x)
        return x

from torch.autograd import grad

def show(img, force=False):
    if force or not DONT_SHOW_IMAGES:
        plt.imshow(img.transpose(0,1).transpose(1,2))
        plt.show()

def output_deformed_image(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch, dontshow=False):
    with torch.no_grad():
        deformed_positions = deform_net(stretched_pos_test)
        pos = stretched_pos_test.clone()
        pos[:,1:] += deformed_positions
        deformed_positions = pos
        test_img = param_net(deformed_positions).transpose(0,1).view(3, DIM_1, DIM_2_TEST).cpu()
        if not dontshow:
            show(test_img)
        #save_image(test_img, "results/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_e"+str(epoch)+"_optimised.png")
        return test_img

def output_gradient_image(param_net, tensor_pos, DIM_1, DIM_2, epoch):
    grid_test_grad = Variable(tensor_pos.to(DEVICE), requires_grad=True)
    test_pos_grad = grid_test_grad
    test_img_grad = param_net(test_pos_grad).transpose(0,1).view(3, DIM_1, DIM_2)
    grd = grad(outputs=test_img_grad.abs().sum(), inputs=grid_test_grad) #test: diff to itself
    output_grad = grd[0].view(DIM_1, DIM_2, -1).abs().sqrt().sum(dim=2).unsqueeze(0)

    output_grad = torch.cat((output_grad, output_grad, output_grad), 0).detach().cpu()
    output_grad -= output_grad.min()
    output_grad /= output_grad.max()
    show(output_grad)
    save_image(output_grad, "results/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_e"+str(epoch)+"_initial.png")

def show_deformation_map(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch):
    test_pos_grad = stretched_pos_test
    test_pos_grad = deform_net(test_pos_grad).transpose(0,1).view(1, DIM_1, DIM_2_TEST)
    
    output_grad = torch.cat((test_pos_grad, test_pos_grad, test_pos_grad), 0).detach().cpu()
    output_grad -= output_grad.min()
    output_grad /= output_grad.max()
    show(output_grad)

    save_image(output_grad, "results/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_e"+str(epoch)+"_deformation.png")

def show_deformation_map_change(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch):
    deformed_positions = deform_net(stretched_pos_test).view(1, DIM_1, DIM_2_TEST)

    #grad:
    diff_a = (deformed_positions[:, 1:, 1:] - deformed_positions[:, :-1, 1:]).abs()
    diff_b = (deformed_positions[:, 1:, 1:] - deformed_positions[:, 1:, :-1]).abs()
    diff = diff_a + diff_b
    
    output_grad = torch.cat((diff, diff, diff), 0).detach().cpu()
    output_grad -= output_grad.min()
    output_grad /= output_grad.max()
    show(output_grad)

    save_image(output_grad, "results/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_e"+str(epoch)+"_deformation_grad.png")

force_recalc_deform = True

def norm(vector):
    #input for 3D would be [b x 3] or [3]
    return (vector.square().sum(dim=-1) + 0.0000001).sqrt()

force_recalc_deform = True

def norm(vector):
    #input for 3D would be [b x 3] or [3]
    return (vector.square().sum(dim=-1) + 0.0000001).sqrt()

force_recalc_deform = True

def norm(vector):
    #input for 3D would be [b x 3] or [3]
    return (vector.square().sum(dim=-1) + 0.0000001).sqrt()

force_recalc_deform = True

def continuous_seam_carve(img, DOWNSCALE_FACTOR, STRETCH_FACTOR, force_recalc, SCENE, ITS_INITIAL, ITS_DEFORM, ITS_OPTIM):
    save_image(img, "results/og.png")
    import copy

    width = img.size()[2]
    height = img.size()[1]

    grid = torch.ones(2, img.size()[1], img.size()[2])
    strip_a = torch.linspace(0, img.size()[1]-1, img.size()[1])[:,None] / img.size()[1]
    strip_b = torch.linspace(0, img.size()[2]-1, img.size()[2])[None,:] / img.size()[2]
    grid[0] = grid[0] * strip_a
    grid[1] = grid[1] * strip_b
    tensor_rgb = img.view(3, -1).transpose(0,1)
    tensor_pos = grid.view(2, -1).transpose(0,1)

    DISTANCE_FOR_GRADIENT = 0.01
    EPSILON = 0.0001
    #stretch factor:
    #    1 = original size
    #  0.5 = half size
    #    2 = double the size
    DIM_1 = img.size()[1]
    DIM_2 = img.size()[2]
    DIM_2_TEST = int(DIM_2 * STRETCH_FACTOR)

    #############################################
    ### STEP 1 - INITIALISE & LEARN IMAGE NET ###
    #############################################

    param_net = NeRF2D().to(DEVICE)
    optimiser = torch.optim.Adam(param_net.parameters(), lr=0.001)

    tensor_rgb = img.view(3, -1).transpose(0,1).to(DEVICE)
    tensor_pos = grid.view(2, -1).transpose(0,1).to(DEVICE)

    best_loss = None
    worst_pixel = None
    best_state = None
    for epoch in range(0, ITS_INITIAL):
        if epoch == 0:
            try:
            #if True:
                if not force_recalc:
                    print("TRY LOAD IMG")
                    print("LOAD: ","net/"+"".join(SCENE.split("_")[:2])+"_scene.net")
                    param_net.load_state_dict(torch.load("net/"+"".join(SCENE.split("_")[:2])+"_scene.net"))
                    print("FK UP LOAD IMG")

                    with torch.no_grad():
                        test_img = param_net(tensor_pos).transpose(0,1).view(3, DIM_1, DIM_2).cpu()
                        print("*** LEARNED: ")
                        show(test_img)
                    break
            except:
                pass
        avg_loss = 0.0
        for s in range(0, 100):
            optimiser.zero_grad()
            
            offset_out = param_net(tensor_pos)
            loss = (offset_out - tensor_rgb).square().mean()
            largest_error_pp = (offset_out - tensor_rgb).abs().max()
            if worst_pixel == None or worst_pixel > largest_error_pp:
                worst_pixel = largest_error_pp.item()
            
            avg_loss += loss.item()
            loss.backward()

            optimiser.step()
        print("*** DONE WITH IMAGE EPOCH ",epoch,"/",ITS_INITIAL," OF STEP 1 (LEARNING THE IMAGE) ***")
        print("\tavg error: ",avg_loss/100.0)
        
        if epoch % 10 == 0 or epoch == ITS_INITIAL-1:
            with torch.no_grad():
                test_img = param_net(tensor_pos).transpose(0,1).view(3, DIM_1, DIM_2).cpu()
                show(test_img)
        if best_loss == None or best_loss > avg_loss:
            if best_loss == None:
                print("\tnew lowest error rate - worst pixel currently has a rounded error of ",int(largest_error_pp.item() * 255.0)," (in 0 to 255 RGB)")

            best_loss = avg_loss
            
            if epoch > int(ITS_INITIAL / 2):
                torch.save(param_net.state_dict(), "net/"+"".join(SCENE.split("_")[:2])+"_scene.net")

    print("*** DONE WITH LEARNING THE IMAGE ***")
    #############################################
    ###  STEP 2 - INITIALISE DEFORMATION NET  ###
    #############################################

    #input samples for the deformed net; but as many input samples as deformed
    strip_a = torch.linspace(0, DIM_1-1, DIM_1)[:,None] / DIM_1
    strip_b = torch.linspace(0, DIM_2-1, int(DIM_2))[None,:] / DIM_2
    grid_test = torch.ones(2, img.size()[1], int(DIM_2))
    grid_test[0] = grid_test[0] * strip_a                  # e.g. [0.0, 1.0]
    grid_test[1] = grid_test[1] * strip_b * STRETCH_FACTOR # e.g. [0.0, 0.5]
    target_dim_1 = grid_test.size()[1]
    target_dim_2 = grid_test.size()[2]
    stretched_pos_train = grid_test.view(2, -1).transpose(0,1).to(DEVICE)
    stretched_pos_train_emb = stretched_pos_train

    #initialise samples on which we regularise
    sample_grid_stretched = torch.ones(2, DIM_1, DIM_2_TEST)
    strip_a = torch.linspace(0, DIM_1-1, DIM_1)[:,None] / DIM_1
    strip_b = torch.linspace(0, DIM_2_TEST-1, int(DIM_2_TEST))[None,:] / DIM_2_TEST
    grid_test = torch.ones(2, img.size()[1], int(DIM_2_TEST))
    grid_test[0] = grid_test[0] * strip_a                  # e.g. [0.0, 1.0]
    grid_test[1] = grid_test[1] * strip_b * STRETCH_FACTOR # e.g. [0.0, 0.5]
    stretched_pos_test = grid_test.view(2, -1).transpose(0,1).to(DEVICE)

    sample_grid_stretched = torch.ones(2, DIM_1, DIM_2_TEST)
    strip_a = torch.linspace(-1, DIM_1, DIM_1 + 2)[:,None] / DIM_1
    strip_b = torch.linspace(-1, DIM_2_TEST, DIM_2_TEST + 2)[None,:] / DIM_2_TEST
    grid_test = torch.ones(2, img.size()[1] + 2, DIM_2_TEST + 2)
    grid_test[0] = grid_test[0] * strip_a                  # e.g. [0.0, 1.0]
    grid_test[1] = grid_test[1] * strip_b * STRETCH_FACTOR # e.g. [0.0, 0.5]
    stretched_pos_test_wider = grid_test.view(2, -1).transpose(0,1).to(DEVICE)

    best_loss = None
    loaded = False

    deform_net = Deform2d().to(DEVICE)
    optimiser = torch.optim.Adam(deform_net.parameters(), lr=0.001)

    for epoch in range(0, ITS_DEFORM):
        avg_loss = 0.0
        for s in range(0, 100):
            optimiser.zero_grad()
            
            if epoch == 0 and s == 0:
                try:
                    if not force_recalc_deform and not CLUSTERMODE:
                        deform_net.load_state_dict(torch.load("net/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_deform_"+str(STRETCH_FACTOR)+".net"))
                        deform_net.train(True)
                        optimiser = torch.optim.Adam(deform_net.parameters(), lr=0.001)
                        loaded = True
                        break
                except:
                    pass

            deformed_pos = stretched_pos_train.clone()
            offset_out = deform_net(deformed_pos)
            deformed_pos[:,1:] += offset_out
            loss = (deformed_pos - tensor_pos).square().mean()
            avg_loss += loss.item()

            if best_loss == None or best_loss > loss.item() and not CLUSTERMODE:
                best_loss = loss.item()

                if epoch > int(ITS_DEFORM / 2):
                    torch.save(deform_net.state_dict(), "net/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_deform_"+str(STRETCH_FACTOR)+".net")
            loss.backward()
            optimiser.step()
        
        if epoch % 10 == 0:
            with torch.no_grad():
                deformed_positions = deform_net(stretched_pos_test)
                pos = stretched_pos_test.clone()
                pos[:,1:] += deformed_positions
                deformed_positions = pos
                test_img = param_net(deformed_positions).transpose(0,1).view(3, DIM_1, DIM_2_TEST).cpu()
                show(test_img)
                save_image(test_img, "results/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_e"+str(epoch)+"_optimised.png")

        if loaded:
            break
        print("*** DONE WITH INITIALISATION EPOCH ",epoch,"/",ITS_DEFORM," OF STEP 2 (LEARNING THE INITIAL DEFORMATION)  ***")
        print("\t\tavg_loss: ",avg_loss/100.0)


    ########################################################
    ########################################################
    ################### CUM GRAD ###########################
    ########################################################
    ########################################################


    ### TEST ###
    def gradient(x):
        print(x.size())
        h_x = x.size()[-2]
        w_x = x.size()[-1]
        print(h_x," - ",w_x)

        left = x
        right = F.pad(x, [0, 1, 0, 0])[:, :, 1:]
        top = x
        bottom = F.pad(x, [0, 0, 0, 1])[:, 1:, :]

        dx, dy = right - left, bottom - top 
        dx[:, :, -1] = 0
        dy[:, -1, :] = 0

        return ((dx.square().sum(dim=0) + 0.000001).sqrt() + (dy.square().sum(dim=0) + 0.000001).sqrt()) / 2.0
    
    print("SIZE: ",img.size())
    grd = gradient(img)
    cumsum_grd = torch.cumsum(grd, 1)
    cumsum_grd /= cumsum_grd.size()[1]
    print("CUMSUM: ",cumsum_grd.size())
    #visualise:
    show(cumsum_grd.unsqueeze(0).repeat(3,1,1) / cumsum_grd.max())
    print(grd.size())
    print(grd.min())
    print(grd.max())

    cumgrad_net = Deform2d().to(DEVICE)
    optimiser_cumgrad_net = torch.optim.Adam(cumgrad_net.parameters(), lr=0.0001)
    loaded = False
    best_loss = None
    print("RANGE 1: ",stretched_pos_train[:,0].max())
    print("RANGE 2: ",stretched_pos_train[:,1].max())

    pts_for_cumgrad = stretched_pos_train.clone().to(DEVICE)
    pts_for_cumgrad[:,1] /= STRETCH_FACTOR

    print("RANGE 1: ",pts_for_cumgrad[:,0].max())
    print("RANGE 2: ",pts_for_cumgrad[:,1].max())
    print("SIZE: ",pts_for_cumgrad.size())
    cumsum_grd = cumsum_grd.to(DEVICE)
    cumsum_grd /= cumsum_grd.max()
    for epoch in range(0, ITS_INITIAL):
        avg_loss = 0.0
        for s in range(0, 100):
            optimiser_cumgrad_net.zero_grad()
            
            #intially, learn identity
            if epoch == 0 and s == 0:
                try:
                    if not force_recalc_deform:
                        print("TRY LOAD")
                        cumgrad_net.load_state_dict(torch.load("net/"+"".join(SCENE.split("_")[:2])+"_cumgrad.net"))
                        print("LOADED")
                        cumgrad_net.train(True)
                        optimiser_cumgrad_net = torch.optim.Adam(cumgrad_net.parameters(), lr=0.001)
                        loaded = True
                        break
                except:
                    pass

            cumsum_out = cumgrad_net(pts_for_cumgrad.clone())
            loss = (cumsum_out - cumsum_grd.view(-1)[:,None]).square().mean()
            avg_loss += loss.item()

            if best_loss == None or best_loss > loss.item():
                if best_loss == None:
                    print("\tnew lowest error rate")
                best_loss = loss.item()

                if epoch > int(ITS_INITIAL / 2):
                    torch.save(cumgrad_net.state_dict(), "net/"+"".join(SCENE.split("_")[:2])+"_cumgrad.net")
            loss.backward()
            optimiser_cumgrad_net.step()
        
        if epoch % 10 == 0:
            with torch.no_grad():
                cumgrad_out = cumgrad_net(pts_for_cumgrad.clone())
                test_img = cumgrad_out.view(1, DIM_1, DIM_2).cpu().repeat(3,1,1)
                test_img -= test_img.min()
                test_img /= test_img.max()
                print("CUMULATIVE GRADIENT, LEARNED:")
                show(test_img)
                save_image(test_img, "results/"+SCENE+"_"+str(DOWNSCALE_FACTOR)+"_cumgrad_"+str(epoch)+"_optimised.png")

        if loaded:
            break
        print("*** DONE WITH CUMULATIVE EPOCH ",epoch,"/",ITS_INITIAL," OF STEP 2 (LEARNING THE CUMULATIVE GRADIENT)  ***")
        print("\t\tavg_loss: ",avg_loss/100.0)
    
    cumgrad_net.train(False)



    #############################################
    ### STEP 3 - OPTIMISE DEFORMATION NETWORK ###
    #############################################
    best_loss = None

    #test: does initialisation fuck us up?
    ##deform_net = Deform2d().to(DEVICE)
    
    AX = 1
    OAX = 0

    optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.001, weight_decay=0.0)
    if STRETCH_FACTOR > 1.0:
        optimiser = torch.optim.AdamW(deform_net.parameters(), lr=0.0001, weight_decay=0.0)
    best_image = None
    best_loss  = None
    best_epoch = 0
    for epoch in range(0, ITS_OPTIM):
        if VERSION == "v4" and epoch > 30:
            break
        avg_loss = 0.0

        avg_loss_mono       = 0.0
        avg_loss_boundaries = 0.0
        avg_loss_gradients  = 0.0
        avg_loss_shearing   = 0.0
        avg_loss_cap        = 0.0

        loss_amp = 1.0
        #decay LR last few iterations
        if epoch > int(ITS_OPTIM * 0.75):
            loss_amp = (ITS_OPTIM - epoch) / ITS_OPTIM * 4.0
        
        lambdas = dict()

        lambdas['mono']   = 10000.0
        lambdas['bound']  = 10000.0
        lambdas['grad']   =  1000.0# * 0.0
        lambdas['shear']  =   250.0
        lambdas['cap']    =    10.0# * 0.0

        if VERSION == "v1":
            lambdas['grad'] *= 1.0
        elif VERSION == "v2":
            lambdas['grad'] *= 5.0
        elif VERSION == "v3":
            lambdas['grad'] *= 0.2

        for s in range(0, 100):
            optimiser.zero_grad()

            #new loss:
            # a) punish non-monotonicity of the deformation function
            #    (for case of extending: invert his, punish not monotonically falling functions!)
            
            ### MONOTONICITY ###
            if True:
                tensor_pos_a = stretched_pos_test.clone()
                tensor_pos_b = stretched_pos_test.clone()
                offset = torch.rand_like(tensor_pos_b) * 0.05 + 0.0025
                offset[:,OAX] = 0.0
                tensor_pos_b += offset

                offset_a = deform_net(tensor_pos_a)
                offset_b = deform_net(tensor_pos_b)
                
                if STRETCH_FACTOR < 1.0: #shrinkage
                    #make sure the further right we go in the image, the more offset we have
                    #i.e. point more to the left should ALWAYS be smaller than the one on it's right
                    loss_mono = torch.nn.functional.relu(offset_a - offset_b).mean()
                else:
                    loss_mono = torch.nn.functional.relu(offset_b - offset_a).mean()
            ### MONOTONICITY ###

            
            # b) 1) make sure it's 1 right (i.e. identity for boundaries)
            tensor_right_side  = torch.rand(1000, 2, device=DEVICE)
            target_positions_right = tensor_right_side.clone()
            target_positions_right[:, 1:] = 1.0
            tensor_right_side[:, 1:] = STRETCH_FACTOR #set to right side with random y-axis value
            offset_out_ones = deform_net(tensor_right_side)
            tensor_right_side[:, 1:] += offset_out_ones #should add up to one



            # b) 2) make sure it's 0 left (i.e. identity for boundaries)
            tensor_left_side  = torch.rand(1000, 2, device=DEVICE)
            target_positions_left = tensor_left_side.clone()
            target_positions_left[:, 1:] = 0.0
            tensor_left_side[:, 1:] = 0.0 #set to right side with random y-axis value
            offset_out_ones = deform_net(tensor_left_side)
            tensor_left_side[:, 1:] += offset_out_ones #should add up to one

            loss_boundaries = (target_positions_right - tensor_right_side).square().mean() + (target_positions_left - tensor_left_side).square().mean()
            
            # c) optimise cuts through gradients:
            # (gradient of colour) * (gradient of position != 1) should be punished(?)
            loss_gradients = 0.0
            if True:
                pt_x = stretched_pos_test.clone() #in[0,1] for dim 0, in[0,0.5] for dim 1

                D_pt_x = pt_x.clone()
                offset_pt_x = deform_net(D_pt_x.clone())
                D_pt_x[:,AX:(AX+1)] += offset_pt_x.clone()
                if STRETCH_FACTOR > 1.0: #expansion: fix boundary losses
                    offsets = D_pt_x[:,AX]
                    
                    smaller = offsets < 0.0
                    if offsets[smaller].size()[0] > 0:
                        loss_boundaries += -offsets[smaller].mean()

                    bigger = offsets > 1.0
                    if offsets[bigger].size()[0] > 0:
                        loss_boundaries += offsets[bigger].mean()
                        
                pt_x_eps = pt_x.clone()
                pt_x_eps[:,AX:(AX+1)] += DISTANCE_FOR_GRADIENT
                D_pt_x_eps = pt_x_eps.clone()
                offset_pt_x_eps = deform_net(D_pt_x_eps.clone())
                D_pt_x_eps[:,AX:(AX+1)] += offset_pt_x_eps

                pt_y_eps = pt_x.clone()
                pt_y_eps[:,OAX:(OAX+1)] += DISTANCE_FOR_GRADIENT
                D_pt_y_eps = pt_y_eps.clone()
                D_pt_y_eps[:,AX:(AX+1)] += deform_net(D_pt_y_eps.clone())

                if True: ### !!! IF WE REACTIVATE, PUT BACK !!! ###
                    #don't step over boundary, could be ugly:
                    D_pt_x_eps = D_pt_x_eps.clamp(0.0, 1.0)
                    D_pt_y_eps = D_pt_y_eps.clamp(0.0, 1.0)
                    D_pt_x = D_pt_x.clamp(0.0, 1.0)
                
                #deformation magnitude: how much more we translated ASIDE from the initial distance
                deformation_magnitude = torch.abs(D_pt_x_eps[:,AX] - (D_pt_x[:,AX] + DISTANCE_FOR_GRADIENT))
                #energy: how much energy the base PLUS deformation has
                energy = torch.abs(cumgrad_net(D_pt_x_eps.clone()) - cumgrad_net(D_pt_x.clone()))[:,0].detach()
                
                if STRETCH_FACTOR > 1.0: #expansion: punish LOCAL gradient, no need for integral stuff (we can't jump over things)
                    D_pt_eps_x = D_pt_x.clone()
                    D_pt_eps_x[:,AX:(AX+1)] += DISTANCE_FOR_GRADIENT
                    energy = ((param_net(D_pt_eps_x.clone()) - param_net(D_pt_x.clone())).square().sum(-1).abs() + 0.000001).sqrt().detach()
                energy *= 10.0

                #colour change per unit
                if s == 99 and epoch % 5 == 0:
                    print("---SIZE ENERGY: ",energy.size())
                    print("---SIZE DEFORM:", deformation_magnitude.size())
                    #print("energy RANGE: ",energy.min(), " to ",energy.max())
                    
                    base_error = energy
                    img = base_error.clone().view(DIM_1, DIM_2_TEST)[None].repeat(3,1,1).contiguous()
                    img -= img.min()
                    img /= img.max()
                    print("!!! ENERGY: ")
                    img[1:] *= 0
                    show(img)
                    print("IMG C: ",img.size())
                    dimg = deformation_magnitude.clone().view(DIM_1, DIM_2_TEST)[None].repeat(3,1,1).contiguous()
                    dimg /= dimg.max()
                    print("!!! DEFORMATION MAGNITUDE: ")
                    show(dimg)

                    print("!!! COMBINED: ")
                    combined = dimg[0] * img[0]
                    combined /= combined.max()
                    combined = combined[None].repeat(3,1,1)
                    show(combined)
                
                energy = energy.view(DIM_1, DIM_2_TEST)
                loss_gradients = (deformation_magnitude.view(DIM_1, DIM_2_TEST) * energy.clone()).mean()

                if False:
                    #clamp small energy values to zero such that our shearing is OK to go occur there; else, we overly punish many seams, even if they're barely noticeable
                    energy = energy.view(-1).detach()
                    tiny = energy < 0.2
                    energy[tiny] *= 0.0
                    energy = energy.view(DIM_1, DIM_2_TEST).detach()
                    if s == 99 and epoch % 5 == 0:
                        base_error = energy
                        img = base_error.clone().view(DIM_1, DIM_2_TEST)[None].repeat(3,1,1).contiguous()
                        img -= img.min()
                        img /= img.max()
                        print("!!! ENERGY CLAMPED: ")
                        show(img)

                #for shearing, only take energy
                if True:
                    D_pt_eps_x = D_pt_x.clone()
                    D_pt_eps_x[:,AX:(AX+1)] += DISTANCE_FOR_GRADIENT
                    energy = ((param_net(D_pt_eps_x.clone()) - param_net(D_pt_x.clone())).square().sum(-1).abs() + 0.000001).sqrt().detach().view(DIM_1, DIM_2_TEST)
                
                energy_for_shearing = energy.clone()

            # e) optimise to NOT perform shearing
            loss_shearing = torch.zeros(1, device=DEVICE)
            if True:
                #1. get points
                pt_x = stretched_pos_test.clone()
                pt_y = pt_x.clone()
                pt_y[:,OAX:(OAX+1)] += DISTANCE_FOR_GRADIENT

                #2. get their offset on y-axis
                D_pt_x = pt_x.clone()
                D_pt_y = pt_y.clone()
                offset_x = deform_net(D_pt_x.clone())
                offset_y = deform_net(D_pt_y.clone())
                diff = (offset_x - offset_y).abs()

                diff = diff.view(DIM_1, DIM_2_TEST).contiguous()
                #3. visualise that error
                if s == 99 and epoch % 5 == 0:
                    img = diff.clone()[None].repeat(3,1,1)
                    img /= img.max()
                    print("\nSHEARING unweighted:")
                    show(img)

                diff = diff * energy_for_shearing

                if s == 99 and epoch % 5 == 0:
                    img = diff.clone()[None].repeat(3,1,1)
                    img /= img.max()
                    print("\nSHEARING WEIGHTED:")
                    show(img)

                if False: ### NEW: pool so that we don't punish single, small seams
                    pool = torch.nn.MaxPool2d(kernel_size=5, stride=1, padding=1)
                    diff = -pool(-diff[None])
                    #diff = -pool(-diff)
                    if s == 99 and epoch % 5 == 0:
                        img = diff.clone().repeat(3,1,1)
                        img /= img.max()
                        print("\nSHEARING WEIGHTED POOLED:")
                        show(img)

                loss_shearing = diff.mean()

            loss_cap = torch.zeros(1, device=DEVICE).mean()
            if STRETCH_FACTOR > 1.0:
                loss_cap = torch.nn.functional.relu(deformation_magnitude/DISTANCE_FOR_GRADIENT - STRETCH_FACTOR) ** 2.0#/DISTANCE_FOR_GRADIENT - STRETCH_FACTOR)
                if epoch % 5 == 0 and s == 99:
                    vis = loss_cap.view(DIM_1, DIM_2_TEST).detach()
                    print("1#!#1#1#1# CAP VIS")
                    show(vis[None].repeat(3,1,1)/vis.max())
                loss_cap = loss_cap.mean()

            loss = loss_mono * lambdas['mono'] + loss_boundaries * lambdas['bound'] + loss_gradients * lambdas['grad'] + loss_shearing * lambdas['shear']
            if STRETCH_FACTOR > 1.0:
                loss += loss_cap * lambdas['cap']
            #loss = loss_mono * lambdas['mono'] + loss_boundaries * lambdas['bound']# + loss_gradients * lambdas['grad'] + loss_shearing * lambdas['shear']
            
            loss = loss * loss_amp

            avg_loss_mono += loss_mono.item()
            avg_loss_boundaries += loss_boundaries.item()
            avg_loss_gradients += loss_gradients.item()
            avg_loss_shearing += loss_shearing.item()
            avg_loss_cap += loss_cap.item()
            
            avg_loss += loss.item()

            loss.backward()

            if epoch == 0:
                optimiser.zero_grad()
                break
            optimiser.step()

            #check if we got a new best LR:

        print("AVG LOSS: ",avg_loss," vs BEST: ",best_loss)
        if (best_loss == None or avg_loss < best_loss) or epoch == 1:
            best_epoch = epoch
            best_loss = avg_loss
            best_image = output_deformed_image(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch, dontshow=True)

        print("*** DONE WITH OPTIMISATION EPOCH ",epoch," OF STEP 3 (OPTIMISING THE DEFORMATION)  ***")
        print("\tavg error: ",avg_loss/100.0," - components: ")
        print("\t\t",(loss_mono * lambdas['mono']).item()," = ",lambdas['mono']," * ",avg_loss_mono/100.0,"(mono)")
        print("\t\t",(loss_boundaries * lambdas['bound']).item()," = ",lambdas['bound']," * ",avg_loss_boundaries/100.0,"(bound)")
        print("\t\t",(loss_gradients * lambdas['grad']).item()," = ",lambdas['grad']," * ",avg_loss_gradients/100.0,"(grad)")
        print("\t\t",(loss_shearing * lambdas['shear']).item()," = ",lambdas['shear']," * ",avg_loss_shearing/100.0,"(shear)")
        print("\t\t",(loss_cap * lambdas['cap']).item()," = ",lambdas['cap']," * ",avg_loss_cap/100.0,"(cap)")

        if epoch % 10 == 0 or epoch % 5 == 0:
            if epoch == 0:
                output_gradient_image(param_net, tensor_pos, DIM_1, DIM_2, epoch) #in original image
            show_deformation_map(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch)
            show_deformation_map_change(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch)
            
            print("OUTPUT")
            last_output = output_deformed_image(deform_net, param_net, stretched_pos_test, DIM_1, DIM_2_TEST, epoch)
            print("CURRENT BEST (FROM IT ",best_epoch,"): ")
            show(best_image)
    return last_output
    
    
    
    
    
ITERATION_MULTIPLIER = 0
def resize_image(img, factor_x, factor_y = 1.0, force_recalc=False):
    height = img.shape[0] // DOWNSCALE_FACTOR
    width = img.shape[1] // DOWNSCALE_FACTOR
    img = Image.fromarray(img).convert("RGBA")
    img = img.resize((width, height), Image.Resampling.LANCZOS)
    img = torch.tensor(np.array(img)).transpose(1,2).transpose(1,0).float()[0:3] / 255.0

    print("INITIAL: ")
    show(img, force=False)
    if factor_x != 1.0:
        img = continuous_seam_carve(img, DOWNSCALE_FACTOR, factor_x, force_recalc, SCENE, int(2 * 50 * ITERATION_MULTIPLIER), int(20 * ITERATION_MULTIPLIER), int(10 * ITERATION_MULTIPLIER))
    print("TEMPORARY: ")
    show(img, force=False)
    if factor_y != 1.0:
        img = continuous_seam_carve(img.transpose(1,2).contiguous(), DOWNSCALE_FACTOR, factor_y, force_recalc, SCENE, int(2 * 50 * ITERATION_MULTIPLIER), int(20 * ITERATION_MULTIPLIER), int(10 * ITERATION_MULTIPLIER)).transpose(1,2).contiguous()
    print("OUTPUT: ")
    show(img, force=False)
    return img

#try:
#    img = imageio.imread("scenes/"+SCENE+".jpg")
#except:
#    img = imageio.imread("scenes/"+SCENE+".png")
#result = resize_image(img, 1.5, 1.0, force_recalc=True)




DOWNSCALE_FACTOR = 1
SILENT_MODE = False
DONT_SHOW_IMAGES = True

sizes_a = []
sizes_b = []
for x in range(50, 100, 10):
    if x == 100:
        continue
    sizes_a.append([x, 100])
    sizes_b.append([100, x])

### STEP 1 ###
SOURCE = "/clusterarchive/SeamCarving/seam_carving_dataset/dataset/"

file_paths = []
for x in range((ITERATION*4), ((ITERATION+1)*4)):
    file_paths.append(SOURCE + str(x) + "/")
    DONT_SHOW_IMAGES = True

def translate(x):
    return x

### STEP 2 ###
### STEP 2.1 ###
ITERATION_MULTIPLIER = 2.5
DOWNSCALE_FACTOR = 1.0
VERSION = ""
for version in["v1", "v2", "v3", "v4"]:  
    VERSION = version
    it = 0
    for f in file_paths:
        file_path = f + "original.png"
        #file_path = "scenes/tower_shearing.png"
        print("DEVICE: ",DEVICE)
        print("PROCESSING - ITERATION ",it," - PATH ",f)
        start = time.time()
        for sizes in [sizes_a, sizes_b]:
            force_recalc = True #always force recalc!
            first = True
            for target_size in sizes:
                SCENE = "scene_"+f.split("/")[-2]+"_sx"+str(target_size[0])+"_sy"+str(target_size[1])
                print("*** SCENE: ",SCENE," ***")
                SILENT_MODE = False
                
                result = resize_image(imageio.imread(file_path), factor_x = target_size[0]/100, factor_y = target_size[1]/100, force_recalc=force_recalc)
                
                if first:
                    first = False
                    force_recalc = False

                SILENT_MODE = False
                #file_path_out = f + "ours_"
                file_path_out = "/clusterstorage/telsner/NeuralCarving/results_2D/" + f.split("/")[-2] + "_"
                if target_size[0] != 100:
                    file_path_out = file_path_out + "x_" + str(translate(target_size[0]))
                else:
                    file_path_out = file_path_out + "y_" + str(translate(target_size[1]))
                file_path_out = file_path_out + "_version_"+version+".png"
                print("\tSAVING ",file_path_out)
                ALLOW_SAVE = True
                save_image(result, file_path_out)
                ALLOW_SAVE = False
        print("Whole image group took ",(time.time()-start) / 60," minutes!")
        it += 1