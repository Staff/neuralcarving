# %%
import numpy as np
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import torch.nn.functional as F

if torch.cuda.is_available():
    import torch.backends.cudnn as cudnn
from torch import autograd
from torch.utils.data import Dataset
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision.utils import save_image

import matplotlib.pyplot as plt 

import imageio
from PIL import Image

# %%
def show(img):
    plt.imshow(img.transpose(0,1).transpose(1,2))
    plt.show()

# %%
def filter_out_all_but_largest(img):
    #1. get list of all non-white pixels
    #2. go over pixel by pixel
    #   2.1 if unmarked:
    #       mark
    #       region grow & mark
    #       add as new component
    #   2.2 if marked:
    #       do nothing
    #3. take biggest component and paste to empty image

    #1. get list of all non-white pixels
    unmarked = torch.ones(img.size()[1], img.size()[2]).long()
    for x in range(0, img.size()[1]):
        for y in range(0, img.size()[2]):
            if img[:,x,y].sum() > 2.9999:
                #white pixel
                unmarked[x,y] = 0


    #2. go over pixel by pixel
    biggest_region = []
    for x in range(0, img.size()[1]):
        for y in range(0, img.size()[2]):
    #   2.1 if unmarked:
            if unmarked[x,y] == 1:
                #new region to grow:
                new_region = [[x,y]]
                to_explore = [[x,y]]
                unmarked[x,y] = 0
                while len(to_explore) > 0:
                    item = to_explore.pop(0)
                    nx, ny = item[0], item[1]
                    new_region.append([nx, ny])
                    if nx > 0:
                        if unmarked[nx-1, ny] == 1:
                            to_explore.append([nx-1, ny])
                            unmarked[nx-1, ny] = 0
                    if ny > 0:
                        if unmarked[nx, ny-1] == 1:
                            to_explore.append([nx, ny-1])
                            unmarked[nx, ny-1] = 0
                    if nx < unmarked.size()[0]-1:
                        if unmarked[nx+1, ny] == 1:
                            to_explore.append([nx+1, ny])
                            unmarked[nx+1, ny] = 0
                    if ny < unmarked.size()[1]-1:
                        if unmarked[nx, ny+1] == 1:
                            to_explore.append([nx, ny+1])
                            unmarked[nx, ny+1] = 0

                if len(biggest_region) < len(new_region):
                    biggest_region = new_region
    
    #3. take biggest component and paste to empty image
    output_image = torch.ones_like(img)
    #output_image[1,:,:] = 0.0
    #output_image[2,:,:] = 0.0

    for pair in biggest_region:
        output_image[:, pair[0], pair[1]] = img[:, pair[0], pair[1]]

    return output_image


# %%
scene_indices = [0, 1, 2] #beach, farm, harbor
axises = [0, 1]
scalingfactors = [1.0]#[0.5, 1.5]

import os
def mkdir(path):
    try:
        os.mkdir(path)
    except:
        pass

def load(path):
    img = imageio.imread(path)
    img = Image.fromarray(img).convert("RGBA")
    img = torch.tensor(np.array(img)).transpose(1,2).transpose(1,0).float()[0:3] / 255.0
    return img

def save(img, path):
    from torchvision.utils import save_image
    save_image(img, path)

scenes_available = ['gta-v-beach-20m', 'gta-v-farm-60m', 'gta-v-harbor-50m', 'seminar']
for scene in scene_indices:
    for axis in axises:
        for scale in scalingfactors:
            path_from = "/clusterstorage/telsner/RAW_NeuralFields/computed_new/final_gif/"
            path_to = "/clusterstorage/telsner/RAW_NeuralFields/computed_new/final_gif_filtered/"

            path_from = path_from + scenes_available[scene] + "/"
            path_to = path_to + scenes_available[scene] + "/"
            mkdir(path_to)
            path_from = path_from + "sc" + str(scale) + "_ax"+str(axis)+"/"
            path_to = path_to + "sc" + str(scale) + "_ax"+str(axis)+"/"
            mkdir(path_to)

            for i in range(0, 105):
                save(filter_out_all_but_largest(load(path_from + str(i) + ".png")), path_to + str(i) + ".png")


